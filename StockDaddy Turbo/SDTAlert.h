//
//  SDTAlert.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SDTAlert : NSObject

- (id) init;
- (void) initDatabase;
- (BOOL) addToStockAlerts:(NSString*)symbol stockName:(NSString*)stockName targetPrice:(NSString*)targetPrc targetChg:(NSString*)targetChg targetChgPct:(NSString*)targetChgPct exchName:(NSString*)exchName yahooSymNExch:(NSString*)yahooSym googleSymbol:(NSString*)googSym targetPrcFlag:(NSInteger)targetPrcFlag targetChgFlag:(NSInteger)targetChgFlag targetChgPctFlag:(NSInteger)targetChgPctFlag notes:(NSString*)notes;

- (NSMutableArray*) getAllStockAlerts;
- (NSMutableString*) getStockListForGoogle;
- (BOOL) deleteFromStockAlerts:(NSString*)alertID;
- (NSMutableArray*) getTargetFlagList;

@end
