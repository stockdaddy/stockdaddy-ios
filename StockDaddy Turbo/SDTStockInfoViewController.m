//
//  SDTStockInfoViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockInfoViewController.h"

@interface SDTStockInfoViewController ()

@end

@implementation SDTStockInfoViewController
BOOL isSellButtonClicked;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    // hide the tab bar
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    
    UIMenuItem *editMenuItem = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(edit:)];
    [[UIMenuController sharedMenuController] setMenuItems: @[editMenuItem]];
    [[UIMenuController sharedMenuController] update];

    buyInfo = [[SDTStockBuy alloc] init];
    
    self.navigationItem.title = self.stockName;
    UIBarButtonItem *sellButton = [[UIBarButtonItem alloc] initWithTitle:@"Sell"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sellMethod)];
    
    self.navigationItem.rightBarButtonItem = sellButton;
    isSellButtonClicked = NO;

    [self reloadVC];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reloadVC)
                                                 name: @"UpdatePortfolioLabel"
                                               object: nil];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(editStockBuy:)
                                                 name: @"EditStockBuyOption"
                                               object: nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    if([self isMovingFromParentViewController]){

        [[NSNotificationCenter defaultCenter] removeObserver: self
                                              name: @"UpdatePortfolioLabel"
                                              object: nil];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma table view properties

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.stockInfoData.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Recent Transactions";
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StockInfoCell";
    SDTStockBuyCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[SDTStockBuyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [[self.stockInfoData objectAtIndex:indexPath.row] objectForKey:@"BuyDate"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Inv. Qty: %@ at %@", [[self.stockInfoData objectAtIndex:indexPath.row] objectForKey:@"BuyQuantity"], [[self.stockInfoData objectAtIndex:indexPath.row] objectForKey:@"BuyPrice"]];
    
    if(isSellButtonClicked) {
        SDTStockInfoSwitch *switchView = [[SDTStockInfoSwitch alloc] initWithFrame:CGRectZero];
        [switchView addTarget:self action:@selector(updateSwitchAtIndexPath:) forControlEvents:UIControlEventTouchUpInside];
        switchView.indexPathRow = indexPath.row;
        cell.accessoryView = switchView;
    }
    cell.indexPathRow = indexPath.row;
    
    return cell;
}

- (void)updateSwitchAtIndexPath:(SDTStockInfoSwitch *)aswitch
{
    NSString *buyID = [[self.stockInfoData objectAtIndex:aswitch.indexPathRow] objectForKey:@"StockBuyID"];
    NSLog(@"Selected Row BuyID %@", buyID);
    
    SDTStockSellViewController *stockSellVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StockSellVC"];
    stockSellVC.portfolioID = self.portfolioID;
    stockSellVC.stockName = self.stockName;
    stockSellVC.buyID = buyID;
    stockSellVC.stockID = [[self.stockInfoData objectAtIndex:aswitch.indexPathRow] objectForKey:@"StockID"];
    stockSellVC.buyDate = [[self.stockInfoData objectAtIndex:aswitch.indexPathRow] objectForKey:@"BuyDate"];
    stockSellVC.buyPrice = [[self.stockInfoData objectAtIndex:aswitch.indexPathRow] objectForKey:@"BuyPrice"];
    stockSellVC.buyQuant = [[self.stockInfoData objectAtIndex:aswitch.indexPathRow] objectForKey:@"BuyQuantity"];
    
    [self.navigationController pushViewController:stockSellVC animated:YES];
    
    [aswitch setOn:NO];
    isSellButtonClicked = !isSellButtonClicked;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *buyID = [[self.stockInfoData objectAtIndex:indexPath.row] objectForKey:@"StockBuyID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [buyInfo deleteFromStockBuyTable:buyID]) {
        // Delete the row from the data source
        [self.stockInfoData removeObjectAtIndex:indexPath.row];
        [self.mainTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.mainTable reloadData];
    [self reloadVC];
}

-(void) sellMethod
{
    NSLog(@"sellMethod Clicked");
    isSellButtonClicked = !isSellButtonClicked;
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma -
#pragma updateLabel

- (void) reloadVC
{
    self.stockInfoData = [buyInfo getBuyQuanPriceList:self.portfolioID stockID:self.stockID];

    self.currInvestmentLabel.text = [buyInfo currentInvestmentForStockID:self.portfolioID stockID:self.stockID];
    self.gainLossLabel.text = [buyInfo gainLossForStockID:self.portfolioID stockID:self.stockID];
    self.remainingQtyLabel.text = [buyInfo remainingQtyForStockID:self.portfolioID stockID:self.stockID];
    [self.mainTable reloadData];
}

#pragma mark -
#pragma mark Edit Menu Controller

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    return (action == @selector(copy:));
}

// required
- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
}

//Notification for StockBuy
- (void) editStockBuy:(NSNotification *)notification
{
    NSInteger indexPathRow = [[notification object] integerValue];
    NSLog(@"Edit StockBuy ID for %@", [[self.stockInfoData objectAtIndex:indexPathRow] objectForKey:@"StockBuyID"]);

    SDTStockBuyViewController *stockBuyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StockBuyVC"];
    stockBuyVC.isEditing = YES;
    stockBuyVC.stockBuyID = [[self.stockInfoData objectAtIndex:indexPathRow] objectForKey:@"StockBuyID"];
    stockBuyVC.stockName = self.stockName;
    stockBuyVC.investDate = [[self.stockInfoData objectAtIndex:indexPathRow] objectForKey:@"BuyDate"];
    stockBuyVC.currPrice = [[self.stockInfoData objectAtIndex:indexPathRow] objectForKey:@"BuyPrice"];
    stockBuyVC.investQuan = [[self.stockInfoData objectAtIndex:indexPathRow] objectForKey:@"BuyQuantity"];
    
    [self.navigationController pushViewController:stockBuyVC animated:YES];
}


@end
