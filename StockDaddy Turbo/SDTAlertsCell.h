//
//  SDTAlertsCell.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/16/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTAlertsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *symbol;
@property (weak, nonatomic) IBOutlet UILabel *stockName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *chg;
@property (weak, nonatomic) IBOutlet UILabel *pct;

@end
