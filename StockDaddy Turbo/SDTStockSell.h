//
//  SDTStockSell.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SDTStock.h"

@interface SDTStockSell : NSObject {
    SDTStock *stockInfo;
}

- (id) init;
- (void) initDatabase;
- (BOOL) addToStockSellTable:(NSString*)portfolioID stockBuyID:(NSString*)stockBuyID stockID:(NSString*)stockID sellDate:(NSString*)sellDate sellQuantity:(NSString*)sellQty sellPrice:(NSString*)sellPrc notes:(NSString*)notes;

- (NSMutableArray*) getStockSellInfoList:(NSString*)portfolioID;

- (NSString*) getSellQuantityForBuyID:(NSString*)buyID;
- (NSString*) getSellPriceForBuyID:(NSString*)buyID;

@end
