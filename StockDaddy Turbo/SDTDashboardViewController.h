//
//  SDTDashboardViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTTickerQuote.h"
#import "SDTPortfolio.h"
#import "SDTDashboardFourLabelCell.h"
#import "SDTDashboardFiveLabelCell.h"
#import "SDTTransactionHistoryViewController.h"
#import "SDTStock.h"
#import "SDTTickerQuoteViewController.h"

@interface SDTDashboardViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    SDTTickerQuote *tickerQuote;
    SDTPortfolio *portfolioObj;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) NSString *portfolioID;
@property (nonatomic, strong) NSString *portfolioName;
@property (nonatomic, strong) NSString *exchangeType;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, strong) NSString *googleExchSym;
@property (nonatomic, strong) NSString *yahooExchSym;
@property (nonatomic, strong) NSString *notes;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *maxGainerView;
@property (weak, nonatomic) IBOutlet UIView *maxLoserView;
@property (weak, nonatomic) IBOutlet UIView *todaysGainView;
@property (weak, nonatomic) IBOutlet UIView *currentGainView;
@property (weak, nonatomic) IBOutlet UIView *totalInvestmentView;
@property (weak, nonatomic) IBOutlet UIView *portfolioGainView;
@property (weak, nonatomic) IBOutlet UILabel *mainViewHeaderLabel;

@property (weak, nonatomic) IBOutlet UIView *fourLabelTitleView;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderSymbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderLabelOne;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderLabeltwo;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderLabelThree;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderLabelFour;

@property (weak, nonatomic) IBOutlet UIView *fiveLabelTitleView;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderFiveSymbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderFiveOne;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderFiveTwo;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderFiveThree;
@property (weak, nonatomic) IBOutlet UILabel *tableHeaderFiveFour;

@property (weak, nonatomic) IBOutlet UILabel *lastUpdatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainHeaderValLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainHeaderValChgL;

@property (weak, nonatomic) IBOutlet UILabel *maxGainerNameL;
@property (weak, nonatomic) IBOutlet UILabel *maxGainerValL;
@property (weak, nonatomic) IBOutlet UILabel *maxGainerChgL;

@property (weak, nonatomic) IBOutlet UILabel *maxLoserNameL;
@property (weak, nonatomic) IBOutlet UILabel *maxLoserValL;
@property (weak, nonatomic) IBOutlet UILabel *maxLoserChgL;


@property (weak, nonatomic) IBOutlet UILabel *todaysGainValL;

@property (weak, nonatomic) IBOutlet UILabel *currentGainValL;
@property (weak, nonatomic) IBOutlet UILabel *currentGainChgL;


@property (weak, nonatomic) IBOutlet UILabel *totalInvestmentValL;

@property (weak, nonatomic) IBOutlet UILabel *portfolioGainValL;
@property (weak, nonatomic) IBOutlet UILabel *portfolioGainChgL;


@end
