//
//  main.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/20/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDTAppDelegate class]));
    }
}
