//
//  SDTDefaultSubtitleCell.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/18/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTPorfolioCell.h"

@implementation SDTPorfolioCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma Edit Menu Controller

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(edit:)) {
        return YES;
    }
    return NO;
}

- (IBAction)edit:(id)sender
{
    NSLog(@"edit Menu called for Portfolio");
//    NSLog(@"portfolioID is %@", self.portfolioID);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditPortfolioOption" object:[NSString stringWithFormat:@"%li",(long)self.indexPathRow]];
}

@end
