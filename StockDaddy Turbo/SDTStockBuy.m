//
//  SDTStockBuy.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockBuy.h"

@implementation SDTStockBuy
static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
        stockInfo = [[SDTStock alloc] init];
        stockSellInfo = [[SDTStockSell alloc] init];
    }
    
    return self;
}

#pragma mark -
#pragma db operations

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS StockBuy ("
        "                StockBuyID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 PortfolioID VARCHAR, "
        "				 StockID VARCHAR, "
        "				 BuyDate VARCHAR, "
        "				 BuyQuantity VARCHAR, "
        "				 BuyPrice VARCHAR, "
        "				 Notes VARCHAR)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create StockBuy table because of %s", errMsg);
        }
        [self closeDataBase];
        
    } else {
        NSLog(@"Failed to open/create database");
    }
}

// return Yes: Succesfully Added to StockBuy Table; NO: Can't add to StockBuy Table
- (BOOL) addToStockBuyTable:(NSString*)portfolioID stockName:(NSString*)stockName stockSymbol:(NSString*)stocksym buyDate:(NSString*)buyDate buyQuantity:(NSString*)buyQuan buyPrice:(NSString*)buyPrc notes:(NSString*)notes exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typeDisp googleExchSym:(NSString*)googExchSym
{
    
    NSString *stockID;
    // if: StockID exists; else: No stock ID exists, insert to the table
    if([stockInfo stockIDExists:portfolioID stockName:stockName stockSymbol:stocksym]) {
        stockID = [stockInfo getStockID:portfolioID stockName:stockName stockSymbol:stocksym];
    } else {
        stockID = [stockInfo addTOStocksTable:portfolioID stockName:stockName stockSymbol:stocksym  exchDisp:exchDisp typeDisp:typeDisp googExchSym:googExchSym];
    }

    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO StockBuy (PortfolioID, StockID, BuyDate, BuyQuantity, BuyPrice, Notes) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", portfolioID, stockID, buyDate, buyQuan, buyPrc, notes];
        const char *insert_stmt = [insert_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Insert to StockBuy table because of %s", errMsg);
            [self closeDataBase];
            return NO;
        }
        [self closeDataBase];
        return YES;
    } else { NSLog(@"Failed to open/create database"); return NO; }
    
    return YES;
}

// return YES: Succesfully Deleted; NO: Error
- (BOOL) deleteFromStockBuyTable:(NSString*)stockBuyID
{
    BOOL deleted = YES;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM StockBuy WHERE StockBuyID=\"%@\"", stockBuyID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From StockBuy table because of %s", errMsg);
            [self closeDataBase];
            deleted = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); deleted = NO; }
    
    return deleted;
}

//Yes: Succesfully Editted info for StockBuy
- (BOOL) updateStockBuyForID:(NSString*)stockBuyID buyDate:(NSString*)buyDate buyPrice:(NSString*)buyPrice buyQuanity:(NSString*)buyQuant
{
    BOOL updated = YES;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *update_qry = [NSString stringWithFormat:@"UPDATE StockBuy SET BuyDate=\"%@\", BuyQuantity=\"%@\", BuyPrice=\"%@\" WHERE StockBuyID=\"%@\"", buyDate, buyQuant, buyPrice, stockBuyID];
        const char *update_stmt = [update_qry UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, update_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Update Edit StockBUyID in StockBuy table because of %s", errMsg);
            [self closeDataBase];
            updated = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); updated = NO; }
    
    return updated;
}

// return YES: Succesfully Updated; NO: Error
- (BOOL) updateBuyQuantityForStockBuyID:(NSString*)stockBuyID buyQuanity:(NSString*)buyQuant
{
    BOOL updated = YES;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *update_qry = [NSString stringWithFormat:@"UPDATE StockBuy SET BuyQuantity=\"%@\" WHERE StockBuyID=\"%@\"", buyQuant, stockBuyID];
        const char *update_stmt = [update_qry UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, update_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Update Buy Quantity in StockBuy table because of %s", errMsg);
            [self closeDataBase];
            updated = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); updated = NO; }
    
    return updated;
}


// Returns the list of all the buy Quantity, Prices for StockIDs.
// Called when calculating Dashboard Stats
// [{"BuyQuantity":"10","BuyPrice":"242.32", "StockID":"1"}, {...}]
- (NSMutableArray*) getBuyQuanPriceList:(NSString*)portfolioID stockID:(NSString*)stockID
{
    NSMutableArray *buyQuanPriceList = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT BuyQuantity, BuyPrice, StockID, BuyDate, StockBuyID FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\" ORDER BY StockBuyID DESC", portfolioID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *buyQuan = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *buyPrice = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *stockID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                NSString *buyDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                NSString *buyID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:buyQuan forKey:@"BuyQuantity"];
                [tempDictionary setObject:buyPrice forKey:@"BuyPrice"];
                [tempDictionary setObject:stockID forKey:@"StockID"];
                [tempDictionary setObject:buyDate forKey:@"BuyDate"];
                [tempDictionary setObject:buyID forKey:@"StockBuyID"];
                
                [buyQuanPriceList addObject:tempDictionary];
            }
            sqlite3_finalize(statement);
        }
        
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyQuanPriceList;
}

// Returns the Cumalative BuyQuantity of a stock
// Used when calcultating Dashboard Stats
- (NSString*) cumalativeBuyQuantity:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *buyQuan = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT SUM(BuyQuantity) FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyQuan = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"-";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyQuan;
}

// Returns the Avg Buy Price for a stock
// Used for Calcuating Dashboard stats
- (NSString*) averageBuyPrice:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *buyPrice = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT AVG(BuyPrice) FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyPrice = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"-";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyPrice;
}

//Returns teh CurrentInvestment for an indivisual stock
// USed for StockInfo page
- (NSString*) currentInvestmentForStockID:(NSString*)portfolioID stockID:(NSString*)stockID
{
    NSString *currInv = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT SUM(BuyQuantity*BuyPrice) AS CurrInv FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portfolioID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                currInv = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"-";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return currInv;
}

//Returns teh GainLoss for an indivisual stock
// USed for StockInfo page
- (NSString*) gainLossForStockID:(NSString*)portfolioID stockID:(NSString*)stockID
{
    float gainLoss = 0.0f;
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockBuyID, BuyPrice FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portfolioID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                if((const char *) sqlite3_column_text(statement, 0)){
                    NSString *buyID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                    float buyPrice = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)] doubleValue];
                    float sellPrice = [[stockSellInfo getSellPriceForBuyID:buyID] doubleValue];
                    float sellQty = [[stockSellInfo getSellQuantityForBuyID:buyID] doubleValue];
                    gainLoss += (sellPrice - buyPrice) * sellQty;
                }
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return [NSString stringWithFormat:@"%0.02f",gainLoss];
}

//Returns teh remaining Qty for an indivisual stock
// USed for StockInfo page
- (NSString*) remainingQtyForStockID:(NSString*)portfolioID stockID:(NSString*)stockID
{
    NSString *buyQuant = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT SUM(BuyQuantity) FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portfolioID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyQuant = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"-";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyQuant;
}

//Returns teh buy Qty for a buyid
// USed for Stocksell stat calculations
- (NSString*) getBuyQuantityForBuyID:(NSString*)buyID
{
    NSString *buyQuant = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT BuyQuantity FROM StockBuy WHERE StockBuyID=\"%@\"", buyID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyQuant = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"0";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyQuant;
}

//Returns teh buy Price for a buyid
// USed for Stocksell stat calculations
- (NSString*) getBuyPriceForBuyID:(NSString*)buyID
{
    NSString *buyPrice = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT BuyPrice FROM StockBuy WHERE StockBuyID=\"%@\"", buyID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyPrice = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"0";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyPrice;
}

- (NSString*) getBuyPriceForStockID:(NSString*)portfolioID stockID:(NSString*)stockID
{
    NSString *buyPrice = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT BuyPrice FROM StockBuy WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portfolioID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                buyPrice = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"0";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return buyPrice;
}

- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

@end
