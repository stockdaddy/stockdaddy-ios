//
//  SDTAlert.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTAlert.h"

@implementation SDTAlert
static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
    }
    
    return self;
}

#pragma mark -
#pragma db operations

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS StockAlerts ("
        "                StockAlertID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 StockName VARCHAR, "
        "				 StockSymbol VARCHAR, "
        "				 TargetPrice VARCHAR, "
        "                TargetChgVal VARCHAR, "
        "                TargetChgPct VARCHAR, "
        "                ExchName VARCHAR, "
        "                YahooSymNExch VARCHAR, "
        "                GoogleExchSym VARCHAR, "
        "                TargetPrcFlag INTEGER, "
        "                TargetChgFlag INTEGER, "
        "                TargetChgPctFlag INTEGER, "
        "                Notes VARCHAR)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create StockAlerts table because of %s", errMsg);
        }
        [self closeDataBase];
        
    } else {
        NSLog(@"Failed to open/create database");
    }
}

// return Yes: Succesfully Added to AlertTable; NO: Can't add to AlertTable
- (BOOL) addToStockAlerts:(NSString*)symbol stockName:(NSString*)stockName targetPrice:(NSString*)targetPrc targetChg:(NSString*)targetChg targetChgPct:(NSString*)targetChgPct exchName:(NSString*)exchName yahooSymNExch:(NSString*)yahooSym googleSymbol:(NSString*)googSym targetPrcFlag:(NSInteger)targetPrcFlag targetChgFlag:(NSInteger)targetChgFlag targetChgPctFlag:(NSInteger)targetChgPctFlag notes:(NSString*)notes
{
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
            char *errMsg;
            NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO StockAlerts (StockName, StockSymbol, TargetPrice, TargetChgVal, TargetChgPct, ExchName, YahooSymNExch, GoogleExchSym, TargetPrcFlag, TargetChgFlag, TargetChgPctFlag, Notes) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%li\", \"%li\", \"%li\", \"%@\")", stockName, symbol, targetPrc, targetChg, targetChgPct, exchName, yahooSym, googSym, (long)targetPrcFlag, (long)targetChgFlag, (long)targetChgPctFlag, notes];
            const char *insert_stmt = [insert_query UTF8String];
            
            if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to Insert to Alerts table because of %s", errMsg);
                [self closeDataBase];
                return NO;
            }
            [self closeDataBase];
            return YES;
        } else { NSLog(@"Failed to open/create database"); return NO; }
}

// returns array of dictionaries of all the stock alerts in the table
- (NSMutableArray*) getAllStockAlerts
{
    NSMutableArray *alertsData = [[NSMutableArray alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT * FROM StockAlerts"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            
                NSString *alertID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                [tempDictionary setObject:alertID forKey:@"alertID"];
                
                NSString *stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                [tempDictionary setObject:stockName forKey:@"stockName"];
                
                NSString *stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                [tempDictionary setObject:stockSymbol forKey:@"stockSymbol"];

                NSString *targetPrice = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                [tempDictionary setObject:targetPrice forKey:@"targetPrice"];

                NSString *targetChgVal = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                [tempDictionary setObject:targetChgVal forKey:@"targetChgVal"];

                NSString *targetChgPct = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                [tempDictionary setObject:targetChgPct forKey:@"targetChgPct"];

                NSString *exchName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)];
                [tempDictionary setObject:exchName forKey:@"exchName"];

                NSString *yahooSymNExch = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)];
                [tempDictionary setObject:yahooSymNExch forKey:@"yahooSymNExch"];

                NSString *googleExchSym = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 8)];
                [tempDictionary setObject:googleExchSym forKey:@"googleExchSym"];

                NSInteger targetPrcFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 9)] integerValue];
                [tempDictionary setObject:[@(targetPrcFlag) stringValue]forKey:@"targetPrcFlag"];

                NSInteger targetChgFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 10)] integerValue];
                [tempDictionary setObject:[@(targetChgFlag) stringValue]forKey:@"targetChgFlag"];

                NSInteger targetChgPctFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 11)] integerValue];
                [tempDictionary setObject:[@(targetChgPctFlag) stringValue]forKey:@"targetChgPctFlag"];

                NSString *notes = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 12)];
                [tempDictionary setObject:notes forKey:@"notes"];
                
                [alertsData addObject:tempDictionary];
            }
            
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getAllStockAlerts Query Failed!");
        }
        [self closeDataBase];
    } else { NSLog(@"Error Opening the DB for StockAlerts!"); }
    
    return alertsData;
}


- (NSMutableString*) getStockListForGoogle
{
    NSMutableString *googleList = [[NSMutableString alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT StockSymbol, GoogleExchSym FROM StockAlerts"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *googleSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];

                [googleList appendString:[NSString stringWithFormat:@"%@:%@,", googleSymbol, stockSymbol]];
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getStockListForGoogle Query Failed!");
        }
        [self closeDataBase];
    }
    
    return googleList;
}


// return YES: Succesfully Deleted; NO: Error
- (BOOL) deleteFromStockAlerts:(NSString*)alertID
{
    BOOL deleted = YES;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM StockAlerts WHERE StockAlertID=\"%@\"", alertID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From StockAlerts table because of %s", errMsg);
            [self closeDataBase];
            deleted = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); deleted = NO; }

    return deleted;
}

// returns the array of dictionaries for all stock target flags in alertsTable
- (NSMutableArray*) getTargetFlagList
{
    NSMutableArray *alertsData = [[NSMutableArray alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT TargetPrcFlag, TargetChgFlag, TargetChgPctFlag FROM StockAlerts"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                
                NSInteger targetPrcFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 0)] integerValue];
                [tempDictionary setObject:[@(targetPrcFlag) stringValue]forKey:@"targetPrcFlag"];
                
                NSInteger targetChgFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 1)] integerValue];
                [tempDictionary setObject:[@(targetChgFlag) stringValue]forKey:@"targetChgFlag"];
                
                NSInteger targetChgPctFlag =  [[[NSNumber alloc] initWithInt:sqlite3_column_int(statement, 2)] integerValue];
                [tempDictionary setObject:[@(targetChgPctFlag) stringValue]forKey:@"targetChgPctFlag"];
                
                [alertsData addObject:tempDictionary];
            }
            
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getTargetFlagList Query Failed!");
        }
        [self closeDataBase];
    } else { NSLog(@"Error Opening the DB for StockAlerts!"); }
    
    return alertsData;
}


- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

@end
