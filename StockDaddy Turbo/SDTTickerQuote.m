//
//  SDTTickerQuote.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTTickerQuote.h"

@implementation SDTTickerQuote

- (id) initWithPostNotificationName:(NSString*)postNotName
{
    self = [super init];
    if(self){
        // initialization code
        self.postNotifName = postNotName;
    }
    
    return self;
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData data] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse
{
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSLog(@"Connection Successfull");

    // convert the returned NSData to UTF8String and then filter the result to convert to JSON
    NSError *error = nil;
    NSString *dataToString = [[NSString alloc] initWithData:_responseData encoding:NSASCIIStringEncoding];

    if([dataToString rangeOfString:@"// "].location != NSNotFound){
        dataToString = [dataToString substringFromIndex:3];
    }
    
    NSDictionary *resultDict= [NSJSONSerialization JSONObjectWithData:[dataToString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    
   if(error == nil){
       NSLog(@"Posting %@ Notification", self.postNotifName);
       [[NSNotificationCenter defaultCenter] postNotificationName:self.postNotifName object: resultDict];
       
    } // end of error == nil
   else {
       NSLog(@"Error Occurred with NSData for NetworkRequestData. The error %@", error);
       [[NSNotificationCenter defaultCenter] postNotificationName:@"DataNotFound" object: nil];
       
       // FIX THE case if request other than Yahoo (Google) throws this error. see what happens
   }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // The request has failed for some reason!
    // Check the error var
    NSLog(@"Network Connection Failed");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network Failure" message:@"Please make sure you are connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkFailure" object: nil];
}


#pragma mark Yahoo Search

- (void) yahooTickerQuote:(NSString*)searchText
{
    NSString *urlText = [NSString stringWithFormat:@"http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol in (\"%@\")&format=json&diagnostics=true&env=http://datatables.org/alltables.env", searchText];
    NSString* newURLText = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    // Create the request.
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:newURLText]];
    
    // Create url connection and fire request
    (void) [[NSURLConnection alloc] initWithRequest:requestURL delegate:self];
}

- (void) yahooOptionChainQuote:(NSString*)symbol
{
    NSString *urlText = [NSString stringWithFormat:@"http://query.yahooapis.com/v1/public/yql?q=SELECT * FROM yahoo.finance.options WHERE symbol='%@' AND expiration in (SELECT contract FROM yahoo.finance.option_contracts WHERE symbol='%@')&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys", symbol, symbol];
    NSString* newURLText = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // Create the request.
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:newURLText]];
    
    // Create url connection and fire request
    (void) [[NSURLConnection alloc] initWithRequest:requestURL delegate:self];
}

- (void) yahooStockNews:(NSString*)symbol
{
    NSString *urlText = [NSString stringWithFormat:@"https://query.yahooapis.com/v1/public/yql?q=select * from rss where url='http://finance.yahoo.com/rss/headline?s=%@'&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys", symbol];
    NSString* newURLText = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // Create the request.
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:newURLText]];
    
    // Create url connection and fire request
    (void) [[NSURLConnection alloc] initWithRequest:requestURL delegate:self];
}


#pragma mark Google Search

- (void) googleTickerQuote:(NSString*)symbol
{
    NSString *urlText = [NSString stringWithFormat:@"http://finance.google.com/finance/info?client=ig&q=%@", symbol];

    // Create the request.
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:urlText]];

    // Create url connection and fire request
    (void) [[NSURLConnection alloc] initWithRequest:requestURL delegate:self];
}

#pragma mark Indian Quote

- (void) indianTickerQuote:(NSString*)searchText
{
    NSLog(@"indianTickerQuote search Request for Symbol %@", searchText);
    
    NSString *urlText = [NSString stringWithFormat:@"http://live-nse.herokuapp.com/?symbol=%@", searchText];
    NSString* newURLText = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // Create the request.
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:newURLText]];
    
    // Create url connection and fire request
    (void) [[NSURLConnection alloc] initWithRequest:requestURL delegate:self];
}

@end
