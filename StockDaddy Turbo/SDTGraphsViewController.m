//
//  SDTGraphsViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/14/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTGraphsViewController.h"

@interface SDTGraphsViewController ()

@end

@implementation SDTGraphsViewController

NSMutableArray* dateRange;
BOOL isDateRangeShowing;
int selectedDateRangeIndex;
UIWebView *graphWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"symbol is %@", self.stockSymbol);
    NSString *url = [NSString stringWithFormat:@"http://chart.finance.yahoo.com/z?s=%@&t=1d&q=&l=&z=l&a=v&p=s&lang=en-US&region=US", self.stockSymbol];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webView loadRequest:urlRequest];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    isDateRangeShowing = NO;
    selectedDateRangeIndex = 0;

    [self initializeDateRange];
    
    graphWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
    graphWebView.scalesPageToFit = YES;
    
    self.adBanner.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view properties

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        if(isDateRangeShowing){
            return [dateRange count];
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

// Add header titles in sections.
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    switch (section) {
        case 0:
            title = @"Date Range";
            break;
    }
    
    return title;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 40.0f;
    } else {
        return 10.0f;
    }
}

// Set the row height.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        return 30.0;
    } else {
        return 200.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GraphCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    if(indexPath.section == 0){
    
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryNone;

        if(!isDateRangeShowing){
            [cell.textLabel setText:[[dateRange objectAtIndex:selectedDateRangeIndex] objectForKey:@"label"]];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            [cell.textLabel setText:[[dateRange objectAtIndex:indexPath.row] objectForKey:@"label"]];
            
            if(indexPath.row == selectedDateRangeIndex){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }

        cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];

    } else {
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[self graphURLString:[[dateRange objectAtIndex:selectedDateRangeIndex] objectForKey:@"value"]]]];
        [graphWebView loadRequest:urlRequest];
        
        [cell addSubview:graphWebView];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(isDateRangeShowing){
            selectedDateRangeIndex = (int)indexPath.row;
        }
        isDateRangeShowing = !isDateRangeShowing;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma -
#pragma initalize

- (void) initializeDateRange
{
    dateRange = [[NSMutableArray alloc] init];
    
    NSDictionary *oneDay = @{@"label":@"1 Day", @"value": @"1d"};
    [dateRange addObject:oneDay];

    NSDictionary *fiveDay = @{@"label":@"5 Day", @"value": @"5d"};
    [dateRange addObject:fiveDay];

    NSDictionary *oneMonth = @{@"label":@"1 Month", @"value": @"1m"};
    [dateRange addObject:oneMonth];

    NSDictionary *threeMonth = @{@"label":@"3 Month", @"value": @"3m"};
    [dateRange addObject:threeMonth];

    NSDictionary *sixMonth = @{@"label":@"6 Month", @"value": @"6m"};
    [dateRange addObject:sixMonth];

    NSDictionary *oneYear = @{@"label":@"1 Year", @"value": @"1y"};
    [dateRange addObject:oneYear];

    NSDictionary *twoYear = @{@"label":@"2 Year", @"value": @"2y"};
    [dateRange addObject:twoYear];
    
    NSDictionary *fiveYear = @{@"label":@"5 Year", @"value": @"5y"};
    [dateRange addObject:fiveYear];
    
    NSDictionary *max = @{@"label":@"Max", @"value": @"my"};
    [dateRange addObject:max];
    
}

-(NSString*) graphURLString:(NSString*)date
{
    return [NSString stringWithFormat:@"http://chart.finance.yahoo.com/z?s=%@&t=%@&q=&l=&z=l&a=v&p=s&lang=en-US&region=US", self.stockSymbol, date];
}

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma iAd Banner Delegate

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"GraphView Banner failed to load because of %@", error);
    [self.adBanner removeFromSuperview];
    self.adBanner.hidden = YES;
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    
    if( ([defaults objectForKey:@"showAds"]) && ([[defaults objectForKey:@"showAds"] isEqualToString:@"RemoveThem"])){
        self.adBanner.hidden = YES;
    } else {
        self.adBanner.hidden = NO;
        [self.mainTable addSubview:self.adBanner];
    }
}

//-(void)bannerViewWillLoadAd:(ADBannerView *)banner
//{
//}

//-(void)bannerViewActionDidFinish:(ADBannerView *)banner
//{
//}

@end
