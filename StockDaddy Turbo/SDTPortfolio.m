//
//  SDTPortfolio.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTPortfolio.h"

@implementation SDTPortfolio

NSMutableArray *portfolioArray;

-(id)init
{
    self = [super init];
    if(self){
        // initialization code
        [self initDatabase];
        
        // initialize the portfolioArray;
        portfolioArray  = [[NSMutableArray alloc] init];
        stockInfo = [[SDTStock alloc] init];
        stockBuyInfo = [[SDTStockBuy alloc] init];
        stockSellInfo = [[SDTStockSell alloc] init];
    }
    
    return self;
}

-(void)initDatabase
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Portfolio (PortfolioID INTEGER PRIMARY KEY AUTOINCREMENT, PortfolioName VARCHAR, ExchangeType VARCHAR, Region VARCHAR, ExchTypeSymbol VARCHAR, GoogExchSymbol VARCHAR, Currency VARCHAR, Notes VARCHAR)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create Portfolio table");
        }
        [self closeDataBase];
    } else {
        NSLog(@"Failed to open/create database");
    }
}

#pragma mark -
#pragma Database operations

// insert the data to the Portfolio Table
-(BOOL) insertPortName:(NSString*)portName insertExchType:(NSString*)exchType insertRegion:(NSString*)region
    insertExchTypeSymb:(NSString*)exchTypeSymb insertGoogleExchSymb:(NSString*)googleExchSymb
        insertCurrency:(NSString*)currency insertNotes:(NSString*)notes
{
    BOOL success = YES;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *sql_stmt = [NSString stringWithFormat:@"INSERT INTO Portfolio (PortfolioName, ExchangeType, Region, ExchTypeSymbol, GoogExchSymbol, Currency, Notes) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", portName, exchType, region, exchTypeSymb, googleExchSymb, currency, notes];
        const char *insert_stmt = [sql_stmt UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Insert to Portfolio table because of %s", errMsg);
            success = NO;
        }
        [self closeDataBase];
    } else {
        NSLog(@"Failed to open/create database");
        success = NO;
    }
    
    return success;
}

// return YES: Succesfully Deleted; NO: Error
- (BOOL) deleteFromPortfolioTable:(NSString*)portfolioID
{
    BOOL deleted = YES;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM Portfolio WHERE PortfolioID=\"%@\"", portfolioID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From Portfolio table because of %s", errMsg);
            [self closeDataBase];
            deleted = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); deleted = NO; }
    
    return deleted;
}

// update Portfolio Data for PortfolioID
-(BOOL) updatePortfolioForID:(NSString*)portfolioID portfolioName:(NSString*)portName portfolioNotes:(NSString*)portNotes
{
    BOOL success = YES;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *sql_stmt = [NSString stringWithFormat:@"UPDATE Portfolio SET PortfolioName=\"%@\", Notes=\"%@\" WHERE PortfolioID=\"%@\"", portName, portNotes, portfolioID];
        const char *update_stmt = [sql_stmt UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, update_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to update to Portfolio table because of %s", errMsg);
            success = NO;
        }
        [self closeDataBase];
    } else {
        NSLog(@"Failed to open/create database");
        success = NO;
    }
    
    return success;
}

//is called for TableView DataSource for Portfolio
- (NSMutableArray*) getAllPortfolioForTableViewDS
{
    // clear teh portfolioDictionary
    [portfolioArray removeAllObjects];
    if(!portfolioArray) {
        portfolioArray = [[NSMutableArray alloc] init];
    }

    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        
        NSString *showAllQuery = [NSString stringWithFormat: @"SELECT * FROM Portfolio"];
        const char *select_stmt = [showAllQuery UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                // temporary dictonary to store sql results. use this dictionary as a value for portID;
                NSMutableDictionary *tmpPortDictionary = [[NSMutableDictionary alloc] init];

                NSString *portfolioID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *portfolioName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *exchangeType = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                NSString *region = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                NSString *exchTypeSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                NSString *googExchSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                NSString *currency = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)];
                NSString *notes = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)];
                                
                [tmpPortDictionary setValue:portfolioID forKey:@"portfolioID"];
                [tmpPortDictionary setValue:portfolioName forKey:@"portfolioName"];
                [tmpPortDictionary setValue:exchangeType forKey:@"exchangeType"];
                [tmpPortDictionary setValue:region forKey:@"region"];
                [tmpPortDictionary setValue:exchTypeSymbol forKey:@"exchTypeSymbol"];
                [tmpPortDictionary setValue:googExchSymbol forKey:@"googExchSymbol"];
                [tmpPortDictionary setValue:currency forKey:@"currency"];
                [tmpPortDictionary setValue:notes forKey:@"notes"];
                
                [portfolioArray addObject:tmpPortDictionary];
            }
            
        } else {
            NSLog(@"SelectAll Portfolio Query Failed!");
        }
        sqlite3_close(stockDaddyDB);
    }
    
    return portfolioArray;
}

//is called when Adding Searched Stock to Portfolio
- (NSMutableArray*) getPortfolioList:(NSString*)exchName
{
    NSMutableArray *portfolioList = [[NSMutableArray alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        
        NSString *showAllQuery = [NSString stringWithFormat: @"SELECT * FROM Portfolio WHERE ExchangeType=\"%@\" OR ExchangeType=\"Multi-Exchange\"",exchName];
        const char *select_stmt = [showAllQuery UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                // temporary dictonary to store sql results. use this dictionary as a value for portID;
                NSMutableDictionary *tmpPortDictionary = [[NSMutableDictionary alloc] init];
                
                NSString *portfolioID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *portfolioName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];

                [tmpPortDictionary setValue:portfolioID forKey:@"PortfolioID"];
                [tmpPortDictionary setValue:portfolioName forKey:@"PortfolioName"];

                [portfolioList addObject:tmpPortDictionary];
            }
            
        } else {
            NSLog(@"getPortfolioList Portfolio Query Failed!");
        }
        sqlite3_close(stockDaddyDB);
    }
    
    return portfolioList;
}

// YES: Portfolio Is Multi-Exchange; No: otherwise
-(BOOL)isMultiExchangePortfolio:(NSString*)portfolioID
{
    NSString *portID;
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT PortfolioID FROM Portfolio WHERE PortfolioID=\"%@\" AND ExchangeType='Multi-Exchange'", portfolioID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                portID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return NO; }
    
    NSLog(@"PortfolioID check for Multi-Exchange is %@", portID);
    return ([portID length]==0) ? NO : YES;
}

// will be called when doing a network request for calculating stats
-(NSString*) getGoogleSymExch:(NSString*)portID
{
    NSString *googExchSym = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT GoogExchSymbol FROM Portfolio WHERE PortfolioID=\"%@\"", portID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                googExchSym = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return googExchSym;
}

// will be called for Sell Stock Page
-(NSString*) getPortfolioNameFromPortfolioID:(NSString*)portID
{
    NSString *portfolioName = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT PortfolioName FROM Portfolio WHERE PortfolioID=\"%@\"", portID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                portfolioName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return portfolioName;
}


- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

#pragma mark -
#pragma Portfolio Calculations

// Portfolio Total Investment
// BuyQuantity(i) * BuyPrice(i)
-(NSString*) portfolioTotalInvestment:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    
    float totalInvestment = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        for(NSDictionary *buyQuanInfo in stockIDInfo){
            totalInvestment += [[buyQuanInfo objectForKey:@"BuyPrice"] doubleValue] * [[buyQuanInfo objectForKey:@"BuyQuantity"] doubleValue];
        }
    }
    
    return [NSString stringWithFormat:@"%.02f", totalInvestment];
}

// Portfolio Networth
// BuyQuantity(i) * CurrentPrice(i)
-(NSString*) portfolioNetWorth:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float totalNetWorth = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        totalNetWorth += [[buyQuanInfo objectForKey:@"BuyQuantity"] doubleValue] * [[googleDataItem objectForKey:@"l"] doubleValue];
                        break;
                    }
                }
                
            }
        }
    }
    
    return [NSString stringWithFormat:@"%.02f", totalNetWorth];
}

// Portfolio Todays Gain
// BuyQuantity(i) * CurrentPriceChg(i)
-(NSString*) portfolioTodaysGain:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float todaysGain = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        todaysGain += [[buyQuanInfo objectForKey:@"BuyQuantity"] doubleValue] * [[googleDataItem objectForKey:@"c"] doubleValue];
                        break;
                    }
                }
                
            }
        }
    }
    
    return [NSString stringWithFormat:@"%.02f", todaysGain];
}

// Portfolio Todays Gain
// (BuyQuantity(i) * CurrentPrice(i)) - (BuyQuantity(i) * BuyPrice(i))
-(NSString*) portfolioCurrentGain:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float todaysGain = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        todaysGain += (([[buyQuanInfo objectForKey:@"BuyQuantity"] doubleValue] * [[googleDataItem objectForKey:@"l"] doubleValue]) - ([[buyQuanInfo objectForKey:@"BuyQuantity"] doubleValue] * [[buyQuanInfo objectForKey:@"BuyPrice"] doubleValue]));
                        break;
                    }
                }
                
            }
        }
    }
    
    return [NSString stringWithFormat:@"%.02f", todaysGain];
}

// Portfolio Current Gain Pct
// (currentGain/totalInvest)*100
-(NSString*) portfolioCurrentGainPct:(NSString*)portID
{
    float totalInvest = [[self portfolioTotalInvestment:portID] doubleValue];
    float currentGain = [[self portfolioCurrentGain:portID] doubleValue];
    
    if(totalInvest!=0){
        return [NSString stringWithFormat:@"%.02f", (currentGain/totalInvest)*100];
    } else {
        return @"0";
    }
}

// Portfolio Gain/Loss
// (SellPrice(i) - BuyPrice(i)) * SellQty(i))
-(NSString*) portfolioGainLoss:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float portGainLoss = 0.0f;
    for(NSString *stockID in stockIDList){
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        NSArray *sellStockInfo = [stockSellInfo getStockSellInfoList:portID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *sellInfo in sellStockInfo){
                    if([[sellInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        float sellPrc = [[sellInfo objectForKey:@"SellPrice"] doubleValue];
                        float sellQty = [[sellInfo objectForKey:@"SellQuantity"] doubleValue];
                        NSString *buyPrice = [stockBuyInfo getBuyPriceForStockID:portID stockID:stockID];
                        
                        portGainLoss += (sellPrc - [buyPrice doubleValue]) * sellQty;
                    }
                }
                
            }
        }
    }
    
    return [NSString stringWithFormat:@"%.02f", portGainLoss];
}

// Portfolio Gain/Loss Pct
// (SUM(SellPrice*SellQuantity) - SUM(BuyPrice*SellQuantity)) / (SUM(BuyPrice*SellQuantity))
-(NSString*) portfolioGainLossPct:(NSString*)portID
{
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float sumSellPriceQty = 0.0f;
    float sumBuyPriceSellQty = 0.0f;
    
    for(NSString *stockID in stockIDList){
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        NSArray *sellStockInfo = [stockSellInfo getStockSellInfoList:portID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *sellInfo in sellStockInfo){
                    if([[sellInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        float sellPrc = [[sellInfo objectForKey:@"SellPrice"] doubleValue];
                        float sellQty = [[sellInfo objectForKey:@"SellQuantity"] doubleValue];
                        NSString *buyPrice = [stockBuyInfo getBuyPriceForStockID:portID stockID:stockID];
                        
                        sumSellPriceQty += sellQty * sellPrc;
                        sumBuyPriceSellQty += [buyPrice doubleValue] * sellQty;
                    }
                }
                
            }
        }
    }
     return (sumBuyPriceSellQty) ? [NSString stringWithFormat:@"%.02f", (sumSellPriceQty - sumBuyPriceSellQty) / sumBuyPriceSellQty] : @"0.00";
}

// Portfolio Max Gainer
// Max Chg Pct. Returns a Dictionary with all the data
-(NSDictionary*) portfolioMaxGainer:(NSString*)portID
{
    NSMutableDictionary *portMaxGainer = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float max = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        if([[googleDataItem objectForKey:@"cp"] doubleValue] > max){

                            [portMaxGainer removeAllObjects];
                            if(!portMaxGainer){
                                portMaxGainer = [[NSMutableDictionary alloc] init];
                            }
                            
                            max = [[googleDataItem objectForKey:@"c"] doubleValue];
                            NSString *stockID = [buyQuanInfo objectForKey:@"StockID"];
                            [portMaxGainer setObject:stockID forKey:@"StockID"];

                            NSString *symbol = [googleDataItem objectForKey:@"t"];
                            [portMaxGainer setObject:symbol forKey:@"StockSymbol"];

                            NSString *currPrice = [googleDataItem objectForKey:@"l"];
                            [portMaxGainer setObject:currPrice forKey:@"Price"];

                            NSString *currPriceChg = [googleDataItem objectForKey:@"c"];
                            [portMaxGainer setObject:currPriceChg forKey:@"Change"];

                            NSString *currPriceChgPct = [googleDataItem objectForKey:@"cp"];
                            [portMaxGainer setObject:currPriceChgPct forKey:@"ChangePct"];
                        }
                        break;
                    }
                }
                
            }
        }
    }
    
    return portMaxGainer;
}

// Portfolio Max Loser
// Max Chg Pct. Returns a Dictionary with all the data
-(NSDictionary*) portfolioMaxLoser:(NSString*)portID
{
    NSMutableDictionary *portMaxLoser = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portID];
    float min = 0.0f;
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        if([[googleDataItem objectForKey:@"cp"] doubleValue] < min){
                            
                            [portMaxLoser removeAllObjects];
                            if(!portMaxLoser){
                                portMaxLoser = [[NSMutableDictionary alloc] init];
                            }
                            
                            min = [[googleDataItem objectForKey:@"c"] doubleValue];
                            NSString *stockID = [buyQuanInfo objectForKey:@"StockID"];
                            [portMaxLoser setObject:stockID forKey:@"StockID"];
                            
                            NSString *symbol = [googleDataItem objectForKey:@"t"];
                            [portMaxLoser setObject:symbol forKey:@"StockSymbol"];
                            
                            NSString *currPrice = [googleDataItem objectForKey:@"l"];
                            [portMaxLoser setObject:currPrice forKey:@"Price"];
                            
                            NSString *currPriceChg = [googleDataItem objectForKey:@"c"];
                            [portMaxLoser setObject:currPriceChg forKey:@"Change"];
                            
                            NSString *currPriceChgPct = [googleDataItem objectForKey:@"cp"];
                            [portMaxLoser setObject:currPriceChgPct forKey:@"ChangePct"];
                        }
                        break;
                    }
                }
                
            }
        }
    }
    
    return portMaxLoser;
}


#pragma mark -
#pragma Stats DataSource

//Returns teh Datasource used for Networth Table
-(NSMutableArray*) portfolioNetworthDS:(NSString*)portfolioID
{
    NSMutableArray *networthDS = [[NSMutableArray alloc] init];

    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portfolioID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];

                        float totalBuy = [[stockBuyInfo cumalativeBuyQuantity:portfolioID stockID:stockID] doubleValue];
                        float currPrice = [[googleDataItem objectForKey:@"l"] doubleValue];
                        float networth = totalBuy * currPrice;

                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", totalBuy] forKey:@"BuyQuantity"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", currPrice] forKey:@"CurrentPrice"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", networth] forKey:@"Networth"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];
                        
                        [networthDS addObject:tempDictionary];
                        break;
                    }
                }
            }
        }
    }
    
    return networthDS;
}

//Returns teh Datasource used for MaxMin Table
// sortAscending Yes: MaxLoser, No: MaxGain
-(NSArray*) portfolioMaxMinDS:(NSString*)portfolioID sortAscending:(BOOL)asc
{
    NSMutableArray *maxMinDS = [[NSMutableArray alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portfolioID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];
        
        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];

                        NSString *currPrice = [googleDataItem objectForKey:@"l"];
                        NSString *currPriceChg = [googleDataItem objectForKey:@"c"];
                        NSString *currPriceChgPct = [googleDataItem objectForKey:@"cp"];
                        
                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:currPrice forKey:@"CurrentPrice"];
                        [tempDictionary setValue:currPriceChg forKey:@"CurrentPriceChg"];
                        [tempDictionary setValue:currPriceChgPct forKey:@"CurrentPriceChgPct"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];
                        
                        [maxMinDS addObject:tempDictionary];
                        break;
                    }
                }
            }
        }
    }

    // for sorting the result
    NSSortDescriptor *chgpctDescriptor = [[NSSortDescriptor alloc] initWithKey:@"CurrentPriceChgPct" ascending:asc];
    NSArray *descriptors = [NSArray arrayWithObjects:chgpctDescriptor, nil];

    return [maxMinDS sortedArrayUsingDescriptors:descriptors];
}

//Returns teh Datasource used for Todays Gain Table
-(NSMutableArray*) portfolioTodaysGainDS:(NSString*)portfolioID
{
    NSMutableArray *todaysGainDS = [[NSMutableArray alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portfolioID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                        
                        float totalBuy = [[stockBuyInfo cumalativeBuyQuantity:portfolioID stockID:stockID] doubleValue];
                        float currPriceChg = [[googleDataItem objectForKey:@"c"] doubleValue];
                        float todayGain = totalBuy * currPriceChg;
                        
                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", totalBuy] forKey:@"BuyQuantity"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", currPriceChg] forKey:@"CurrentPriceChg"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", todayGain] forKey:@"TodaysGain"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];

                        [todaysGainDS addObject:tempDictionary];
                        break;
                    }
                }
            }
        }
    }
    
    return todaysGainDS;
}

//Returns teh Datasource used for Current Gain Table
-(NSMutableArray*) portfolioCurrentGainDS:(NSString*)portfolioID
{
    NSMutableArray *currentGainDS = [[NSMutableArray alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portfolioID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                        
                        float totalBuy = [[stockBuyInfo cumalativeBuyQuantity:portfolioID stockID:stockID] doubleValue];
                        float avgPrice = [[stockBuyInfo averageBuyPrice:portfolioID stockID:stockID] doubleValue];
                        float currPrice = [[googleDataItem objectForKey:@"l"] doubleValue];
                        float currentGainVal = (totalBuy * currPrice)-(totalBuy * avgPrice);
                        
                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", totalBuy] forKey:@"BuyQuantity"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", avgPrice] forKey:@"AvgBuyPrice"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", currPrice] forKey:@"CurrentPrice"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", currentGainVal] forKey:@"CurrentGain"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];

                        [currentGainDS addObject:tempDictionary];
                        break;
                    }
                }
            }
        }
    }
    
    return currentGainDS;
}

//Returns teh Datasource used for Total Investment Table
-(NSMutableArray*) portfolioTotalInvestmentDS:(NSString*)portfolioID
{
    NSMutableArray *totalInvestmentDS = [[NSMutableArray alloc] init];

    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSArray *stockIDInfo = [stockBuyInfo getBuyQuanPriceList:portfolioID stockID:stockID];
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *buyQuanInfo in stockIDInfo){
                    if([[buyQuanInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                        
                        float totalBuy = [[stockBuyInfo cumalativeBuyQuantity:portfolioID stockID:stockID] doubleValue];
                        float avgPrice = [[stockBuyInfo averageBuyPrice:portfolioID stockID:stockID] doubleValue];
                        float totalInvest = totalBuy * avgPrice;
                        
                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", totalBuy] forKey:@"BuyQuantity"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", avgPrice] forKey:@"BuyPrice"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", totalInvest] forKey:@"TotalInvestment"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];

                        [totalInvestmentDS addObject:tempDictionary];
                        break;
                    }
                }
            }
        }
    }
    
    return totalInvestmentDS;
}

//Returns teh Datasource used for Portfolio gainLoss Table
-(NSMutableArray*) portfolioGainLossDS:(NSString*)portfolioID
{
    NSMutableArray *portGainLossDS = [[NSMutableArray alloc] init];
    
    NSMutableArray *stockIDList = [stockInfo getStockIDList:portfolioID];
    for(NSString *stockID in stockIDList){
        NSString *stockSym = [stockInfo stockSymbolByStockID:portfolioID stockID:stockID];
        NSArray *sellStockInfo = [stockSellInfo getStockSellInfoList:portfolioID];
        NSString *exchDisp = [stockInfo stockExchDispByStockID:portfolioID stockID:stockID];
        NSString *typeDisp = [stockInfo stockTypeDispByStockID:portfolioID stockID:stockID];

        // need to make sure the symbols match with returned data to
        // calculate stats correctly
        for(NSDictionary *googleDataItem in self.googleData){
            if([[googleDataItem objectForKey:@"t"] isEqualToString:stockSym]) {
                for(NSDictionary *sellInfo in sellStockInfo){
                    if([[sellInfo objectForKey:@"StockID"] isEqualToString:stockID]) {
                        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                        
                        float sellPrc = [[sellInfo objectForKey:@"SellPrice"] doubleValue];
                        float sellQty = [[sellInfo objectForKey:@"SellQuantity"] doubleValue];
                        NSString *buyPrice = [stockBuyInfo getBuyPriceForStockID:portfolioID stockID:stockID];
                        
                        float gainLoss = (sellPrc - [buyPrice doubleValue]) * sellQty;
                        
                        [tempDictionary setValue:stockID forKey:@"StockID"];
                        [tempDictionary setValue:stockSym forKey:@"StockSymbol"];
                        [tempDictionary setValue:buyPrice forKey:@"BuyPrice"];
                        [tempDictionary setValue:[stockInfo stockNameByStockID:portfolioID stockID:stockID] forKey:@"StockName"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", sellPrc] forKey:@"SellPrice"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", sellQty] forKey:@"SellQuantity"];
                        [tempDictionary setValue:[NSString stringWithFormat:@"%.02f", gainLoss] forKey:@"GainLoss"];
                        [tempDictionary setValue:exchDisp forKey:@"ExchDisp"];
                        [tempDictionary setValue:typeDisp forKey:@"TypeDisp"];
                        
                        [portGainLossDS addObject:tempDictionary];
                    }
                }
            }
        }
    }
    
    return portGainLossDS;
}

#pragma mark -
#pragma helper functions

// returns a symbol list string which will be used in Google Request
-(NSString*) googleRequestURLSymList:(NSString*)portfolioID
{
    return [stockInfo generateGoogleURLStockList:portfolioID];
}

-(NSString*) sumBuyQuan:(NSString*)portID stockID:(NSString*)stockID
{
    return [stockBuyInfo cumalativeBuyQuantity:portID stockID:stockID];
}


-(NSString*) avgBuyPrice:(NSString*)portID stockID:(NSString*)stockID
{
    return [stockBuyInfo averageBuyPrice:portID stockID:stockID];
}

@end
