//
//  SDTNotificationNewViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTNotificationNewViewController.h"

@interface SDTNotificationNewViewController ()

@end

@implementation SDTNotificationNewViewController

NSMutableArray* notificationData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    notificationData = [notificationInfo getAllNotifications];
    
    [self.mainTable reloadData];

    [self checkForNoData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    
    notificationInfo = [[SDTNotification alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notificationData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotificationCell";
    SDTNotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    // Configure the cell...
    NSDictionary *tempDictionary= [notificationData objectAtIndex:indexPath.row];
    
    cell.msg.lineBreakMode = NSLineBreakByWordWrapping;
    cell.msg.numberOfLines = 0;
    cell.msg.text = [self generateMsgBody:[tempDictionary objectForKey:@"stockSymbol"] targetVal:[tempDictionary objectForKey:@"TargetVal"] targetValFlag:[tempDictionary objectForKey:@"TargetValFlag"] currentPrice:[tempDictionary objectForKey:@"CurrentPrice"] currentChg:[tempDictionary objectForKey:@"CurrentChg"] currentChgPct:[tempDictionary objectForKey:@"CurrentChgPct"]];
    
    cell.stockName.text = [tempDictionary objectForKey:@"stockName"];
    cell.dateTime.text = [tempDictionary objectForKey:@"NotifDate"];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *notificationID = [[notificationData objectAtIndex:indexPath.row] objectForKey:@"notificationID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [notificationInfo deleteFromNotifTable:notificationID]) {
        // Delete the row from the data source
        [notificationData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.mainTable reloadData];
    
    [self checkForNoData];
}


#pragma mark -
#pragma helper methods

-(NSString*) generateMsgBody:(NSString*)symbol targetVal:(NSString*)tarVal targetValFlag:(NSString*)tarValFlag currentPrice:(NSString*)cPrice currentChg:(NSString*)cChg currentChgPct:(NSString*)cChgPct
{
    return [NSString stringWithFormat:@"%@ has reached the Target Quote of %@. The Current Quote is %@  %@ (%@%%)", symbol, tarValFlag, cPrice, cChg, cChgPct];
}

- (void) checkForNoData
{
    if([notificationData count] > 0){
        self.noDataCont.hidden = YES;
    } else {
        self.noDataCont.hidden = NO;
    }
}

@end
