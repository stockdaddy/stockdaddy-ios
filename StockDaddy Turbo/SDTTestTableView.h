//
//  SDTTestTableView.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/23/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTTestTableView : UITableView

@property (strong, nonatomic) NSArray *tableData;

@end
