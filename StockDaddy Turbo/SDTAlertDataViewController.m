//
//  SDTAlertDataViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTAlertDataViewController.h"

@interface SDTAlertDataViewController ()

@end

@implementation SDTAlertDataViewController
UITextField *targetPrcTextField;
UITextField *targetChgTextField;
UITextField *targetChgPctTextField;
UIAlertView *alertWindow;
CGPoint offset;
CGFloat mainTableHeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    alert = [[SDTAlert alloc] init];
    
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    
    [self initializeFormTextFields];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    alertWindow = [[UIAlertView alloc]initWithTitle:@"Add to Stock Alerts" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // For Adjusting TableView when Keyboard Shows
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma Tableview operations

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Just show the lable
    if(section == 0){
        return 0;
    } else {
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    
    switch (section) {
        case 0:
            title = @"Notify me if the stock hits either of the following criteria!";
            break;
        case 1:
            title = @"Target Price";
            break;
        case 2:
            title = @"Target Chg";
            break;
        case 3:
            title = @"Target Chg Pct";
            break;
    }
    return title;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 70.0f;
    } else {
        return 30.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"AlertDataCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    switch (indexPath.section) {
        case 1:
            [cell.contentView addSubview:targetPrcTextField];
            break;
        case 2:
            [cell.contentView addSubview:targetChgTextField];
            break;
        case 3:
            [cell.contentView addSubview:targetChgPctTextField];
            break;
        default:
            break;
    }
    
    return cell;
}


#pragma mark -
#pragma notifications

- (IBAction)save:(id)sender {
    NSLog(@"Save Alert Data clicked!");
    
    self.stockSymbol = ([[self.symbol componentsSeparatedByString:@"."] count] > 1) ? [[self.symbol componentsSeparatedByString:@"."] firstObject] : self.symbol;
    
    NSInteger targetPriceFlag = ([self.currPrice doubleValue] <= [[targetPrcTextField text] doubleValue]) ?  1 :  0 ;
    NSInteger targetChgFlag = ([self.currChg doubleValue] <= [[targetChgTextField text]  doubleValue]) ?  1 :  0 ;
    NSInteger targetChgPctFlag = ([self.currChgPct doubleValue] <= [[targetChgPctTextField text] doubleValue]) ?  1 :  0 ;
    
    NSLog(@"stockSymbol is %@", self.stockSymbol);
    NSLog(@"YahooSymNExch is %@", self.symbol);
    NSLog(@"stockName is %@", self.stockName);
    NSLog(@"googleSymbol is %@", self.googleSymbol);
    NSLog(@"exchName is %@", self.exchName);
    NSLog(@"targetPrice is %@", [targetPrcTextField text]);
    NSLog(@"targetChg is %@", [targetChgTextField text]);
    NSLog(@"targetChgPct is %@", [targetChgPctTextField text]);
    NSLog(@"targetPriceFlag is %li", (long)targetPriceFlag);
    NSLog(@"targetChgFlag is %li", (long)targetChgFlag);
    NSLog(@"targetChgPctFlag is %li", (long)targetChgPctFlag);
    
    // add the data to the alerts Table
    
    if([self alertValidation]){
        if([alert addToStockAlerts:self.stockSymbol stockName:self.stockName targetPrice:[targetPrcTextField text] targetChg:[targetChgTextField text] targetChgPct:[targetChgPctTextField text] exchName:self.exchName yahooSymNExch:self.symbol googleSymbol:self.googleSymbol targetPrcFlag:targetPriceFlag targetChgFlag:targetChgFlag targetChgPctFlag:targetChgPctFlag notes:@""]){
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}

#pragma mark -
#pragma helper functions

-(void) initializeFormTextFields
{
    targetPrcTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    targetChgTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    targetChgPctTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    
    targetPrcTextField.adjustsFontSizeToFitWidth = YES;
    targetPrcTextField.placeholder = [NSString stringWithFormat:@"Current Value: %@", self.currPrice];
    targetPrcTextField.backgroundColor = [UIColor whiteColor];
    targetPrcTextField.keyboardType = UIKeyboardTypeDecimalPad;
    targetPrcTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
    
    targetChgTextField.adjustsFontSizeToFitWidth = YES;
    targetChgTextField.placeholder = [NSString stringWithFormat:@"Current Value: %@", self.currChg];
    targetChgTextField.backgroundColor = [UIColor whiteColor];
    targetChgTextField.keyboardType = UIKeyboardTypeDecimalPad;
    targetChgTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
    
    targetChgPctTextField.adjustsFontSizeToFitWidth = YES;
    targetChgPctTextField.placeholder = [NSString stringWithFormat:@"Current Value: %@", self.currChgPct];
    targetChgPctTextField.backgroundColor = [UIColor whiteColor];
    targetChgPctTextField.keyboardType = UIKeyboardTypeDecimalPad;
    targetChgPctTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
}

// True: Good; False: Error
-(BOOL) alertValidation
{
    BOOL validate = true;
    if(([[targetPrcTextField text] length] == 0) && ([[targetChgTextField text] length] == 0) && ([[targetChgPctTextField text] length] == 0)) {
        validate = false;
        alertWindow.message = @"All Fields cannot be Empty";
        [alertWindow show];
    }
    
    return validate;
}

#pragma mark -
#pragma notifications

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    offset = self.mainTable.contentOffset;
    mainTableHeight = self.mainTable.frame.size.height;
    CGRect viewFrame = self.mainTable.frame;
    viewFrame.size.height -= keyboardSize.height;
    self.mainTable.frame = viewFrame;
    
    CGRect textFieldRect = [targetChgPctTextField frame];
    textFieldRect.origin.y += 10;
    [self.mainTable scrollRectToVisible:textFieldRect animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainTable.frame = CGRectMake(0, 0, 320, mainTableHeight);
    self.mainTable.contentOffset = offset;
    self.mainTable.scrollIndicatorInsets = contentInsets;
    
    [self.mainTable scrollRectToVisible:targetChgPctTextField.frame animated:YES];
}

@end
