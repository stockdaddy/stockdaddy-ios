//
//  SDTStockNewsWebViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/17/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockNewsWebViewController.h"

@interface SDTStockNewsWebViewController ()

@end

@implementation SDTStockNewsWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.webURL]];
    [self.webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
