//
//  SDTAlertListNewViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTAlert.h"
#import "SDTAlertsCell.h"
#import "SDTNotification.h"
#import "SDTTickerQuote.h"

@interface SDTAlertListNewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    SDTAlert *alertData;
    SDTNotification *notificationInfo;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *noDataCont;

- (BOOL) checkForNotification:(NSArray*)googleData;
- (void) showNotification;

@end
