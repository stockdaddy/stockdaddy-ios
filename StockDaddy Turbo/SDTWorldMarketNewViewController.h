//
//  SDTWorldMarketNewViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/14/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTTickerQuote.h"
#import "SDTWatchlistCell.h"

@interface SDTWorldMarketNewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    SDTTickerQuote *tickerQuote;
}
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *loadingCont;

@end
