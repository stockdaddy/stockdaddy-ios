//
//  SDTPageViewContentViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/29/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTPageViewContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property NSUInteger pageIndex;
@property NSString *imageFile;

@end
