//
//  SDTAlertDataViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTAlert.h"

@interface SDTAlertDataViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    SDTAlert *alert;
}

@property (nonatomic, strong) NSString *stockSymbol;
@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, strong) NSString *stockName;
@property (nonatomic, strong) NSString *googleSymbol;
@property (nonatomic, strong) NSString *exchName;

@property (nonatomic, strong) NSString *currPrice;
@property (nonatomic, strong) NSString *currChg;
@property (nonatomic, strong) NSString *currChgPct;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
- (IBAction)save:(id)sender;

@end
