//
//  SDTPortfolioHomeNewViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTPortfolio.h"
#import "SDTDashboardViewController.h"
#import "SDTPorfolioCell.h"
#import "SDTNewPortfolioViewController.h"

@interface SDTPortfolioHomeNewViewController : UIViewController  <UITableViewDataSource, UITableViewDelegate> {
    SDTPortfolio *portfolio;
}


@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *noPortfolioCont;

@end
