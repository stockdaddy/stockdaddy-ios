//
//  SDTExchangeInfo.m
//  StockDaddy Turbo
//
//  Created by Jain on 3/10/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTExchangeInfo.h"

@interface SDTExchangeInfo()

// Private Method
- (NSArray*) getExchangeList;

@end

static NSString *databasePath;
static sqlite3 *stockDaddyDB;

@implementation SDTExchangeInfo

- (id)init
{
    self = [super init];
    if(self){
    }
    
    return self;
}

#pragma mark -
#pragma mark Initializing Database

- (void)initDatabase
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS ExchangeInfo (Region VARCHAR, ExchangeName VARCHAR, YahooSymbol VARCHAR, GoogleSymbol VARCHAR, UNIQUE(Region, ExchangeName, YahooSymbol, GoogleSymbol))";
            
            if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to create ExchangeInfo table");
            }
            // Insert the Stocks to the DB
            [self insertExchangesList];
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
}

#pragma mark -
#pragma mark Inserting values

- (void) insertExchangesList
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        
        char *errMsg;
        NSArray *exchangeList = [self getExchangeList];
        
        sqlite3_exec(stockDaddyDB, "BEGIN TRANSACTION", 0, 0, 0);
        
        for (NSString* insert_query in exchangeList) {
            const char *insert_stmt = [insert_query UTF8String];
            if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to Insert to ExchangeInfo table because of %s", errMsg);
            }
        }
        
        sqlite3_exec(stockDaddyDB, "COMMIT TRANSACTION", 0, 0, 0);
        sqlite3_close(stockDaddyDB);
        NSLog(@"Finished Inserting ExchangeInfo To the DB");
        
    } else { NSLog(@"Failed to open/create database"); }    
}


#pragma mark Exchange List
- (NSArray*) getExchangeList
{
    NSArray *exchangeList = @
    [@"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES(\"North America\", \"America Stock Exchange\", \"\", \"NYSEMKT\")",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'BATS Exchange', '', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'Chicago Board of Trade', 'CBT', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'Chicago Mercantile Exchange', 'CME', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'Dow Jones Indexes', '', 'INDEXDJX')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'NASDAQ Stock Exchange', '', 'NASDAQ')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'New York Board of Trade', 'NYB', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'New York Commodities Exchange', 'CMX', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'New York Mercantile Exchange', 'NYM', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'New York Stock Exchange', '', 'NYSE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'OTC Bulletin Board Market', 'OB', 'OTCBB')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'Pink Sheets', 'PK', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'S&P Indices', '', 'INDEXSP')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'Toronto Stock Exchange', 'TO', 'TSE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('North America', 'TSX Venture Exchange', 'V', 'CVE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Shanghai Stock Exchange', 'SS', 'SHA')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Shenzhen Stock Exchange', 'SZ', 'SHE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Hong Kong Stock Exchange', 'HK', 'HKG')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'National Stock Exchange of India', 'NS', 'NSE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Jakarta Stock Exchange', 'JK', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Nikkei Indices', '', 'INDEXNIKKEI')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Singapore Stock Exchange', 'SI', 'SGX')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Korea Stock Exchange', 'KS', 'KRX')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'KOSDAQ', 'KQ', 'KOSDAQ')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Taiwan OTC Exchange', 'TWO', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Asia-Pacific', 'Taiwan Stock Exchange', 'TW', 'TPE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Vienna Stock Exchange', 'VI', 'VIE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Copenhagen Stock Exchange', 'CO', 'CPH')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Euronext', 'NX', 'INDEXEURO')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Paris Stock Exchange', 'PA', 'EPA')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Berlin Stock Exchange', 'BE', 'ETR')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Bremen Stock Exchange', 'BM', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Dusseldorf Stock Exchange', 'DU', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Frankfurt Stock Exchange', 'F', 'FRA')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Hamburg Stock Exchange', 'HM', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Hanover Stock Exchange', 'HA', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Munich Stock Exchange', 'MU', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Stuttgart Stock Exchange', 'SG', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'XETRA Stock Exchange', 'DE', 'ETR')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Milan Stock Exchange', 'MI', 'INDEXBIT')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Amsterdam Stock Exchange', 'AS', 'AMS')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Oslo Stock Exchange', 'OL', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Barcelona Stock Exchange', 'BC', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Bilbao Stock Exchange', 'BI', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Madrid Fixed Income Market', 'MF', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Madrid SE C.A.T.S.', 'MC', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Madrid Stock Exchange', 'MA', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Stockholm Stock Exchange', 'ST', 'STO')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'Swiss Exchange', 'SW', 'SWX')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'FTSE Indices', '', 'INDEXFTSE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Europe', 'London Stock Exchange', 'L', 'LON')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('South America', 'BOVESPA-Sao Paolo Stock Exchange', 'SA', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('South America', 'Santiago Stock Exchange', 'SN', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('South America', 'Mexico Stock Exchange', 'MX', '')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('South Pacific', 'Australian Stock Exchange', 'AX', 'ASX')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('South Pacific', 'New Zealand Stock Exchange', 'NZ', 'NZE')",
     @"INSERT OR IGNORE INTO ExchangeInfo (Region, ExchangeName, YahooSymbol, GoogleSymbol) VALUES('Multi-Exchange', 'Multi-Exchange', 'multi', 'multi')"];
    
    return exchangeList;
}

#pragma mark -
#pragma mark Search

- (BOOL) getGoogleSymbol:(NSString*)yahooSym withExchDisp:(NSString*)exchDisp withTypeDisp:(NSString*)typeDisp
{
    if([yahooSym length] != 0) {
        const char *dbpath = [databasePath UTF8String];
        sqlite3_stmt *statement;
        
        if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
            NSString *googQuery = [NSString stringWithFormat: @"SELECT GoogleSymbol FROM ExchangeInfo WHERE YahooSymbol=\"%@\"", yahooSym];
            const char *goog_stmt = [googQuery UTF8String];
            
            if (sqlite3_prepare_v2(stockDaddyDB, goog_stmt, -1, &statement, NULL) == SQLITE_OK) {
                if (sqlite3_step(statement) == SQLITE_ROW) {
                    self.googleSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                    if([self.googleSymbol length] == 0 ) return NO;
                } else { return NO; }
                sqlite3_finalize(statement);
            } else { NSLog(@"Get GoogleSymbol Query Failed! because of %s", sqlite3_errmsg(stockDaddyDB)); return NO; }
        }
        sqlite3_close(stockDaddyDB);
        
        return YES;
    }
    else {
        return [self infoForUSMarket:exchDisp withTypeDisp:typeDisp];
    }
}

- (BOOL) getExchangeName:(NSString*)yahooSym withExchDisp:(NSString*)exchDisp withTypeDisp:(NSString*)typeDisp
{
    if([yahooSym length] != 0) {
        const char *dbpath = [databasePath UTF8String];
        sqlite3_stmt *statement;
        
        if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
            NSString *exchQuery = [NSString stringWithFormat: @"SELECT ExchangeName FROM ExchangeInfo WHERE YahooSymbol=\"%@\"", yahooSym];
            const char *exch_stmt = [exchQuery UTF8String];
            
            if (sqlite3_prepare_v2(stockDaddyDB, exch_stmt, -1, &statement, NULL) == SQLITE_OK) {
                
                if (sqlite3_step(statement) == SQLITE_ROW) {
                    self.exchangeName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                    if([self.exchangeName length] == 0 ) return NO;
                } else { return NO; }
                sqlite3_finalize(statement);
            } else {  NSLog(@"Get ExchangeName Query Failed Because %s", sqlite3_errmsg(stockDaddyDB)); return NO;  }
        }
        sqlite3_close(stockDaddyDB);
        
        return YES;
    }
    else {
        return [self infoForUSMarket:exchDisp withTypeDisp:typeDisp];;
    }
}

// Will be called when storing a new portfolio
- (NSString*) getYahooSymbol:(NSString*)exchName region:(NSString*)region
{
    NSString *yahooSymbol;
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT YahooSymbol FROM ExchangeInfo WHERE Region=\"%@\" AND ExchangeName=\"%@\"", region, exchName];
        const char *select_stmt = [query UTF8String];

        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            if (sqlite3_step(statement) == SQLITE_ROW) {
                yahooSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getYahooSymbol Query Failed!");
        }
        sqlite3_close(stockDaddyDB);
    }
    
    return yahooSymbol;
}

// Will be called when storing a new portfolio
- (NSString*) getGoogleSymbol:(NSString*)exchName region:(NSString*)region
{
    NSString *googleSymbol;
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT GoogleSymbol FROM ExchangeInfo WHERE Region=\"%@\" AND ExchangeName=\"%@\"", region, exchName];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            if (sqlite3_step(statement) == SQLITE_ROW) {
                googleSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getYahooSymbol Query Failed!");
        }
        sqlite3_close(stockDaddyDB);
    }
    
    return googleSymbol;
}


# pragma mark -
# pragma mark helper methods

- (BOOL) infoForUSMarket:(NSString*)exchDisp withTypeDisp:(NSString*)typeDisp
{
    if([exchDisp isEqualToString:@"NASDAQ"]) {
        self.googleSymbol = @"NASDAQ";
        self.exchangeName = @"NASDAQ Stock Exchange";
        return [self checkForTypeDisp:typeDisp withExchDisp:exchDisp];
    } else if ([exchDisp isEqualToString:@"AMEX"]) {
        self.googleSymbol = @"NYSEMKT";
        self.exchangeName = @"American Stock Exchange";
        return [self checkForTypeDisp:typeDisp withExchDisp:exchDisp];
    } else {
            self.googleSymbol = @"NYSE";
            self.exchangeName = @"New York Stock Exchange";
            return [self checkForTypeDisp:typeDisp withExchDisp:exchDisp];
    }
    
    return NO;
}

- (BOOL) checkForTypeDisp:(NSString*)typeDisp withExchDisp:(NSString*)exchDisp
{
    // Redo. NEed more data to check for ETF from different Exchanges
    if([typeDisp  isEqualToString: @"ETF"]) {
        self.googleSymbol = @"NYSEARCA";
        self.exchangeName = @"New York Stock Exchange";
    }
    
    if([exchDisp isEqualToString:@"OTC Markets"]){
        self.googleSymbol = @"OTCMKTS";
        self.exchangeName = @"OTC Markets";
    }

    return YES;
}

@end
