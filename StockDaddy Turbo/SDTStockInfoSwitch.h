//
//  SDTStockInfoSwitch.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTStockInfoSwitch : UISwitch

@property (nonatomic, assign) NSInteger indexPathRow;

@end
