//
//  SDTTransactionHistoryViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTStockInfoViewController.h"
#import "SDTStock.h"

@interface SDTTransactionHistoryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    SDTStock *stockInfo;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, strong) NSString *portfolioID;
@property (nonatomic, strong) NSMutableArray *transHistoryData;

@property (strong, nonatomic) UIView *moveView;

@end
