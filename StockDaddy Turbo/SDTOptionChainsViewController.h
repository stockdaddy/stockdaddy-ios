//
//  SDTOptionChainsViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTOptionChainCell.h"
#import "SDTTickerQuote.h"

@interface SDTOptionChainsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *symbol;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UITableView *optionDataTable;

@property (weak, nonatomic) IBOutlet UIView *noDataCont;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@end
