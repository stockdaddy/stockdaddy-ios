//
//  SDTTransactionHistoryViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTTransactionHistoryViewController.h"

@interface SDTTransactionHistoryViewController ()

@end

@implementation SDTTransactionHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    // hide the tab bar
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    self.navigationItem.title = @"Transaction History";
    
    stockInfo = [[SDTStock alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma TableView properties

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.transHistoryData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TransactionHistoryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"TransactionHistoryCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.textLabel.text = [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"StockName"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@:%@", [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"GoogleExchange"], [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
    
    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo BOLD" size:15.0f];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDTStockInfoViewController *stockInfoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StockInfoVC"];
    stockInfoVC.portfolioID = self.portfolioID;
    stockInfoVC.stockID = [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"StockID"];
    stockInfoVC.stockName = [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"StockName"];
    
    [self.navigationController pushViewController:stockInfoVC animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *stockID = [[self.transHistoryData objectAtIndex:indexPath.row] objectForKey:@"StockID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [stockInfo deleteFromStockTable:stockID]) {
        // Delete the row from the data source
        [self.transHistoryData removeObjectAtIndex:indexPath.row];
        [self.mainTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark -
#pragma gesture

@end
