//
//  SDTNotification.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTNotification.h"

@implementation SDTNotification

static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
    }
    
    return self;
}

#pragma mark -
#pragma db operations

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Notification ("
        "                NotificationID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 StockName VARCHAR, "
        "				 StockSymbol VARCHAR, "
        "				 TargetVal VARCHAR, "
        "				 TargetValFlag VARCHAR, "
        "                CurrentPrice VARCHAR, "
        "                CurrentChg VARCHAR, "
        "                CurrentChgPct VARCHAR, "
        "                NotifDate DATETIME)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create Notification table because of %s", errMsg);
        }
        [self closeDataBase];
        
    } else {
        NSLog(@"Failed to open/create database");
    }
}

// return Yes: Succesfully Added to Notification Table; NO: Can't add to Notification Table
- (BOOL) addToNotification:(NSString*)symbol stockName:(NSString*)stockName targetVal:(NSString*)targetVal targetValFlag:(NSString*)targetValFlag currentPrice:(NSString*)cPrice currentChg:(NSString*)cChg currentChgPct:(NSString*)cChgPct {
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO Notification (StockName, StockSymbol, TargetVal, TargetValFlag, CurrentPrice, CurrentChg, CurrentChgPct, NotifDate) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", current_timestamp)", stockName, symbol, targetVal, targetValFlag, cPrice, cChg, cChgPct];
        const char *insert_stmt = [insert_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Insert to Notification table because of %s", errMsg);
            [self closeDataBase];
            return NO;
        }
        [self closeDataBase];
        return YES;
    } else { NSLog(@"Failed to open/create database"); return NO; }
}

// returns array of dictionaries of all the notifications in the table
- (NSMutableArray*) getAllNotifications
{
    NSMutableArray *notifData = [[NSMutableArray alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT * FROM Notification ORDER BY NotificationID DESC"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                
                NSString *notifID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                [tempDictionary setObject:notifID forKey:@"notificationID"];
                
                NSString *stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                [tempDictionary setObject:stockName forKey:@"stockName"];
                
                NSString *stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                [tempDictionary setObject:stockSymbol forKey:@"stockSymbol"];
                
                NSString *targetVal = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                [tempDictionary setObject:targetVal forKey:@"TargetVal"];
                
                NSString *targetValFlag = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                [tempDictionary setObject:targetValFlag forKey:@"TargetValFlag"];
                
                NSString *cPrice = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                [tempDictionary setObject:cPrice forKey:@"CurrentPrice"];
                
                NSString *cChg = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)];
                [tempDictionary setObject:cChg forKey:@"CurrentChg"];
                
                NSString *cChgPct = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)];
                [tempDictionary setObject:cChgPct forKey:@"CurrentChgPct"];
                
                NSString *notifDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 8)];
                [tempDictionary setObject:notifDate forKey:@"NotifDate"];
                
                [notifData addObject:tempDictionary];
            }
            
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getAllNotifications Query Failed!");
        }
        [self closeDataBase];
    } else { NSLog(@"Error Opening the DB for Notification!"); }
    
    return notifData;
}

// return YES: Succesfully Deleted; NO: Error
- (BOOL) deleteFromNotifTable:(NSString*)notifID
{
    BOOL deleted = YES;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM Notification WHERE NotificationID=\"%@\"", notifID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From Notification table because of %s", errMsg);
            [self closeDataBase];
            deleted = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); deleted = NO; }
    
    return deleted;
}

- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

@end