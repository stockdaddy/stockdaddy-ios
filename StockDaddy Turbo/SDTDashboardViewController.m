//
//  SDTDashboardViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTDashboardViewController.h"

@interface SDTDashboardViewController ()

@end

@implementation SDTDashboardViewController

#define defaultViewWidth 320.0f
#define halfViewWidth 160.0f
#define iPhone5Container1YPosition 0
#define iPhone5ContainerHeight 118
#define iPhone5LabelViewHeight 44
#define iPhone4ContainerHeight 100


NSMutableArray *googleData;
BOOL isMainViewClicked;
NSInteger viewTag;
NSMutableArray *mainTableDS;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    googleData = [[NSMutableArray alloc] init];
    mainTableDS = [[NSMutableArray alloc] init];
    portfolioObj = [[SDTPortfolio alloc] init];
    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"GooglePortfolioStats"];
    [tickerQuote googleTickerQuote:[portfolioObj googleRequestURLSymList:self.portfolioID]];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(googleReturnedData:)
                                                 name: @"GooglePortfolioStats"
                                               object: nil];
    
    // hide the tab bar
    self.tabBarController.tabBar.hidden = YES;

    [self.mainTable reloadData];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // hide the tab bar
    self.tabBarController.tabBar.hidden = NO;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = self.portfolioName;
    
    UILabel *tranLabel = [[UILabel alloc] init];
    tranLabel.font = [UIFont fontWithName:@"FontAwesome" size:20.0];
    tranLabel.text = @"\uf0c9";
    
    UIBarButtonItem *optionBarItem = [[UIBarButtonItem alloc] initWithTitle:@"\uf0c9"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(portOptions)];
    
    [optionBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"FontAwesome" size:20.0]} forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = optionBarItem;

    isMainViewClicked = NO;
    
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    
    viewTag = 0;
    // Add Gestures to the view. Views differentiated by different tag IDs.
    // tag 1
    [self.mainView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 2
    [self.maxGainerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 3
    [self.maxLoserView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 4
    [self.todaysGainView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 5
    [self.currentGainView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 6
    [self.totalInvestmentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    // tag 7
    [self.portfolioGainView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)]];
    
    //Check for iphone 4 or iphone 5 to adjust the view
    [self checkForiPhoneScreensize];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma Helper Functions

- (void) googleReturnedData:(NSNotification *)notification
{
    [googleData removeAllObjects];
    if(!googleData){
        googleData = [[NSMutableArray alloc] init];
    }
    NSLog(@"recieved GooglePortfolioStats Data");
    googleData = [notification object];
    portfolioObj.googleData = googleData;

    NSLog(@"Total Investment is %@", [portfolioObj portfolioTotalInvestment:self.portfolioID]);
    NSLog(@"Total Networth is %@", [portfolioObj portfolioNetWorth:self.portfolioID]);
    NSLog(@"Todays Gain is %@", [portfolioObj portfolioTodaysGain:self.portfolioID]);
    NSLog(@"Current Gain is %@", [portfolioObj portfolioCurrentGain:self.portfolioID]);
    NSLog(@"Current Gain Pct is %@", [portfolioObj portfolioCurrentGainPct:self.portfolioID]);
    NSLog(@"Max Gainer is %@", [portfolioObj portfolioMaxGainer:self.portfolioID]);
    NSLog(@"Max Loser is %@", [portfolioObj portfolioMaxLoser:self.portfolioID]);
    
    // Remove as observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"GooglePortfolioStats"
                                                  object: nil];
    
    // update the Labels on the dashboard
    if(googleData.count!=0){
        NSString *stockSymMax = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"StockSymbol"];
        NSString *maxMinPrcMax = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"Price"];
        NSString *maxMinChgMax = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"Change"];
        NSString *maxMinChgPctMax = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"ChangePct"];
        
        NSString *stockSymMin = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"StockSymbol"];
        NSString *maxMinPrcMin = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"Price"];
        NSString *maxMinChgMin = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"Change"];
        NSString *maxMinChgPctMin = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"ChangePct"];
        
        
        self.mainHeaderValLabel.text = [portfolioObj portfolioNetWorth:self.portfolioID];
        self.mainHeaderValChgL.text = @"";
        self.lastUpdatedLabel.text = [NSString stringWithFormat:@"Last updated: %@",[[googleData objectAtIndex:0] objectForKey:@"lt"]];
        
        // Max Gainer
        self.maxGainerNameL.text = ([stockSymMax length]==0) ? @"-" : stockSymMax;
        self.maxGainerValL.text = ([maxMinPrcMax length]==0) ? @"-" : maxMinPrcMax;
        self.maxGainerChgL.text = ([maxMinChgMax length]==0) ? @"" : [NSString stringWithFormat:@"%@ (%@ %%)", maxMinChgMax, maxMinChgPctMax];
        
        // Max Loser
        self.maxLoserNameL.text = ([stockSymMin length]==0) ? @"-" : stockSymMin;
        self.maxLoserValL.text = ([maxMinPrcMin length]==0) ? @"-" : maxMinPrcMin;
        self.maxLoserChgL.text = ([maxMinChgMin length]==0) ? @"" : [NSString stringWithFormat:@"%@ (%@ %%)", maxMinChgMin, maxMinChgPctMin];
        
        // Todays Gain
        self.todaysGainValL.text = [portfolioObj portfolioTodaysGain:self.portfolioID];
        
        // Current Gain
        self.currentGainValL.text = [portfolioObj portfolioCurrentGain:self.portfolioID];
        self.currentGainChgL.text = [NSString stringWithFormat:@"%@ %%", [portfolioObj portfolioCurrentGainPct:self.portfolioID]];
        
        // Total Investment
        self.totalInvestmentValL.text = [portfolioObj portfolioTotalInvestment:self.portfolioID];
        
        // Portfolio gainLoss
        self.portfolioGainValL.text = [portfolioObj portfolioGainLoss:self.portfolioID];
        self.portfolioGainChgL.text = [NSString stringWithFormat:@"%@ %%", [portfolioObj portfolioGainLossPct:self.portfolioID]];
    }
}


#pragma mark - 
#pragma view tap gestures

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSInteger tagID = (long)recognizer.view.tag;
    switch (tagID) {
        case 1:
            NSLog(@"My Networth Clicked");
            
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];            
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            
            self.fourLabelTitleView.hidden = NO;
            self.fiveLabelTitleView.hidden = YES;
            self.mainViewHeaderLabel.text = @"Net Worth";
            self.mainHeaderValLabel.text = [portfolioObj portfolioNetWorth:self.portfolioID];
            self.mainHeaderValChgL.text = @"";
            self.tableHeaderLabelOne.text = @"Last\rPrice";
            self.tableHeaderLabeltwo.text = @"Buy\rQty";
            self.tableHeaderLabelThree.text = @"Net\rWorth";
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 1;
            mainTableDS = [portfolioObj portfolioNetworthDS:self.portfolioID];
            [self.mainTable reloadData];
            break;
            
        case 2:
            NSLog(@"Max Gainer Clicked");
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            [self updateMaxGainView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 2;
            break;
            
        case 3:
            NSLog(@"Max loser Clicked");
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 3;
            break;
            
        case 4:
            NSLog(@"Todays Gain Clicked");
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 4;
            break;
            
        case 5:
            NSLog(@"Current Gain");
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 5;
            break;
            
        case 6:
            NSLog(@"Total Investment Clicked");
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 6;
            break;
            
        case 7:
            NSLog(@"Portfolio Gain");
            [self updateMaxGainView: NO hideView:!isMainViewClicked];
            [self updateMaxLoserView: NO hideView:!isMainViewClicked];
            [self updateTodaysGainView: NO hideView:!isMainViewClicked];
            [self updateCurrentGainView: NO hideView:!isMainViewClicked];
            [self updateTotalInvestmentView: NO hideView:!isMainViewClicked];
            [self updatePortfolioGainView: YES hideView:!isMainViewClicked];
            
            isMainViewClicked = !isMainViewClicked;
            viewTag = 7;
            break;
            
        default:
            break;
    }
}

//Tag 2
-(void)updateMaxGainView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.maxGainerView duration:0.4 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.maxGainerView duration:0.9 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.maxGainerView.hidden = hide;
    
    // update table header Titles
    if(isClicked){
        self.fourLabelTitleView.hidden = NO;
        self.fiveLabelTitleView.hidden = YES;
        self.mainViewHeaderLabel.text = @"Max Gainer";
        
        NSString *stockSym = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"StockSymbol"];
        NSString *val = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"Price"];
        NSString *chg = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"Change"];
        NSString *chgPct = [[portfolioObj portfolioMaxGainer:self.portfolioID] objectForKey:@"ChangePct"];

        self.mainHeaderValLabel.text = ([stockSym length]==0) ? @"-" : stockSym;
        self.mainHeaderValChgL.text = ([val length]==0) ? @"-" : [NSString stringWithFormat:@"%@ %@ (%@ %%)", val, chg, chgPct];
        self.tableHeaderLabelOne.text = @"Last\rPrice";
        self.tableHeaderLabeltwo.text = @"Last\rChg";
        self.tableHeaderLabelThree.text = @"Last\rChg %";
        mainTableDS = (NSMutableArray*)[portfolioObj portfolioMaxMinDS:self.portfolioID sortAscending:NO];
        [self.mainTable reloadData];
    }
}

//Tag 3
-(void)updateMaxLoserView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.maxLoserView duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.maxLoserView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.maxLoserView.hidden = hide;
    
    if(isClicked){
        self.fourLabelTitleView.hidden = NO;
        self.fiveLabelTitleView.hidden = YES;
        self.mainViewHeaderLabel.text = @"Max Loser";

        NSString *stockSym = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"StockSymbol"];
        NSString *val = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"Price"];
        NSString *chg = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"Change"];
        NSString *chgPct = [[portfolioObj portfolioMaxLoser:self.portfolioID] objectForKey:@"ChangePct"];
        
        self.mainHeaderValLabel.text = ([stockSym length]==0) ? @"-" : stockSym;
        self.mainHeaderValChgL.text = ([val length]==0) ? @"-" : [NSString stringWithFormat:@"%@ %@ (%@ %%)", val, chg, chgPct];
        self.tableHeaderLabelOne.text = @"Last\rPrice";
        self.tableHeaderLabeltwo.text = @"Last\rChg";
        self.tableHeaderLabelThree.text = @"Last\rChg %";
        mainTableDS = (NSMutableArray*)[portfolioObj portfolioMaxMinDS:self.portfolioID sortAscending:YES];
        [self.mainTable reloadData];
    }
}


//Tag 4
-(void)updateTodaysGainView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.todaysGainView duration:0.6 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.todaysGainView duration:0.7 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.todaysGainView.hidden = hide;
    
    if(isClicked){
        self.fourLabelTitleView.hidden = NO;
        self.fiveLabelTitleView.hidden = YES;
        self.mainViewHeaderLabel.text = @"Todays Gain";
        self.mainHeaderValLabel.text = [portfolioObj portfolioTodaysGain:self.portfolioID];
        self.mainHeaderValChgL.text = @"";
        self.tableHeaderLabelOne.text = @"Last\rChg";
        self.tableHeaderLabeltwo.text = @"Buy\rQty";
        self.tableHeaderLabelThree.text = @"Todays\rGain";
        mainTableDS = [portfolioObj portfolioTodaysGainDS:self.portfolioID];
        [self.mainTable reloadData];
    }
}

//Tag 5
-(void)updateCurrentGainView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.currentGainView duration:0.7 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.currentGainView duration:0.6 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.currentGainView.hidden = hide;
    
    if(isClicked) {
        self.fourLabelTitleView.hidden = YES;
        self.fiveLabelTitleView.hidden = NO;
        self.mainViewHeaderLabel.text = @"Current Gain";
        self.mainHeaderValLabel.text = [portfolioObj portfolioCurrentGain:self.portfolioID];
        self.mainHeaderValChgL.text = [NSString stringWithFormat:@"%@ %%",[portfolioObj portfolioCurrentGainPct:self.portfolioID]];
        self.tableHeaderFiveSymbolLabel.text = @"Symbol";
        self.tableHeaderFiveOne.text = @"Last\rPrc";
        self.tableHeaderFiveTwo.text = @"Buy\rPrc";
        self.tableHeaderFiveThree.text = @"Buy\rQty";
        self.tableHeaderFiveFour.text = @"G\\L";
        mainTableDS = [portfolioObj portfolioCurrentGainDS:self.portfolioID];
        [self.mainTable reloadData];
    }
}

//Tag 6
-(void)updateTotalInvestmentView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.totalInvestmentView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.totalInvestmentView duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.totalInvestmentView.hidden = hide;
    
    if(isClicked){
        self.fourLabelTitleView.hidden = NO;
        self.fiveLabelTitleView.hidden = YES;
        self.mainViewHeaderLabel.text = @"Total Investment";
        self.mainHeaderValLabel.text = [portfolioObj portfolioTotalInvestment:self.portfolioID];
        self.mainHeaderValChgL.text = @"";
        self.tableHeaderLabelOne.text = @"Buy\rPrice";
        self.tableHeaderLabeltwo.text = @"Buy\rQty";
        self.tableHeaderLabelThree.text = @"Total\rInvest";
        mainTableDS = [portfolioObj portfolioTotalInvestmentDS:self.portfolioID];
        [self.mainTable reloadData];
    }
}

//Tag 7
-(void)updatePortfolioGainView:(BOOL)isClicked hideView:(BOOL)hide
{
    // Animation
    (hide) ? [UIView transitionWithView:self.portfolioGainView duration:0.9 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL]
    : [UIView transitionWithView:self.portfolioGainView duration:0.4 options:UIViewAnimationOptionTransitionFlipFromRight animations:NULL completion:NULL];
    
    self.portfolioGainView.hidden = hide;
    
    if(isClicked){
        self.fourLabelTitleView.hidden = YES;
        self.fiveLabelTitleView.hidden = NO;
        self.mainViewHeaderLabel.text = @"Portfolio Gain";
        self.mainHeaderValLabel.text = [portfolioObj portfolioGainLoss:self.portfolioID];
        self.mainHeaderValChgL.text =  [NSString stringWithFormat:@"%@ %%", [portfolioObj portfolioGainLossPct:self.portfolioID]];
        self.tableHeaderFiveSymbolLabel.text = @"Symbol";
        self.tableHeaderFiveOne.text = @"Buy\rPrice";
        self.tableHeaderFiveTwo.text = @"Sell\rPrice";
        self.tableHeaderFiveThree.text = @"Sell\rQty";
        self.tableHeaderFiveFour.text = @"G\\L";
        mainTableDS = [portfolioObj portfolioGainLossDS:self.portfolioID];
        [self.mainTable reloadData];
    }
}

#pragma mark -
#pragma mark Table View Operations

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mainTableDS count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    if(viewTag==5 || viewTag==7){
        CellIdentifier = @"DashboardFiveTableCell";
    } else {
        CellIdentifier = @"DashboardTableCell";
    }

    if(mainTableDS.count != 0) {
        switch (viewTag) {
            case 1:
            {
                SDTDashboardFourLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPrice"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyQuantity"]];
                [cell.labelThree setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"Networth"]];
                return cell;
                break;
            }
            case 2:
            case 3:
            {
                SDTDashboardFourLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPrice"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPriceChg"]];
                [cell.labelThree setText:[NSString stringWithFormat:@"%@ %%",[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPriceChgPct"]]];

                return cell;
                break;
            }
                
            case 4:
            {
                SDTDashboardFourLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPriceChg"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyQuantity"]];
                [cell.labelThree setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"TodaysGain"]];
                
                return cell;
                break;
            }
                
            case 6:
            {
                SDTDashboardFourLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyPrice"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyQuantity"]];
                [cell.labelThree setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"TotalInvestment"]];

                return cell;
                break;
            }
                
            case 5:
            {
                SDTDashboardFiveLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyName setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentPrice"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"AvgBuyPrice"]];
                [cell.labelThree setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyQuantity"]];
                [cell.labelFour setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"CurrentGain"]];

                return cell;
                break;
            }
            case 7:
            {
                SDTDashboardFiveLabelCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
                [cell.symbolLabel setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockSymbol"]];
                [cell.companyName setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"StockName"]];                
                [cell.labelOne setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"BuyPrice"]];
                [cell.labelTwo setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"SellPrice"]];
                [cell.labelThree setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"SellQuantity"]];
                [cell.labelFour setText:[[mainTableDS objectAtIndex:indexPath.row] objectForKey:@"GainLoss"]];
                
                return cell;
                break;
            }

                
        } // end of switch
    } // end of ds count if
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    SDTTickerQuoteViewController *quoteView = [self.storyboard instantiateViewControllerWithIdentifier:@"TickerInfo"];
    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooQuoteData"];
    
    NSDictionary *tempDictionary= [mainTableDS objectAtIndex:indexPath.row];
    NSString *symbol = [tempDictionary objectForKey:@"StockSymbol"];
    
    // initiate the Yahoo Search Request
    if(([symbol rangeOfString:@".BO"].location == NSNotFound) && ([symbol rangeOfString:@".NS"].location == NSNotFound)) {
        [tickerQuote yahooTickerQuote:symbol];
    }
    else {
        [tickerQuote indianTickerQuote:[tempDictionary objectForKey:@"stockSymbol"]];
    }
    
    // assign the company name from Searched Result to Quote View. Also pass the entire result so that
    // we can use to pass on the Dictionary to Ticker Options
    quoteView.companyName = [tempDictionary objectForKey:@"StockName"];;
    quoteView.symbol = symbol;
    quoteView.exchDisp = [tempDictionary objectForKey:@"ExchDisp"];
    quoteView.typDisp = [tempDictionary objectForKey:@"TypeDisp"];
    
    [self.navigationController pushViewController:quoteView animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma navigation

-(void) portOptions
{
    SDTTransactionHistoryViewController *transHis = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionHistory"];
    SDTStock *stockInfo = [[SDTStock alloc] init];
    transHis.transHistoryData = [stockInfo getStockInfo:self.portfolioID];
    transHis.portfolioID = self.portfolioID;
    
    [self.navigationController pushViewController:transHis animated:YES];
}


#pragma mark -
#pragma view for iPhone4

-(void) checkForiPhoneScreensize
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            [self updateViewForiPhone4];
        }
    }
}

-(void) updateViewForiPhone4
{
    NSLog(@"inside updateViewForiPhone4");
    
    // NetWorth
    [self.mainView removeFromSuperview];
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainView setFrame:CGRectMake(iPhone5Container1YPosition, 0, defaultViewWidth, 116)];
    [self.scrollView addSubview:self.mainView];
    
    //NetworthLabel
    [self.mainViewHeaderLabel removeFromSuperview];
    [self.mainViewHeaderLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainViewHeaderLabel setFrame:CGRectMake(9, 2, 282, 55)];
    self.mainViewHeaderLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:28.0f];
    [self.scrollView addSubview:self.mainViewHeaderLabel];

    //NetworthLabel Val
    [self.mainHeaderValLabel removeFromSuperview];
    [self.mainHeaderValLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainHeaderValLabel setFrame:CGRectMake(9, 40, 280, 41)];
    self.mainHeaderValLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:22.0f];
    [self.scrollView addSubview:self.mainHeaderValLabel];

    //NetworthLabel Val Chg
    [self.mainHeaderValChgL removeFromSuperview];
    [self.mainHeaderValChgL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainHeaderValChgL setFrame:CGRectMake(9, 65, 282, 40)];
    self.mainHeaderValChgL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:16.0f];
    [self.scrollView addSubview:self.mainHeaderValChgL];

    //last updated label
    [self.lastUpdatedLabel removeFromSuperview];
    [self.lastUpdatedLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.lastUpdatedLabel setFrame:CGRectMake(20, 95, 280, 21)];
    [self.scrollView addSubview:self.lastUpdatedLabel];

    //FourLabel
    [self.fourLabelTitleView removeFromSuperview];
    [self.fourLabelTitleView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.fourLabelTitleView setFrame:CGRectMake(iPhone5Container1YPosition, 116, defaultViewWidth, iPhone5LabelViewHeight)];
    [self.scrollView addSubview:self.fourLabelTitleView];

    //FiveLabel
    [self.fiveLabelTitleView removeFromSuperview];
    [self.fiveLabelTitleView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.fiveLabelTitleView setFrame:CGRectMake(iPhone5Container1YPosition, 116, defaultViewWidth, iPhone5LabelViewHeight)];
    [self.scrollView addSubview:self.fiveLabelTitleView];

    //mainTable
    [self.mainTable removeFromSuperview];
    [self.mainTable setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainTable setFrame:CGRectMake(iPhone5Container1YPosition, 160, defaultViewWidth, 256)];
    [self.scrollView addSubview:self.mainTable];

    //maxGainer
    [self.maxGainerView removeFromSuperview];
    [self.maxGainerView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxGainerView setFrame:CGRectMake(iPhone5Container1YPosition, 116, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.maxGainerView];

    [self.maxGainerNameL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxGainerNameL setFrame:CGRectMake(9, 39, 145, 30)];
    self.maxGainerNameL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:18.0f];
    
    [self.maxGainerValL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxGainerValL setFrame:CGRectMake(9, 57, 145, 30)];
    self.maxGainerValL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:14.0f];

    [self.maxGainerChgL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxGainerChgL setFrame:CGRectMake(9, 77, 145, 30)];
    self.maxGainerChgL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:14.0f];

    //maxLoser
    [self.maxLoserView removeFromSuperview];
    [self.maxLoserView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxLoserView setFrame:CGRectMake(halfViewWidth, 116, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.maxLoserView];

    [self.maxLoserNameL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxLoserNameL setFrame:CGRectMake(9, 39, 145, 30)];
    self.maxLoserNameL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:18.0f];

    [self.maxLoserValL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxLoserValL setFrame:CGRectMake(9, 57, 145, 30)];
    self.maxLoserValL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:14.0f];

    [self.maxLoserChgL setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.maxLoserChgL setFrame:CGRectMake(9, 77, 145, 30)];
    self.maxLoserChgL.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:14.0f];

    //todaysGain
    [self.todaysGainView removeFromSuperview];
    [self.todaysGainView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.todaysGainView setFrame:CGRectMake(iPhone5Container1YPosition, 216, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.todaysGainView];

    //currentGain
    [self.currentGainView removeFromSuperview];
    [self.currentGainView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.currentGainView setFrame:CGRectMake(halfViewWidth, 216, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.currentGainView];

    //totalInvestment
    [self.totalInvestmentView removeFromSuperview];
    [self.totalInvestmentView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.totalInvestmentView setFrame:CGRectMake(iPhone5Container1YPosition, 316, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.totalInvestmentView];

    //portfolioGain
    [self.portfolioGainView removeFromSuperview];
    [self.portfolioGainView setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.portfolioGainView setFrame:CGRectMake(halfViewWidth, 316, halfViewWidth, iPhone4ContainerHeight)];
    [self.scrollView addSubview:self.portfolioGainView];
}

@end
