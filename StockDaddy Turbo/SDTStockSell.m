//
//  SDTStockSell.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockSell.h"

@implementation SDTStockSell
static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
        stockInfo = [[SDTStock alloc] init];
    }
    
    return self;
}

#pragma mark -
#pragma db operations

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS StockSell ("
        "                StockSellID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 PortfolioID VARCHAR, "
        "				 StockBuyID VARCHAR, "
        "				 StockID VARCHAR, "
        "				 SellDate VARCHAR, "
        "				 SellQuantity VARCHAR, "
        "				 SellPrice VARCHAR, "
        "				 Notes VARCHAR)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create StockSell table because of %s", errMsg);
        }
        [self closeDataBase];
        
    } else {
        NSLog(@"Failed to open/create database");
    }
}

// return Yes: Succesfully Added to StockSell Table; NO: Can't add to StockBuy Table
- (BOOL) addToStockSellTable:(NSString*)portfolioID stockBuyID:(NSString*)stockBuyID stockID:(NSString*)stockID sellDate:(NSString*)sellDate sellQuantity:(NSString*)sellQty sellPrice:(NSString*)sellPrc notes:(NSString*)notes
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO StockSell (PortfolioID, StockBuyID, StockID, SellDate, SellQuantity, SellPrice, Notes) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", portfolioID, stockBuyID, stockID, sellDate, sellQty, sellPrc, notes];
        const char *insert_stmt = [insert_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Insert to StockBuy table because of %s", errMsg);
            [self closeDataBase];
            return NO;
        }
        [self closeDataBase];
        return YES;
    } else { NSLog(@"Failed to open/create database"); return NO; }
    
    return YES;
}


// Returns the list of all the Sell Quantity, Prices for BuyIDs.
// Called when calculating Dashboard Stats
// [{"SellQuantity":"10","SellPrice":"242.32", "StockID":"1"}, {...}]
- (NSMutableArray*) getStockSellInfoList:(NSString*)portfolioID
{
    NSMutableArray *stockSellList = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockSellID, StockBuyID, StockID, SellDate, SellQuantity, SellPrice FROM StockSell WHERE PortfolioID=\"%@\" ORDER BY StockSellID DESC", portfolioID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *sellID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *buyID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *stockID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                NSString *sellDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                NSString *sellQuan = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                NSString *sellPrice = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:sellID forKey:@"StockSellID"];
                [tempDictionary setObject:buyID forKey:@"StockBuyID"];
                [tempDictionary setObject:stockID forKey:@"StockID"];
                [tempDictionary setObject:sellDate forKey:@"SellDate"];
                [tempDictionary setObject:sellQuan forKey:@"SellQuantity"];
                [tempDictionary setObject:sellPrice forKey:@"SellPrice"];
                
                [stockSellList addObject:tempDictionary];
            }
            sqlite3_finalize(statement);
        }
        
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockSellList;
}

//Returns teh sell Qty for a buyid
// USed for Portfoilio stat calculations
- (NSString*) getSellQuantityForBuyID:(NSString*)buyID
{
    NSString *sellQuant = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT SellQuantity FROM StockSell WHERE StockBuyID=\"%@\"", buyID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                sellQuant = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"0";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return sellQuant;
}

//Returns teh sell Price for a buyid
// USed for Portfolio stat calculations
- (NSString*) getSellPriceForBuyID:(NSString*)buyID
{
    NSString *sellPrice = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT SellPrice FROM StockSell WHERE StockBuyID=\"%@\"", buyID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                sellPrice = ((const char *) sqlite3_column_text(statement, 0)) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] : @"0";
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return sellPrice;
}


- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

@end
