//
//  SDTFuturesViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTTickerQuote.h"
#import "SDTWatchlistCell.h"

@interface SDTFuturesViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate> {
    SDTTickerQuote *tickerQuote;
    
}

@property (nonatomic, strong) NSArray *futureSection;
@property (nonatomic, strong) NSMutableArray *yahooData;

@end