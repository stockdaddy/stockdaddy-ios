//
//  SDTStockNewsWebViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/17/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTStockNewsWebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *webURL;

@end
