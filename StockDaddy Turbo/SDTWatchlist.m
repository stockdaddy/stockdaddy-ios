//
//  SDTWatchlist.m
//  StockDaddy Turbo
//
//  Created by Jain on 3/12/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTWatchlist.h"

@implementation SDTWatchlist

static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
        self.stockNameList = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];

    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Watchlist ("
        "                WatchID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 StockName VARCHAR, "
        "				 StockSymbol VARCHAR, "
        "				 ExchName VARCHAR, "
        "                YahooSymNExch VARCHAR, "
        "                GoogleExchSym VARCHAR, "
        "                ExchDisp VARCHAR, "
        "                TypeDisp VARCHAR, "
        "                Preloaded BOOLEAN)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create Watchlist table");
        }
        [self closeDataBase];

    } else {
        NSLog(@"Failed to open/create database");
    }
}

#pragma mark -
#pragma DataBase Operations

// return Yes: Succesfully Added to Watchlist; NO: Can't add to Watchlist
- (BOOL) addToWatchlist:(NSString*)symbol stockName:(NSString*)stockName exchName:(NSString*)exchName yahooSymNExch:(NSString*)yahooSym googleSymbol:(NSString*)googSym exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typeDisp preLoaded:(NSInteger)defaultValue
{
    if([self checkForDuplicates:yahooSym stockName:stockName exchName:exchName]){
        return NO;
    } else {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
            char *errMsg;
            NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO Watchlist (StockName, StockSymbol, ExchName, YahooSymNExch, GoogleExchSym, ExchDisp, TypeDisp, Preloaded) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", %ld)", stockName, symbol, exchName, yahooSym, googSym, exchDisp, typeDisp, (long)defaultValue];
            const char *insert_stmt = [insert_query UTF8String];
            
            if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to Insert to Watchlist table because of %s", errMsg);
                [self closeDataBase];
                return NO;
            }
            [self closeDataBase];
        } else { NSLog(@"Failed to open/create database"); return NO; }
        
        return YES;
    }
}

// return Yes: Duplicate Exists; NO: Duplicate DoesNOT exists
- (BOOL) checkForDuplicates:(NSString*)yahooSymNExch stockName:(NSString*)stockName exchName:(NSString*)exchName
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *exchQuery = [NSString stringWithFormat: @"SELECT WatchID FROM Watchlist WHERE YahooSymNExch=\"%@\" AND StockName=\"%@\" AND ExchName=\"%@\"", yahooSymNExch, stockName, exchName];
        const char *exch_stmt = [exchQuery UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, exch_stmt, -1, &statement, NULL) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *watchID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                if(watchID != nil ) { sqlite3_finalize(statement); [self closeDataBase]; return YES; }
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"Get WatchID Query Failed (for checking Duplicates) Because %s", sqlite3_errmsg(stockDaddyDB));
            [self closeDataBase];
            return NO;
        }
    }
    
    return NO;
}

- (NSMutableArray*) getWatchlistForGoogle
{
    NSMutableArray *watchlistData = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT StockSymbol, GoogleExchSym, StockName FROM Watchlist"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *googleSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                [watchlistData addObject:[NSString stringWithFormat:@"%@:%@", googleSymbol, stockSymbol]];
                [[self stockNameList] addObject:stockName];
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getWatchlistData Query Failed!");
        }
        [self closeDataBase];
    }
    
    return watchlistData;
}

// returns array of dictionaries of all the Watchlists in the table
- (NSMutableArray*) getAllWatchList
{
    NSMutableArray *watchlistData = [[NSMutableArray alloc] init];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK) {
        NSString *query = [NSString stringWithFormat: @"SELECT * FROM Watchlist"];
        const char *select_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, select_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];

                NSString *watchID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                [tempDictionary setObject:watchID forKey:@"watchID"];
                
                NSString *stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                [tempDictionary setObject:stockName forKey:@"stockName"];
                
                NSString *stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                [tempDictionary setObject:stockSymbol forKey:@"stockSymbol"];
                
                NSString *exchName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                [tempDictionary setObject:exchName forKey:@"exchName"];
                
                NSString *yahooSymExch = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                [tempDictionary setObject:yahooSymExch forKey:@"yahooSymNExch"];
                
                NSString *googleExch = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                [tempDictionary setObject:googleExch forKey:@"googleExchSym"];
                
                NSString *exchDisp = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)];
                [tempDictionary setObject:exchDisp forKey:@"exchDisp"];
                
                NSString *typeDisp = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)];
                [tempDictionary setObject:typeDisp forKey:@"typeDisp"];
                
                [watchlistData addObject:tempDictionary];
            }
            
            sqlite3_finalize(statement);
        } else {
            NSLog(@"getAllWatchList Query Failed!");
        }
        [self closeDataBase];
    } else { NSLog(@"Error Opening the DB for Watchlist!"); }
    
    return watchlistData;
}

// return Yes: Succesfully Deleted to Watchlist; NO: Can't Delete to Watchlist
- (BOOL) deleteFromWatchlist:(NSString*)watchID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM Watchlist WHERE WatchID=\"%@\"", watchID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From Watchlist table because of %s", errMsg);
            [self closeDataBase];
            return NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return NO; }
    
    return YES;
}

// Returns the StockName based StockSymbol
// used for showing the name on Watchlst
- (NSString*) stockNameByStockSymbol:(NSString*)symbol googExchSymbol:(NSString*)googExchSym
{
    NSString *stockName = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockName FROM Watchlist WHERE StockSymbol=\"%@\" AND GoogleExchSym=\"%@\"", symbol, googExchSym];
        const char *sel_stmt = [sel_query UTF8String];
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockName;
}

// insert default List to the Watchlist;
-(void) insertDefaultWatchlistTicker
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;

        for(NSString *insert_query in [self listDefaultWatchlistTicker]){
            const char *insert_stmt = [insert_query UTF8String];
            if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to Insert Default list to Watchlist table because of %s", errMsg);
            }
        }

        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); }
    
}
// To list Default Tickers to the Watchlist
- (NSArray*) listDefaultWatchlistTicker
{
    NSArray *defaultList = @[@"INSERT OR IGNORE INTO Watchlist (WatchID, StockName, StockSymbol, ExchName, YahooSymNExch, GoogleExchSym, ExchDisp, TypeDisp, Preloaded) VALUES(1, \"Tesla Motors, Inc.\", \"TSLA\", \"NASDAQ Stock Exchange\", \"TSLA\", \"NASDAQ\", \"NASDAQ\", \"Equity\", 0)",
                             @"INSERT OR IGNORE INTO Watchlist (WatchID, StockName, StockSymbol, ExchName, YahooSymNExch, GoogleExchSym, ExchDisp, TypeDisp, Preloaded) VALUES(2, \"Apple Inc.\", \"AAPL\", \"NASDAQ Stock Exchange\", \"AAPL\", \"NASDAQ\", \"NASDAQ\", \"Equity\", 0)",
                             @"INSERT OR IGNORE INTO Watchlist (WatchID, StockName, StockSymbol, ExchName, YahooSymNExch, GoogleExchSym, ExchDisp, TypeDisp, Preloaded) VALUES(3, \"BlackBerry Limited\", \"BBRY\", \"NASDAQ Stock Exchange\", \"BBRY\", \"NASDAQ\", \"NASDAQ\", \"Equity\", 0)",
                             @"INSERT OR IGNORE INTO Watchlist (WatchID, StockName, StockSymbol, ExchName, YahooSymNExch, GoogleExchSym, ExchDisp, TypeDisp, Preloaded) VALUES(4, \"iShares Russell 2000\", \"IWM\", \"New York Stock Exchange\", \"IWM\", \"NYSEARCA\", \"\", \"ETF\", 0)"];
    
    return defaultList;
}

- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}

@end
