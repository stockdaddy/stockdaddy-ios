//
//  SDTWatchlistNewViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTWatchlistNewViewController.h"

@interface SDTWatchlistNewViewController ()

@end

@implementation SDTWatchlistNewViewController

NSMutableArray* watchlistData;
UIRefreshControl *refreshControl;
NSMutableArray *googleData;
NSArray *adBannerConstraints;
NSArray *watchListTableConstraints;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        googleData = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [self refreshTable];
    
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    NSLog(@"default showAds watchlist is %@", [defaults objectForKey:@"showAds"]);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;

    watchlistInfo = [[SDTWatchlist alloc] init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [self.mainTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    self.adBanner.delegate = self;
    
    adBannerConstraints = [[NSArray alloc] initWithArray:[self.adBanner constraints]];
    watchListTableConstraints = [[NSArray alloc] initWithArray:[self.mainTable constraints]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [googleData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WatchlistCell";
    SDTWatchlistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"WatchlistCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }

    // Configure the cell...
    NSDictionary *tempDictGoogle = [googleData objectAtIndex:indexPath.row];
    
    if([googleData count] >0){
        cell.symbol.text = [tempDictGoogle objectForKey:@"t"];
        cell.stockName.text = [watchlistInfo stockNameByStockSymbol:[tempDictGoogle objectForKey:@"t"] googExchSymbol:[tempDictGoogle objectForKey:@"e"]];
        cell.value.text = [tempDictGoogle objectForKey:@"l"];
        cell.chgPct.text = [NSString stringWithFormat:@"%@ (%@%%)", [tempDictGoogle objectForKey:@"c"], [tempDictGoogle objectForKey:@"cp"]];
        if([[tempDictGoogle objectForKey:@"cp"] floatValue] >= 0.0){
            cell.containerColor.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
            cell.mainCont.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
        } else {
            cell.containerColor.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
            cell.mainCont.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
        }
    }
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *watchID = [[watchlistData objectAtIndex:indexPath.row] objectForKey:@"watchID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [watchlistInfo deleteFromWatchlist:watchID]) {
        // Delete the row from the data source
        [watchlistData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.mainTable reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    SDTTickerQuoteViewController *quoteView = [self.storyboard instantiateViewControllerWithIdentifier:@"TickerInfo"];
    SDTTickerQuote *watchTickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooQuoteData"];
    
    NSDictionary *tempDictionary= [watchlistData objectAtIndex:indexPath.row];
    NSString *companyName = [tempDictionary objectForKey:@"stockName"];
    NSString *symbol = [tempDictionary objectForKey:@"yahooSymNExch"];
    
    // initiate the Yahoo Search Request
    if(([symbol rangeOfString:@".BO"].location == NSNotFound) && ([symbol rangeOfString:@".NS"].location == NSNotFound)) {
        [watchTickerQuote yahooTickerQuote:symbol];
    }
    else {
        [watchTickerQuote indianTickerQuote:[tempDictionary objectForKey:@"stockSymbol"]];
    }
    
    // assign the company name from Searched Result to Quote View. Also pass the entire result so that
    // we can use to pass on the Dictionary to Ticker Options
    quoteView.companyName = companyName;
    quoteView.symbol = symbol;
    quoteView.exchDisp = [tempDictionary objectForKey:@"exchDisp"];
    quoteView.typDisp = [tempDictionary objectForKey:@"typeDisp"];
    
    [self.navigationController pushViewController:quoteView animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark Helper Functions

-(NSMutableString*) generateGoogleRequestList:(NSMutableArray*)googleData
{
    NSMutableString *resultString = [[NSMutableString alloc]init];
    for (int i=0; i<[googleData count]; i++) {
        [resultString appendString: [NSString stringWithFormat:@"%@,",googleData[i]]];
    }
    
    return resultString;
}

- (void) googleReturnedData:(NSNotification *)notification
{
    NSLog(@"recieved GoogWatchQuoteData Data");
    NSArray *returnedData = [notification object];
    
    [googleData removeAllObjects];
    googleData = [NSMutableArray arrayWithArray:returnedData];
    [self.mainTable reloadData];
    
    // Remove as observer
    [self removeObservers];
    
    self.noDataCont.hidden = YES;
    
    [refreshControl endRefreshing];
}

- (void) networkFailure
{
    // Remove as observer
    [self removeObservers];

}

- (void) removeObservers
{
    
    self.noDataCont.hidden = NO;
    self.loadingLab.text = @"Network Error...";
    self.loadingLab.hidden = NO;
    self.loadingActInd.hidden = YES;
    self.noDataLabel.hidden = YES;

    // Remove as observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"GoogWatchQuoteData"
                                                  object: nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                 name: @"NetworkFailure"
                                               object: nil];
}


- (void) refreshTable
{
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(googleReturnedData:)
                                                 name: @"GoogWatchQuoteData"
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(networkFailure)
                                                 name: @"NetworkFailure"
                                               object: nil];

    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"GoogWatchQuoteData"];
    watchlistInfo = [[SDTWatchlist alloc] init];
    
    if([[watchlistInfo getWatchlistForGoogle] count]!= 0){
        [tickerQuote googleTickerQuote:[self generateGoogleRequestList:[watchlistInfo getWatchlistForGoogle]]];        
    }
    if([[watchlistInfo getAllWatchList] count] == 0){
        self.noDataCont.hidden = NO;
        self.noDataLabel.hidden = NO;
        self.loadingLab.hidden = YES;
        self.loadingActInd.hidden = YES;
    } else {
        self.noDataCont.hidden = NO;
        self.noDataLabel.hidden = YES;
        self.loadingLab.hidden = NO;
        self.loadingActInd.hidden = NO;
        self.loadingLab.text = @"Loading ...";
    }
    
    watchlistData = [watchlistInfo getAllWatchList];
}

#pragma mark -
#pragma iAd Banner Delegate

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Watchlist Banner failed to load because of %@", error);
    
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    if( ([defaults objectForKey:@"showAds"]) && ([[defaults objectForKey:@"showAds"] isEqualToString:@"RemoveThem"])){
        self.adBanner.hidden = YES;
        [self.adBanner removeConstraints:self.adBanner.constraints];
        [self.mainTable removeConstraints:self.mainTable.constraints];
    }
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    if( ([defaults objectForKey:@"showAds"]) && ([[defaults objectForKey:@"showAds"] isEqualToString:@"RemoveThem"])){
        [self removeAds];
    } else {
        [self.adBanner addConstraints:adBannerConstraints];
        [self.mainTable addConstraints:watchListTableConstraints];
        
        self.adBanner.hidden = NO;
    }
}

//-(void)bannerViewWillLoadAd:(ADBannerView *)banner
//{
//}

//-(void)bannerViewActionDidFinish:(ADBannerView *)banner
//{
//}

-(void) removeAds
{
    self.adBanner.hidden = YES;
    [self.adBanner removeFromSuperview];
    [self.adBanner removeConstraints:self.adBanner.constraints];

    [self.mainTable removeFromSuperview];
    [self.mainTable removeConstraints:self.mainTable.constraints];
    [self.mainTable setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.mainTable setFrame:CGRectMake(0, 0, 320, 518)];
    [self.view addSubview:self.mainTable];
}

@end
