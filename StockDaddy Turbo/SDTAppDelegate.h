//
//  SDTAppDelegate.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/20/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTExchangeInfo.h"
#import "SDTWatchlist.h"
#import "SDTAlert.h"
#import "SDTNotification.h"
#import "SDTStock.h"
#import "SDTStockBuy.h"
#import "SDTStockSell.h"
#import "Flurry.h"
#import "SDTAlertListNewViewController.h"
#import "SDTTickerQuote.h"

@interface SDTAppDelegate : UIResponder <UIApplicationDelegate> {
    SDTAlert *alert;
    SDTAlertListNewViewController *alertVC;
    SDTTickerQuote *tickerQuote;
}

@property (strong, nonatomic) UIWindow *window;

@end
