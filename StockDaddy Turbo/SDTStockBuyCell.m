//
//  SDTStockBuyCell.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/19/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockBuyCell.h"

@implementation SDTStockBuyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma Edit Menu Controller

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(edit:)) {
        return YES;
    }
    return NO;
}

- (IBAction)edit:(id)sender
{
    NSLog(@"edit Menu called for StockBuy");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditStockBuyOption" object:[NSString stringWithFormat:@"%li",(long)self.indexPathRow]];
}
@end
