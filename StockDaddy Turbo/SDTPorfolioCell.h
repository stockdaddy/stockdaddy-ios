//
//  SDTDefaultSubtitleCell.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/18/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTPorfolioCell : UITableViewCell

- (IBAction)edit:(id)sender;

@property NSInteger indexPathRow;

@end
