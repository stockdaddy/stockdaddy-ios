//
//  SDTStockSellViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockSellViewController.h"

@interface SDTStockSellViewController ()

@end

@implementation SDTStockSellViewController

NSMutableArray *sellStockData;
BOOL isShowingDatePicker;
UITextField *sellQuanTextField;
UITextField *sellPriceTextField;
CGPoint offset;
CGFloat mainTableHeight;
UIAlertView *alertView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    // hide the tab bar
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    sellInfo = [[SDTStockSell alloc] init];
    portfolioInfo = [[SDTPortfolio alloc] init];
    buyInfo = [[SDTStockBuy alloc] init];
    sellStockData = [[NSMutableArray alloc] init];
	
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;

    isShowingDatePicker = NO;

    // initalize for cell (sell Date)
    self.dateViewNew = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 162)];
    self.datePickerViewNew = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 162)];
    self.datePickerViewNew.datePickerMode = UIDatePickerModeDate;
    self.datePickerViewNew.hidden = YES;
    self.dateViewNew.hidden = YES;
    [self.dateViewNew addSubview:self.datePickerViewNew];

    [self initializeFormTextFields];
    
    self.navigationItem.title = @"Sell Stock Info";
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(saveMethod)];
    
    self.navigationItem.rightBarButtonItem = saveButton;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // For Adjusting TableView when Keyboard Shows
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    alertView = [[UIAlertView alloc]initWithTitle:@"Sell Stock Info" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma TableView Properties

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return @"Portfolio Name";
    } else if (section == 1){
        return @"Stock Name";
    } else if (section == 2){
        return @"Sell Date";
    } else if (section == 3){
        return @"Sell Quantity";
    } else {
        return @"Sell Price";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2){
        return (isShowingDatePicker) ? 169.0f : 35.0f;
    } else {
        return 35.0f;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 40.0f;
    } else {
        return 30.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SellStockCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"SellStockCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = [portfolioInfo getPortfolioNameFromPortfolioID:self.portfolioID];
            break;
        case 1:
            cell.textLabel.text = self.stockName;
            break;
        case 2:
        {
            cell.textLabel.hidden = (isShowingDatePicker) ? YES : NO;
            [cell.textLabel setText:[self convertDateToString:[self.datePickerViewNew date]]];
            [cell.contentView addSubview:self.dateViewNew];
            break;
        }
        case 3:
            [cell.contentView addSubview:sellQuanTextField];
            break;
        case 4:
            [cell.contentView addSubview:sellPriceTextField];
            break;
        default:
            break;
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2) {
        if(isShowingDatePicker) {
            self.datePickerViewNew.hidden = YES;
            self.dateViewNew.hidden = YES;
        } else {
            self.datePickerViewNew.hidden = NO;
            self.dateViewNew.hidden = NO;
        }
        
        isShowingDatePicker = !isShowingDatePicker;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark -
#pragma navigation

-(void) saveMethod
{
    NSLog(@"Save Button Clicked");
    NSLog(@"Portfolio ID is %@", self.portfolioID);
    NSLog(@"Stock ID is %@", self.stockID);
    NSLog(@"StockBuy ID is %@", self.buyID);
    NSLog(@"SellDate is %@", [self convertDateToString:[self.datePickerViewNew date]]);
    NSLog(@"Sell Price  is %@", [sellPriceTextField text]);
    NSLog(@"Sell Quantity is %@", [sellQuanTextField text]);
    
    if([self checkValidation]){
        
        // Add to Sell Stock Table
        if([sellInfo addToStockSellTable:self.portfolioID stockBuyID:self.buyID stockID:self.stockID sellDate:[self convertDateToString:[self.datePickerViewNew date]] sellQuantity:[sellQuanTextField text] sellPrice:[sellPriceTextField text] notes:@""]) {
            
            // Save Successfull, update the Buy Quantity for stockBuyID;
            double newQuant = [self.buyQuant doubleValue] - [[sellQuanTextField text] doubleValue];
            if([buyInfo updateBuyQuantityForStockBuyID:self.buyID buyQuanity:[NSString stringWithFormat:@"%0.02f", newQuant]]){
                [self.navigationController popViewControllerAnimated:YES];
                
                // Post Notification
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePortfolioLabel" object:nil];
            }
        }
    }
}

#pragma mark notifications

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    offset = self.mainTable.contentOffset;
    mainTableHeight = self.mainTable.frame.size.height;
    CGRect viewFrame = self.mainTable.frame;
    viewFrame.size.height -= keyboardSize.height;
    self.mainTable.frame = viewFrame;
    
    CGRect textFieldRect = [sellPriceTextField frame];
    textFieldRect.origin.y += 10;
    [self.mainTable scrollRectToVisible:textFieldRect animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainTable.frame = CGRectMake(0, 0, 320, mainTableHeight);
    self.mainTable.contentOffset = offset;
    self.mainTable.scrollIndicatorInsets = contentInsets;
    
    [self.mainTable scrollRectToVisible:sellPriceTextField.frame animated:YES];
}

#pragma mark -
#pragma helper functions

-(void) initializeFormTextFields
{
    sellQuanTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    sellPriceTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    
    sellQuanTextField.adjustsFontSizeToFitWidth = YES;
    sellQuanTextField.placeholder = @"15";
    sellQuanTextField.backgroundColor = [UIColor whiteColor];
    sellQuanTextField.keyboardType = UIKeyboardTypeDecimalPad;
    sellQuanTextField.text = self.buyQuant;
    sellQuanTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
    
    sellPriceTextField.adjustsFontSizeToFitWidth = YES;
    sellPriceTextField.placeholder = @"466.23";
    sellPriceTextField.backgroundColor = [UIColor whiteColor];
    sellPriceTextField.keyboardType = UIKeyboardTypeDecimalPad;
    sellPriceTextField.text = self.buyPrice;
    sellPriceTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
}

-(NSString*) convertDateToString:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    return [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date] ];
    
}

-(NSDate*) convertStringToDate:(NSString*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    return [dateFormatter dateFromString:date];
}

// Yes it is an integer: No: not an integer.
-(BOOL) isInteger:(NSString*)value
{
    float valueFlot = [value floatValue];
    return (valueFlot <= LONG_MIN || valueFlot >= LONG_MAX || valueFlot == (long)valueFlot) ? YES : NO;
}

// yes: passed, no:failed
- (BOOL) checkValidation
{
    BOOL passed = YES;
    
    if([[sellQuanTextField text] length] == 0 || [[sellPriceTextField text] length] == 0){
        alertView.message = @"Buy Quantity Or Buy Price Cannot Be Empty";
        [alertView show];
        passed = NO;
    }
    
    if(![self isInteger:[sellQuanTextField text]]) {
        alertView.message = @"Buy Quantity is not a Valid Integer";
        [alertView show];
        passed = NO;
    }
    
    if([[sellQuanTextField text] doubleValue] < 0 || [[sellPriceTextField text] doubleValue] < 0){
        alertView.message = @"Buy Quantity Or Buy Price Cannot Be less than 0";
        [alertView show];
        passed = NO;
    }
    
    if([[sellQuanTextField text] doubleValue] > [self.buyQuant doubleValue]){
        alertView.message = @"Sell Quantity Cannot be More than Buy Quantity";
        [alertView show];
        passed = NO;
    }
    
    // Compare teh Dates
    NSComparisonResult compareDate = [[self.datePickerViewNew date] compare:[self convertStringToDate:self.buyDate]];
    if(compareDate == NSOrderedAscending){
        alertView.message = @"Sell Date Cannot be less than Buy Date";
        [alertView show];
        passed = NO;
    }
    
    return passed;
}

@end
