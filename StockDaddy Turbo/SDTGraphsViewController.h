//
//  SDTGraphsViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/14/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface SDTGraphsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate>

@property (nonatomic, strong) NSString *stockSymbol;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet ADBannerView *adBanner;

@end
