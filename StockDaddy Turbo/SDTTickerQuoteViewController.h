//
//  SDTTickerQuoteViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTTickerQuote.h"
#import "SDTTickerOptionsViewController.h"
#import <iAd/iAd.h>

@interface SDTTickerQuoteViewController : UIViewController <ADBannerViewDelegate>

@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSString *exchDisp;
@property (nonatomic, strong) NSString *typDisp;
@property (nonatomic, strong) NSString *chgVal;
@property (nonatomic, strong) NSString *chgPct;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceChangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastUpatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *previousCloseLabel;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *bidLabel;
@property (weak, nonatomic) IBOutlet UILabel *askLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneYearEstLabel;
@property (weak, nonatomic) IBOutlet UILabel *divYieldLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneYearRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *avgVolLabel;
@property (weak, nonatomic) IBOutlet UILabel *mktCapLabel;
@property (weak, nonatomic) IBOutlet UILabel *PELabel;
@property (weak, nonatomic) IBOutlet UILabel *EPSLabel;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *quoteView;
@property (weak, nonatomic) IBOutlet UILabel *DataNotFoundLabel;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
- (IBAction)RefreshButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *RefreshButtonOutlet;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addOutlet;

@property (weak, nonatomic) IBOutlet ADBannerView *adBanner;

- (void)reloadUIView:(NSNotification *)notification;
- (void)DataNotFound:(NSNotification *)notification;

- (void)reloadTickerLabels:(NSDictionary *)returnedData;
- (void)reloadIndianTickerLabels:(NSDictionary *)returnedData;

@end
