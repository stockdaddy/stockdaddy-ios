//
//  SDTStockNewsViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/26/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockNewsViewController.h"

@interface SDTStockNewsViewController ()

@end

@implementation SDTStockNewsViewController

NSArray *stockNewsData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    SDTTickerQuote *tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooStockNewsData"];
    [tickerQuote yahooStockNews:self.symbol];
    
    [self loadingContVisibility];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reloadUIView:)
                                                 name: @"YahooStockNewsData"
                                               object: nil];
    self.stockNewsTable.dataSource = self;
    self.stockNewsTable.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma table view properties

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return stockNewsData.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StockNewsCell";
    SDTStockNewsCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[SDTStockNewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(stockNewsData.count != 0) {
        [cell.title setText:[[stockNewsData objectAtIndex:indexPath.row] objectForKey:@"title"]];
        NSString *desc = [[stockNewsData objectAtIndex:indexPath.row] objectForKey:@"description"];

        if(![desc isEqual:[NSNull null]]) {
            [cell.description setText:desc];
        } else {
            [cell.description setText:@"--"];
        }
        
        [cell.dateTime setText:[[stockNewsData objectAtIndex:indexPath.row] objectForKey:@"pubDate"]];
    }
  
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SDTStockNewsWebViewController *stockNewsWeb = [self.storyboard instantiateViewControllerWithIdentifier:@"StockNewsWebVC"];
    stockNewsWeb.webURL = [[stockNewsData objectAtIndex:indexPath.row] objectForKey:@"link"];
    
    [self.navigationController pushViewController:stockNewsWeb animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma helper functions

- (void)reloadUIView:(NSNotification *)notification
{
    NSLog(@"recieved YahooStockNewsData Data");
    
    NSDictionary *returnedData = [notification object];
    
    if([returnedData objectForKey:@"query"] != [NSNull null]){
        NSDictionary *queryData = [returnedData objectForKey:@"query"];
        if([queryData objectForKey:@"results"] != [NSNull null]){
            NSDictionary *resultsData = [queryData objectForKey:@"results"];
            if([resultsData objectForKey:@"item"] != [NSNull null]){
                stockNewsData = [[NSArray alloc] initWithArray:[resultsData objectForKey:@"item"]];
            }
        }
    }
    
    [self.stockNewsTable reloadData];
    
    // Remove it as an observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"YahooStockNewsData"
                                                  object: nil];
    
    [self loadingContVisibility];
}


- (void) loadingContVisibility
{
    if([stockNewsData count] == 0){
        self.loadingCont.hidden = NO;
    } else {
        self.loadingCont.hidden = YES;
    }
}

@end
