//
//  SDTNotification.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SDTNotification : NSObject

- (id) init;
- (void) initDatabase;
- (BOOL) addToNotification:(NSString*)symbol stockName:(NSString*)stockName targetVal:(NSString*)targetVal targetValFlag:(NSString*)targetValFlag currentPrice:(NSString*)cPrice currentChg:(NSString*)cChg currentChgPct:(NSString*)cChgPct;

- (NSMutableArray*) getAllNotifications;
- (BOOL) deleteFromNotifTable:(NSString*)notifID;

@end
