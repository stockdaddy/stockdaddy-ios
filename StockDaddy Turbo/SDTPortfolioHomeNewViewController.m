//
//  SDTPortfolioHomeNewViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTPortfolioHomeNewViewController.h"

@interface SDTPortfolioHomeNewViewController ()

@end

@implementation SDTPortfolioHomeNewViewController

NSMutableArray* portfolioData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [portfolioData removeAllObjects];
    portfolioData = [portfolio getAllPortfolioForTableViewDS];
    
    [self.mainTable reloadData];
    
    [self checkForNoPortfolio];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;

    UIMenuItem *editMenuItem = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(edit:)];
    [[UIMenuController sharedMenuController] setMenuItems: @[editMenuItem]];
    [[UIMenuController sharedMenuController] update];

    
    portfolio = [[SDTPortfolio alloc] init];
    portfolioData = [[NSMutableArray alloc] initWithArray:[portfolio getAllPortfolioForTableViewDS]];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reloadTable:)
                                                 name: @"NewPortfolioAdded"
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(editPortfolio:)
                                                 name: @"EditPortfolioOption"
                                               object: nil];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [portfolioData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PortfolioCell";
    SDTPorfolioCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *tempDictionary= [portfolioData objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [tempDictionary objectForKey:@"portfolioName"];
    cell.detailTextLabel.text = [tempDictionary objectForKey:@"exchangeType"];
    cell.indexPathRow =  indexPath.row;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *tempDictionary= [portfolioData objectAtIndex:indexPath.row];
    
    SDTDashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"Dashboard"];
    
    dashboard.portfolioID = [tempDictionary objectForKey:@"portfolioID"];
    dashboard.portfolioName = [tempDictionary objectForKey:@"portfolioName"];
    dashboard.exchangeType = [tempDictionary objectForKey:@"exchangeType"];
    dashboard.region = [tempDictionary objectForKey:@"region"];
    dashboard.googleExchSym = [tempDictionary objectForKey:@"googExchSymbol"];
    dashboard.yahooExchSym = [tempDictionary objectForKey:@"exchTypeSymbol"];
    dashboard.notes = [tempDictionary objectForKey:@"notes"];
    
    [self.navigationController pushViewController:dashboard animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *portfolioID = [[portfolioData objectAtIndex:indexPath.row] objectForKey:@"portfolioID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [portfolio deleteFromPortfolioTable:portfolioID]) {
        // Delete the row from the data source
        [portfolioData removeObjectAtIndex:indexPath.row];
        [self.mainTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self checkForNoPortfolio];
    
    [self.mainTable reloadData];
}

#pragma Helper Function

- (void) reloadTable:(NSNotification *)notification
{
    NSLog(@"New Portfolio Added. Reloading Table");
    [self.mainTable reloadData];
    
    // Remove as observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"NewPortfolioAdded"
                                                  object: nil];
}

- (void) checkForNoPortfolio
{
    if([portfolioData count] > 0){
        self.noPortfolioCont.hidden = YES;
    } else {
        self.noPortfolioCont.hidden = NO;
    }
}

#pragma mark -
#pragma mark Edit Menu Controller

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    return (action == @selector(copy:));
}

// required
- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
}

// Notification for Edit Portfolio
- (void) editPortfolio:(NSNotification *)notification
{
    NSInteger indexPathRow = [[notification object] integerValue];
    NSLog(@"Edit Portfolio ID for %@", [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"portfolioID"]);
    
    SDTNewPortfolioViewController *newPortVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPortfolioVC"];
    newPortVC.isEditing = YES;
    newPortVC.portfolioID = [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"portfolioID"];
    newPortVC.portfolioName = [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"portfolioName"];
    newPortVC.region = [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"region"];
    newPortVC.exchType = [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"exchangeType"];
    newPortVC.portNotes = [[portfolioData objectAtIndex:indexPathRow] objectForKey:@"notes"];

    [self.navigationController pushViewController:newPortVC animated:YES];
}

@end
