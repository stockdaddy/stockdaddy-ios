//
//  SDTAlertListNewViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTAlertListNewViewController.h"

@interface SDTAlertListNewViewController ()

@end

@implementation SDTAlertListNewViewController

NSMutableArray* alertStockData;
UIRefreshControl *refreshControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [alertStockData removeAllObjects];
    alertStockData = [alertData getAllStockAlerts];
    
    [self.mainTable reloadData];
    
    [self checkForNoData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    
    alertData = [[SDTAlert alloc] init];
    notificationInfo = [[SDTNotification alloc] init];
    
    alertStockData = [alertData getAllStockAlerts];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [self.mainTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [alertStockData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StockAlertsCell";
    SDTAlertsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"StockAlertsCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    // Configure the cell...
    NSDictionary *tempDictionary= [alertStockData objectAtIndex:indexPath.row];
    
    cell.symbol.text = [tempDictionary objectForKey:@"stockSymbol"];
    cell.stockName.text = [tempDictionary objectForKey:@"stockName"];
    
    NSString *tPrice = [tempDictionary objectForKey:@"targetPrice"];
    cell.price.text = ([tPrice length] == 0) ? @"-" : tPrice;
    
    NSString *tPriceChg = [tempDictionary objectForKey:@"targetChgVal"];
    cell.chg.text = ([tPriceChg length] == 0) ? @"-" : tPriceChg;
    
    NSString *tPriceChgPct = [tempDictionary objectForKey:@"targetChgPct"];
    cell.pct.text = ([tPriceChgPct length] == 0) ? @"-" : tPriceChgPct;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *alertID = [[alertStockData objectAtIndex:indexPath.row] objectForKey:@"alertID"];
    if (editingStyle == UITableViewCellEditingStyleDelete && [alertData deleteFromStockAlerts:alertID]) {
        // Delete the row from the data source
        [alertStockData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.mainTable reloadData];
    
    [self checkForNoData];
}

#pragma mark -
#pragma helper functions

- (void) checkForNoData
{
    if([alertStockData count] > 0){
        self.noDataCont.hidden = YES;
        // Refresh teh table with notifications;
        [self refreshTable];
    } else {
        self.noDataCont.hidden = NO;
    }
}

#pragma mark -
#pragma notification_check

// true: target hit; false: nothing new
- (BOOL) checkForNotification:(NSArray*)googleData
{
    BOOL hit = false;
    NSMutableDictionary *tempDictFlag = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *tempDictGoogle = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *tempDictAlertInfo = [[NSMutableDictionary alloc]init];
    
    NSMutableArray *targetInfo = [[NSMutableArray alloc] initWithArray:[alertData getTargetFlagList]];
    NSMutableArray *stockAlertInfo = [[NSMutableArray alloc] initWithArray:[alertData getAllStockAlerts]];
    NSMutableArray *googleDataArray = [[NSMutableArray alloc] initWithArray:googleData];

    // use nested forloop
    for(int i=0; i<[stockAlertInfo count]; i++) {
        tempDictFlag = [targetInfo objectAtIndex:i];
        tempDictAlertInfo = [stockAlertInfo objectAtIndex:i];
        tempDictGoogle = [googleDataArray objectAtIndex:i];
        
        NSString *stockSymbol = [tempDictAlertInfo objectForKey:@"stockSymbol"];
        NSString *stockName = [tempDictAlertInfo objectForKey:@"stockName"];
        NSString *googleStockSymbol = [tempDictGoogle objectForKey:@"t"];
        
        if([ stockSymbol isEqualToString:googleStockSymbol]) {
            
            NSString *googlePrice = [tempDictGoogle objectForKey:@"l" ];
            NSString *googleChg = [tempDictGoogle objectForKey:@"c" ];
            NSString *googleChgPct = [tempDictGoogle objectForKey:@"cp" ];
            
            NSString *targetPrice = [tempDictAlertInfo objectForKey:@"targetPrice"];
            NSString *targetChg = [tempDictAlertInfo objectForKey:@"targetChgVal"];
            NSString *targetChgPct = [tempDictAlertInfo objectForKey:@"targetChgPct"];
            
            // for targetPrice
            if([[tempDictFlag objectForKey:@"targetPrcFlag"] integerValue]){
                if( ([googlePrice floatValue] >= [targetPrice floatValue]) && ([[tempDictAlertInfo objectForKey:@"targetPrice"] length] != 0) ){
                    NSLog(@"TargetVal hit 1 for StockSymbol %@", stockSymbol);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetPrice targetValFlag:@"Price" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    [self removeFromStockAlertTable:[tempDictAlertInfo objectForKey:@"alertID"]];
                    break;
                }
            } else{
                if( [googlePrice floatValue] < [targetPrice floatValue] && ([[tempDictAlertInfo objectForKey:@"targetPrice"] length] != 0) ){
                    NSLog(@"TargetVal hit 0 for StockSymbol %@", stockSymbol);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetPrice targetValFlag:@"Price" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    break;
                }
            }
            
            // for targetChg
            if([[tempDictFlag objectForKey:@"targetChgFlag"] integerValue]){
                if( ([googleChg floatValue] >= [targetChg floatValue]) && ([[tempDictAlertInfo objectForKey:@"targetChgVal"] length] != 0) ){
                    NSLog(@"TargetChgVal hit 1 for StockSymbol %@", stockSymbol);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetChg targetValFlag:@"Change" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    break;
                }
            } else{
                if( ([googleChg floatValue] < [targetChg floatValue]) && ([[tempDictAlertInfo objectForKey:@"targetChgVal"] length] != 0) ){
                    NSLog(@"TargetChgVal hit 0 for StockSymbol %@", [tempDictAlertInfo objectForKey:@"stockSymbol"]);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetChg targetValFlag:@"Change" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    break;
                }
            }
            
            // for targetChgPct
            if([[tempDictFlag objectForKey:@"targetChgPctFlag"] integerValue]){
                if( ([googleChgPct floatValue] >= [targetChgPct floatValue]) && ([[tempDictAlertInfo objectForKey:@"targetChgPct"] length] != 0) ){
                    NSLog(@"TargetChgPct hit 1 for StockSymbol %@", [tempDictAlertInfo objectForKey:@"stockSymbol"]);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetChgPct targetValFlag:@"Change Pct" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    break;
                }
            } else{
                if( ([googleChgPct floatValue] < [targetChgPct floatValue]) && ([[tempDictAlertInfo objectForKey:@"targetChgPct"] length] != 0) ){
                    NSLog(@"TargetChgPct hit 0 for StockSymbol %@", [tempDictAlertInfo objectForKey:@"stockSymbol"]);
                    hit = true;
                    [self addNotificationInfo:stockSymbol stockName:stockName targetVal:targetChgPct targetValFlag:@"Change Pct" currentPrice:googlePrice currentChg:googleChg currentChgPct:googleChgPct];
                    break;
                }
            }
            
        }
        
    } // enf of for-loop
    
    [tempDictGoogle removeAllObjects];
    [tempDictFlag removeAllObjects];
    [tempDictAlertInfo removeAllObjects];
    
    return hit;
}

- (void) addNotificationInfo:(NSString*)symbol stockName:(NSString*)stockName targetVal:(NSString*)targetVal targetValFlag:(NSString*)targetValFlag currentPrice:(NSString*)cPrice currentChg:(NSString*)cChg currentChgPct:(NSString*)cChgPct
{
    [notificationInfo addToNotification:symbol stockName:stockName targetVal:targetVal targetValFlag:targetValFlag currentPrice:cPrice currentChg:cChg currentChgPct:cChgPct];
}

- (void) removeFromStockAlertTable:(NSString*)alertID
{
    NSLog(@"Delete AlertID %@ from StockAlert Table", alertID);
    [alertData deleteFromStockAlerts:alertID];
}

#pragma mark -
#pragma mark Refresh & Initiate

- (void) refreshTable
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(googleReturnedData:)
                                                 name: @"GoogleStockAlert"
                                               object: nil];

    SDTTickerQuote *watchTickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"GoogleStockAlert"];
    [watchTickerQuote googleTickerQuote:[alertData getStockListForGoogle]];
}


- (void) googleReturnedData:(NSNotification *)notification
{
    NSLog(@"recieved GoogleStockAlert Data");

    NSArray *returnedData = [notification object];
    if([self checkForNotification:returnedData]){
        [self showNotification];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"GoogleStockAlert"
                                                  object: nil];
    [refreshControl endRefreshing];
}

- (void) showNotification
{
    UILocalNotification *localNotif = [[UILocalNotification alloc]init];
    localNotif.fireDate = Nil;
    localNotif.alertBody = @"Ticker has hit a Target";
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];

}

@end
