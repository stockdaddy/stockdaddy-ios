//
//  SDTNewPortfolioViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/26/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTExchangeInfo.h"
#import "SDTPortfolio.h"

@interface SDTNewPortfolioViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    SDTExchangeInfo *exchangeInfo;
    SDTPortfolio *portfolio;
}

- (IBAction)cancelButton:(id)sender;
- (IBAction)saveButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;

// For Editing Portfolio
@property BOOL isEditing;
@property (nonatomic, strong) NSString *portfolioID;
@property (nonatomic, strong) NSString *portfolioName;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, strong) NSString *exchType;
@property (nonatomic, strong) NSString *portNotes;

@end
