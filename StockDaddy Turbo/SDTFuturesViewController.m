//
//  SDTFuturesViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTFuturesViewController.h"

@interface SDTFuturesViewController ()

@end

@implementation SDTFuturesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.yahooData = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooFuturesMarketData"];
    [tickerQuote yahooTickerQuote:[self yahooFuturesSymbols]];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(yahooReturnedData:)
                                                 name: @"YahooFuturesMarketData"
                                               object: nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self initializeFutureSection];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    UITabBarItem *barItem = [[UITabBarItem alloc] initWithTitle:@"\uf080" image:nil tag:0];
    [barItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"FontAwesome" size:30.0]} forState:UIControlStateNormal];
    
    self.navigationController.tabBarItem = barItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.futureSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section == 0){
        return [[self.yahooData subarrayWithRange:NSMakeRange(0, 4)] count];
    }
    else if(section == 1) {
        return [[self.yahooData subarrayWithRange:NSMakeRange(4, 7)] count];
    }
    else if(section == 2) {
        return [[self.yahooData subarrayWithRange:NSMakeRange(11, 7)] count];
    }
    else if(section == 3) {
        return [[self.yahooData subarrayWithRange:NSMakeRange(18, 3)] count];
    }
    else {
        return [[self.yahooData subarrayWithRange:NSMakeRange(21, 5)] count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.futureSection objectAtIndex:section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FuturesCell";
    SDTWatchlistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"FuturesCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    NSDictionary *tempDict = [[NSDictionary alloc] init];
    switch (indexPath.section) {
        case 0:
            tempDict = [[self.yahooData subarrayWithRange:NSMakeRange(0, 4)] objectAtIndex:indexPath.row];
            break;
        case 1:
            tempDict = [[self.yahooData subarrayWithRange:NSMakeRange(4, 7)] objectAtIndex:indexPath.row];
            break;
        case 2:
            tempDict = [[self.yahooData subarrayWithRange:NSMakeRange(11, 7)] objectAtIndex:indexPath.row];
            break;
        case 3:
            tempDict = [[self.yahooData subarrayWithRange:NSMakeRange(18, 3)] objectAtIndex:indexPath.row];
            break;
        case 4:
            tempDict = [[self.yahooData subarrayWithRange:NSMakeRange(21, 5)] objectAtIndex:indexPath.row];
            break;
    }
    
    // Configure the cell...
    cell.symbol.text = [tempDict objectForKey:@"symbol"];
    cell.stockName.text = [tempDict objectForKey:@"Name"];
    cell.value.text = [tempDict objectForKey:@"LastTradePriceOnly"];
    cell.chgPct.text = [NSString stringWithFormat:@"%@ (%@)", [tempDict objectForKey:@"Change"], [tempDict objectForKey:@"PercentChange"]];
    if([[tempDict objectForKey:@"PercentChange"] floatValue] >= 0.0){
        cell.containerColor.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
        cell.mainCont.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
    } else {
        cell.containerColor.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
        cell.mainCont.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

#pragma mark -
#pragma helperFunctions

- (void) yahooReturnedData:(NSNotification *)notification
{
    NSLog(@"recieved YahooFuturesMarketData Data");
    [self.yahooData removeAllObjects];
    
    NSMutableDictionary *returnedData = [notification object];
    
    if([returnedData objectForKey:@"query"] != [NSNull null]){
        NSDictionary *queryData = [returnedData objectForKey:@"query"];
        if([queryData objectForKey:@"results"] != [NSNull null]){
            NSDictionary *resultsData = [queryData objectForKey:@"results"];
            if([resultsData objectForKey:@"quote"] != [NSNull null]){
                NSArray *quoteData = [resultsData objectForKey:@"quote"];
                
                // store the data
                self.yahooData = [NSMutableArray arrayWithArray:quoteData];
                [self.tableView reloadData];
                
            } else { NSLog(@"QuoteData is NULL"); }
            
        } else {
            NSLog(@"Results is NULL. Posting DataNotFound Notification");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataNotFound" object: nil];
        }
        
    } else { NSLog(@"QueryData is NULL"); }
    
    // Remove as observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"YahooFuturesMarketData"
                                                  object: nil];
}

- (NSString*) yahooFuturesSymbols
{
    return @"CLG14.NYM, HOG14.NYM, NGG14.NYM, RBG14.NYM, HGF14.CMX, ZGG14.CBT, GCF14.CMX, PAH14.NYM, PLF14.NYM, ZIH14.CBT, SIF14.CMX, CH14.CBT, KWH14.CBT, OH14.CBT, SMH14.CBT, RRK14.CBT, BOH14.CBT, SH14.CBT, FCF14.CME, LHG14.CME, LCG14.CME, CCH14.NYB, KCH14.NYB, CTH14.NYB, LBH14.CME, OJH14.NYB";
}
// Future Month index
//Jan : F
//Feb : M
//Mar : H
//Apr : J
//May : K
//Jun : M
//Jul : N
//Aug : Q ,
//Sep : U
//Oct : V
//Nov : X ,
//Dec : Z ,

-(void) initializeFutureSection
{
    self.futureSection = @[ @"Energy", @"Metals", @"Grains", @"Live Stock", @"Softs"];
}

@end
