//
//  SDTStock.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SDTStock : NSObject

- (id) init;
- (void) initDatabase;
- (NSString*) addTOStocksTable:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typDisp  googExchSym:(NSString*) googExchSym;
- (BOOL) deleteFromStockTable:(NSString*)stockID;

- (BOOL) stockIDExists:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol;
- (NSString*) getStockID:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol;

- (NSMutableArray*) getStockIDList:(NSString*)portfolioID;
- (NSMutableArray*) getStockInfo:(NSString*)portfolioID;
- (NSString*) generateGoogleURLStockList:(NSString*)portID;
- (NSString*) stockSymbolByStockID:(NSString*)portID stockID:(NSString*)stockID;
- (NSString*) stockNameByStockID:(NSString*)portID stockID:(NSString*)stockID;

- (NSString*) stockExchDispByStockID:(NSString*)portID stockID:(NSString*)stockID;
- (NSString*) stockTypeDispByStockID:(NSString*)portID stockID:(NSString*)stockID;

@end
