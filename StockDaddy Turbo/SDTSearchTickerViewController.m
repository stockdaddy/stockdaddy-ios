//
//  SDTSearchTickerViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/20/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTSearchTickerViewController.h"

@interface SDTSearchTickerViewController ()

@end

@implementation SDTSearchTickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -
#pragma mark Table View Properties

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [yahooSearchResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SearchResultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    if([yahooSearchResult count] > 0){
        NSDictionary *tempDictionary= [yahooSearchResult objectAtIndex:indexPath.row];
        
        NSMutableString *exchType = [tempDictionary objectForKey:@"exchDisp"];
        NSMutableString *typeDisp = [tempDictionary objectForKey:@"typeDisp"];
        
        if([exchType isEqualToString:@"Bombay"]) {
            exchType = [@"NSE" mutableCopy];
        }
        
        if(exchType == nil || [typeDisp isEqualToString:@"ETF"]){
            exchType = typeDisp;
        }
        
        cell.textLabel.text = [tempDictionary objectForKey:@"name"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ : %@", exchType, [tempDictionary objectForKey:@"symbol"]];        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SDTTickerQuoteViewController *quoteView = [self.storyboard instantiateViewControllerWithIdentifier:@"TickerInfo"];
    
    NSDictionary *tempDictionary= [yahooSearchResult objectAtIndex:indexPath.row];
    
    // assign the company name from Searched Result to Quote View. Also pass the entire result so that
    // we can use to pass on the Dictionary to Ticker Options
    quoteView.companyName = [tempDictionary objectForKey:@"name"];;
    quoteView.symbol = [tempDictionary objectForKey:@"symbol"];;
    quoteView.exchDisp = [tempDictionary objectForKey:@"exchDisp"];
    quoteView.typDisp = [tempDictionary objectForKey:@"typeDisp"];
    
    [self.navigationController pushViewController:quoteView animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma -
#pragma mark SearchBar Properties

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"The searched Ticker Text %@", searchBar.text);
    [self yahooSearchTickerRequest:searchBar.text];

}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    NSLog(@"User canceled search");
    [searchBar resignFirstResponder]; // if you want the keyboard to go away
}

#pragma -
#pragma mark Network Requests

- (void) yahooSearchTickerRequest:(NSString*)searchText
{
    NSString *urlText = [NSString stringWithFormat:@"http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=%@&callback=YAHOO.Finance.SymbolSuggest.ssCallback", searchText];
    NSString* newURLText = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:newURLText]];
    [NSURLConnection sendAsynchronousRequest:requestURL queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
        
        // convert the returned NSData to UTF8String and then filter the result to convert to JSON
        NSString *dataToString= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if(connectionError == nil){
            // parse the Data
            NSError *error = nil;
            NSString *cleanedData = [dataToString substringWithRange:NSMakeRange(39, [dataToString length]-40)];
            NSDictionary *resultDict= [NSJSONSerialization JSONObjectWithData:[cleanedData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
            
            if(error == nil){
                NSDictionary *initialResultData = [resultDict objectForKey:@"ResultSet"];
                yahooSearchResult = [initialResultData objectForKey:@"Result"];

                [self.tableView reloadData];
            }
        }
        else{
            NSLog(@"yahooSearchTickerRequest FAILED!");
        }
    }];
}

#pragma mark -
#pragma signals

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

@end
