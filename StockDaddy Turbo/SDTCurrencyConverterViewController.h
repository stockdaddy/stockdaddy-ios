//
//  SDTCurrencyConverterViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/30/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTCurrencyConverterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, NSXMLParserDelegate, UITextFieldDelegate>
 
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property NSMutableString *lastUpdatedDate;
@property NSMutableString *currency;
@property NSMutableString *rate;

@end
