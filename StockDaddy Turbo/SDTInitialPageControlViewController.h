//
//  SDTInitialPageControlViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/29/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTPageViewContentViewController.h"

@interface SDTInitialPageControlViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageImages;
- (IBAction)startAgain:(id)sender;
- (IBAction)closeTutorial:(id)sender;

@end
