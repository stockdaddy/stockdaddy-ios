//
//  SDTOptionChainCell.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/26/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTOptionChainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *strike;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *chg;
@property (weak, nonatomic) IBOutlet UILabel *bid;
@property (weak, nonatomic) IBOutlet UILabel *ask;
@property (weak, nonatomic) IBOutlet UILabel *volume;
@property (weak, nonatomic) IBOutlet UILabel *openInt;

@end
