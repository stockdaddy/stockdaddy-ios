//
//  SDTStockNewsViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/26/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTStockNewsCell.h"
#import "SDTTickerQuote.h"
#import "SDTStockNewsWebViewController.h"

@interface SDTStockNewsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *symbol;

@property (weak, nonatomic) IBOutlet UITableView *stockNewsTable;
@property (weak, nonatomic) IBOutlet UIView *loadingCont;

@end
