//
//  SDTStockInfoViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTStockBuy.h"
#import "SDTStockInfoSwitch.h"
#import "SDTStockSellViewController.h"
#import "SDTStockBuyCell.h"
#import "SDTStockBuyViewController.h"

@interface SDTStockInfoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    SDTStockBuy *buyInfo;
}

@property (nonatomic, strong) NSString *portfolioID;
@property (nonatomic, strong) NSString *stockID;
@property (nonatomic, strong) NSString *stockName;
@property (nonatomic, strong) NSMutableArray *stockInfoData;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;

@property (weak, nonatomic) IBOutlet UILabel *currInvestmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *gainLossLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingQtyLabel;

@end
