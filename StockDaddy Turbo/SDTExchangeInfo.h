//
//  SDTExchangeInfo.h
//  StockDaddy Turbo
//
//  Created by Jain on 3/10/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SDTExchangeInfo : NSObject {
}

@property (nonatomic, strong) NSString *googleSymbol;
@property (nonatomic, strong) NSString *exchangeName;

- (id) init;
- (void) initDatabase;
- (void) insertExchangesList;
- (NSString*) getGoogleSymbol:(NSString*)exchName region:(NSString*)region;
- (NSString*) getYahooSymbol:(NSString*)exchName region:(NSString*)region;
- (BOOL) getGoogleSymbol:(NSString*)yahooSym withExchDisp:(NSString*)exchDisp withTypeDisp:(NSString*)typeDisp;
- (BOOL) getExchangeName:(NSString*)yahooSym withExchDisp:(NSString*)exchDisp withTypeDisp:(NSString*)typeDisp;

@end
