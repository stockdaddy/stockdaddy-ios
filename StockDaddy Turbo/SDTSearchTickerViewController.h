//
//  SDTSearchTickerViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/23/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTTickerQuoteViewController.h"
#import "SDTTickerQuote.h"
#import "SDTTickerOptionsViewController.h"

@interface SDTSearchTickerViewController : UITableViewController  <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
    
    NSMutableArray *yahooSearchResult;
    SDTTickerQuote *tickerQuote;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

//Network Requests
-(void) yahooSearchTickerRequest:(NSString*)searchText;

@end
