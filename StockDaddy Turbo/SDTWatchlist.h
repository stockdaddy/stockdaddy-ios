//
//  SDTWatchlist.h
//  StockDaddy Turbo
//
//  Created by Jain on 3/12/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SDTWatchlist : NSObject

@property (nonatomic, strong) NSMutableArray *stockNameList;

- (id) init;
- (void) initDatabase;
- (BOOL) addToWatchlist:(NSString*)symbol stockName:(NSString*)stockName exchName:(NSString*)exchName yahooSymNExch:(NSString*)yahooSym googleSymbol:(NSString*)googSym exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typeDisp preLoaded:(NSInteger) defaultValue;

- (NSMutableArray*) getWatchlistForGoogle;
- (NSMutableArray*) getAllWatchList;
- (BOOL) deleteFromWatchlist:(NSString*)watchID;
- (NSString*) stockNameByStockSymbol:(NSString*)symbol googExchSymbol:(NSString*)googExchSym;
- (void) insertDefaultWatchlistTicker;

@end
