//
//  SDTAppDelegate.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/20/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTAppDelegate.h"

@implementation SDTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    NSLog(@"Application Launched! Initialising DB for Exchange Info, Watchlist, Alert, Notification, Stock, StockBuy, StockSell");
    SDTExchangeInfo *exchangeInfo = [[SDTExchangeInfo alloc] init];
    [exchangeInfo initDatabase];
    
    SDTWatchlist *watchlist = [[SDTWatchlist alloc] init];
    [watchlist initDatabase];
    
    alert= [[SDTAlert alloc] init];
    [alert initDatabase];
    
    SDTNotification *notificationInfo = [[SDTNotification alloc] init];
    [notificationInfo initDatabase];
    
    SDTStock *stock = [[SDTStock alloc] init];
    [stock initDatabase];
    
    SDTStockBuy *stockBuy = [[SDTStockBuy alloc] init];
    [stockBuy initDatabase];
    
    SDTStockSell *stockSell = [[SDTStockSell alloc] init];
    [stockSell initDatabase];
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
        
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    
    alertVC = [[SDTAlertListNewViewController alloc] init];
    
    // Flurry API KEY;
    NSString *apiKey = @"6Z9FXM22YD8ZT9FB7HR8";
    [Flurry startSession:apiKey];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(googleReturnedData:)
                                                 name: @"GoogStockAlertInitData"
                                               object: nil];

    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"GoogStockAlertInitData"];
    
    // schedule a 300 second timer to check for stock alert notification;
    [NSTimer scheduledTimerWithTimeInterval:300 target:self selector:@selector(scheduleStockAlert) userInfo:nil repeats:YES];
    

    // pagecontrol for tutorial
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    UIStoryboard *storyBoard = self.window.rootViewController.storyboard;

    // For Tutorial (only showing for the first time)
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(![[defaults objectForKey:@"showTutorial"] isEqualToString:@"yes"]){
        UIViewController *tabBar = [storyBoard instantiateViewControllerWithIdentifier:@"TutorialPage"];
        self.window.rootViewController = tabBar;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}

- (void) application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Entered Background Fetch Operation");
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://finance.google.com/finance/info?client=ig&q=%@", [self getAlertsGoogleList]]];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            if (error) {
                                                completionHandler(UIBackgroundFetchResultFailed);
                                                return;
                                            }
                                            
                                            BOOL hasNewData =  [alertVC checkForNotification:[self NSDataToNSArray:data]];

                                            // Parse response/data and determine whether new content was available
                                            if (hasNewData) {
                                                NSLog(@"New Data for BackgroundFetch!");
                                                
                                                UILocalNotification *localNotif = [[UILocalNotification alloc]init];
                                                localNotif.fireDate = Nil;
                                                localNotif.alertBody = @"Ticker has hit a Target";
                                                localNotif.soundName = UILocalNotificationDefaultSoundName;
                                                localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                                                
                                                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
                                                
                                                completionHandler(UIBackgroundFetchResultNewData);
                                                
                                            } else {
                                                NSLog(@"No New Data for BackgroundFetch");
                                                completionHandler(UIBackgroundFetchResultNoData);
                                            }
                                        }];
    
    [task resume];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"App Entered Foreground. Clearing any Local Notifications!");
    if(application.applicationIconBadgeNumber){
        application.applicationIconBadgeNumber = 0;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"GoogStockAlertInitData"
                                                  object: nil];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alertWindow = [[UIAlertView alloc] initWithTitle:@"Stock Alerts"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alertWindow show];
    }
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}

#pragma mark -
#pragma helper functions

- (NSString*) getAlertsGoogleList
{
    return [alert getStockListForGoogle];
}

- (NSArray*) NSDataToNSArray:(NSData*)googleData
{
    NSError *error = nil;
    NSString *dataToString = [[NSString alloc] initWithData:googleData encoding:NSASCIIStringEncoding];
    
    if([dataToString rangeOfString:@"// "].location != NSNotFound){
        dataToString = [dataToString substringFromIndex:3];
    }
    
    NSArray *resultDict= [NSJSONSerialization JSONObjectWithData:[dataToString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];

    return resultDict;
}

#pragma mark -
#pragma schedule alert timer

- (void) scheduleStockAlert
{
    NSLog(@"5 minute timer over");
    if([[alert getStockListForGoogle] length] != 0){
        [tickerQuote googleTickerQuote:[self getAlertsGoogleList]];
    }
}

#pragma mark -
#pragma stock alert data request

- (void) googleReturnedData:(NSNotification *)notification
{
    NSLog(@"recieved GoogStockAlertInitData Data");
    NSArray *returnedData = [notification object];
    
    BOOL hasNotification = [alertVC checkForNotification:returnedData];
    if(hasNotification){
        [alertVC showNotification];
    }
}

@end