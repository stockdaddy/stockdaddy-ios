//
//  SDTCurrencyConverterViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/30/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTCurrencyConverterViewController.h"

@interface SDTCurrencyConverterViewController ()

@end

@implementation SDTCurrencyConverterViewController

NSXMLParser *parser;
NSMutableArray *currencyList;
UITextField *amountTextField;
UIPickerView *fromCurrencyPickerView;
BOOL isShowingFromPickerView;
NSString *fromPickerViewLabel;
NSString *fromPickerViewValue;
int fromPickerViewValIndex;
UIPickerView *toCurrencyPickerView;
BOOL isShowingToPickerView;
NSString *toPickerViewLabel;
NSString *toPickerViewValue;
int toPickerViewValIndex;
NSString *xmlElementName;
NSString *xmlAttributeName;
NSString *xmlCurrAttributeName;
NSString *xmlRateAttributeName;
NSMutableArray *currencyExchangeData;
UILabel *conversion;
UILabel *amountConvertion;
UILabel *lastUpdatedLabel;
UIView *conversionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self initializeCurrencyList];
    [self initializePickerView];
    [self initializeAmountTextField];
    [self intializeConversionView];

    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    
    isShowingFromPickerView = NO;
    fromPickerViewLabel = @"Choose Currency";
    isShowingToPickerView = NO;
    fromPickerViewValIndex = 0;
    toPickerViewLabel = @"Choose Currency";
    toPickerViewValIndex = 0;
    
    self.lastUpdatedDate = [[NSMutableString alloc] init];
    self.currency = [[NSMutableString alloc] init];
    self.rate = [[NSMutableString alloc] init];

    xmlElementName = @"";
    xmlAttributeName = @"";
    xmlCurrAttributeName = @"";
    xmlRateAttributeName = @"";
    currencyExchangeData = [[NSMutableArray alloc] init];
    
    NSURL *url = [NSURL URLWithString:@"https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:YES];
    [parser parse];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table View Delegate

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Add header titles in sections.
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    switch (section) {
        case 0:
            title = @"Amount";
            break;
        case 1:
            title = @"From";
            break;
        case 2:
            title = @"To";
            break;

    }
    
    return title;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 3){
        return 10;
    } else if(section == 0) {
        return 40;
    }
    
    return 20.0f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1){
        if(isShowingFromPickerView){
            return 150;
        } else {
            return 44;
        }
    } else if(indexPath.section == 2){
        if(isShowingToPickerView){
            return 150;
        } else {
            return 44;
        }
    } else if(indexPath.section == 3){
        if((([fromPickerViewLabel isEqualToString:@"Choose Currency"]) || ([toPickerViewLabel isEqualToString:@"Choose Currency"]))){
            return 0;
        }
        return 100;
    }
    else {
        return 44;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CurrencyConvCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0:
            [cell.contentView addSubview:amountTextField];
            break;
        case 1:
            if(isShowingFromPickerView){
                [cell.textLabel setText:@""];
                [cell.contentView addSubview:fromCurrencyPickerView];
            } else {
                [fromCurrencyPickerView removeFromSuperview];
                [cell.textLabel setText:fromPickerViewLabel];
            }
            break;
        case 2:
            if(isShowingToPickerView){
                [cell.textLabel setText:@""];
                [cell.contentView addSubview:toCurrencyPickerView];
            } else {
                [toCurrencyPickerView removeFromSuperview];
                [cell.textLabel setText:toPickerViewLabel];
            }
            break;
        case 3:
            // Remove existing views
            [conversion removeFromSuperview];
            [lastUpdatedLabel removeFromSuperview];
            [amountConvertion removeFromSuperview];
            [conversionView removeFromSuperview];

            [conversion setText:[NSString stringWithFormat:@"1 %@ = %@ %@", fromPickerViewValue, [self calculateCurrency], toPickerViewValue]];
            if([[amountTextField text] length] !=0){
                [amountConvertion setText:[NSString stringWithFormat:@"%@ %@ = %@ %@", [amountTextField text], fromPickerViewValue, [self calculateAmountConversion:[amountTextField text]], toPickerViewValue]];
                [conversionView addSubview:amountConvertion];
            }
            [lastUpdatedLabel setText:[NSString stringWithFormat:@"Last updated: %@",self.lastUpdatedDate]];
            [conversionView addSubview:lastUpdatedLabel];
            [conversionView addSubview:conversion];
            [cell.contentView addSubview:conversionView];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
    }

    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1){
        isShowingFromPickerView = !isShowingFromPickerView;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    } else if(indexPath.section == 2){
        isShowingToPickerView = !isShowingToPickerView;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark PickerView Datasource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if(pickerView == fromCurrencyPickerView){
        return [currencyList count];
    } else {
        return [currencyList count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if(pickerView == fromCurrencyPickerView){
        return [[currencyList objectAtIndex:row] objectForKey:@"Label"];
    } else {
        return [[currencyList objectAtIndex:row] objectForKey:@"Label"];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if(pickerView == fromCurrencyPickerView) {
        fromPickerViewLabel = [[currencyList objectAtIndex:row] objectForKey:@"Label"];
        fromPickerViewValue = [[currencyList objectAtIndex:row] objectForKey:@"Value"];
        isShowingFromPickerView = !isShowingFromPickerView;
        fromPickerViewValIndex = (int) row;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
        [self calculateCurrency];
    } else {
        toPickerViewLabel = [[currencyList objectAtIndex:row] objectForKey:@"Label"];
        toPickerViewValue = [[currencyList objectAtIndex:row] objectForKey:@"Value"];
        isShowingToPickerView = !isShowingToPickerView;
        toPickerViewValIndex = (int) row;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
        [self calculateCurrency];
    }
}

#pragma mark -
#pragma mark XML Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    xmlElementName = elementName;
    xmlAttributeName = ([attributeDict objectForKey:@"time"]) ? @"time" : @"";
    xmlCurrAttributeName = ([attributeDict objectForKey:@"currency"]) ? @"currency" : @"";
    xmlRateAttributeName = ([attributeDict objectForKey:@"rate"]) ? @"rate" : @"";

    if(([xmlElementName isEqualToString:@"Cube"]) && ([xmlAttributeName isEqualToString:@"time"])) {
        self.lastUpdatedDate = [attributeDict objectForKey:@"time"];
    }
    
    if(([xmlElementName isEqualToString:@"Cube"]) && ([xmlCurrAttributeName isEqualToString:@"currency"]) && ([xmlRateAttributeName isEqualToString:@"rate"])) {
        self.currency = [attributeDict objectForKey:@"currency"];
        self.rate = [attributeDict objectForKey:@"rate"];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if(([xmlElementName isEqualToString:@"Cube"]) && ([xmlCurrAttributeName isEqualToString:@"currency"]) && ([xmlRateAttributeName isEqualToString:@"rate"])) {
        NSDictionary *data = @{@"Currency": self.currency, @"Rate" : self.rate};
        [currencyExchangeData addObject:data];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
//    NSLog(@"currency Exchange Data is %@", currencyExchangeData);
}

#pragma mark -
#pragma mark TextField Delegate

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    return YES;
}

#pragma mark -
#pragma mark Currency Calculations

// returns 54.57
- (NSString*) calculateCurrency
{
    float fromExchValue;
    float toExchValue;
    
    for (NSDictionary *curr in currencyExchangeData){
        
        if ([[curr objectForKey:@"Currency"] isEqualToString:fromPickerViewValue]){
            fromExchValue = [[curr objectForKey:@"Rate"] floatValue];
        }
        
        if ([[curr objectForKey:@"Currency"] isEqualToString:toPickerViewValue]){
            toExchValue = [[curr objectForKey:@"Rate"] floatValue];
        }
        if([fromPickerViewValue isEqualToString:@"EUR"]){
            fromExchValue = 1;
        }
        if([toPickerViewValue isEqualToString:@"EUR"]){
            toExchValue = 1;
        }
    }
    
    float exchangePrice = toExchValue / fromExchValue;
    return [NSString stringWithFormat:@"%0.2f", exchangePrice];
}

// returns 10 CAD = 54.57 INR
- (NSString*) calculateAmountConversion:(NSString*)amount
{
    return [NSString stringWithFormat:@"%0.2f", ([amount floatValue] * [[self calculateCurrency] floatValue])];
}

#pragma mark -
#pragma mark Initialization stuff

- (void) initializeCurrencyList
{
    currencyList = [[NSMutableArray alloc] init];

    NSDictionary *usd = @{@"Label": @"USD - US Dollar", @"Value" : @"USD"};
    [currencyList addObject:usd];
    
    NSDictionary *eur = @{@"Label": @"EUR - EURO", @"Value" : @"EUR"};
    [currencyList addObject:eur];

    NSDictionary *aud = @{@"Label": @"AUD - Australian Dollar", @"Value" : @"AUD"};
    [currencyList addObject:aud];

    NSDictionary *bgn = @{@"Label": @"BGN - Bulgarian Lev", @"Value" : @"BGN"};
    [currencyList addObject:bgn];

    NSDictionary *cad = @{@"Label": @"CAD - Canadian Dollar", @"Value" : @"CAD"};
    [currencyList addObject:cad];

    NSDictionary *chf = @{@"Label": @"CHF - Swiss Franc", @"Value" : @"CHF"};
    [currencyList addObject:chf];

    NSDictionary *cny = @{@"Label": @"CNY - Chinese Yuan", @"Value" : @"CNY"};
    [currencyList addObject:cny];

    NSDictionary *czk = @{@"Label": @"CZK - Czech Koruna", @"Value" : @"CZK"};
    [currencyList addObject:czk];

    NSDictionary *dkk = @{@"Label": @"DKK - Danish Krone", @"Value" : @"DKK"};
    [currencyList addObject:dkk];

    NSDictionary *gbp = @{@"Label": @"GBP - British Pound", @"Value" : @"GBP"};
    [currencyList addObject:gbp];

    NSDictionary *hkd = @{@"Label": @"HKD - Hong Kong Dollar", @"Value" : @"HKD"};
    [currencyList addObject:hkd];

    NSDictionary *hrk = @{@"Label": @"HRK - Croatian Kuna", @"Value" : @"HRK"};
    [currencyList addObject:hrk];

    NSDictionary *huf = @{@"Label": @"HUF - Hungarian Forint", @"Value" : @"HUF"};
    [currencyList addObject:huf];

    NSDictionary *idr = @{@"Label": @"IDR - Indonesian Rupiah", @"Value" : @"IDR"};
    [currencyList addObject:idr];

    NSDictionary *ils = @{@"Label": @"ILS - Israeli Shekel", @"Value" : @"ILS"};
    [currencyList addObject:ils];

    NSDictionary *inr = @{@"Label": @"INR - Indian Rupee", @"Value" : @"INR"};
    [currencyList addObject:inr];

    NSDictionary *jpy = @{@"Label": @"JPY - Japanese Yen", @"Value" : @"JPY"};
    [currencyList addObject:jpy];

    NSDictionary *krw = @{@"Label": @"KRW - South Korean Won", @"Value" : @"KRW"};
    [currencyList addObject:krw];

    NSDictionary *ltl = @{@"Label": @"LTL - Lithuanian Litas", @"Value" : @"LTL"};
    [currencyList addObject:ltl];

    NSDictionary *mxn = @{@"Label": @"MXN - Mexican Peso", @"Value" : @"MXN"};
    [currencyList addObject:mxn];

    NSDictionary *myr = @{@"Label": @"MYR - Malaysian Ringgit", @"Value" : @"MYR"};
    [currencyList addObject:myr];

    NSDictionary *nok = @{@"Label": @"NOK - Norwegian Krone", @"Value" : @"NOK"};
    [currencyList addObject:nok];

    NSDictionary *nzd = @{@"Label": @"NZD - New Zealand Dollar", @"Value" : @"NZD"};
    [currencyList addObject:nzd];

    NSDictionary *php = @{@"Label": @"PHP - Philippine Peso", @"Value" : @"PHP"};
    [currencyList addObject:php];

    NSDictionary *pln = @{@"Label": @"PLN - Polish Zioty", @"Value" : @"PLN"};
    [currencyList addObject:pln];

    NSDictionary *ron = @{@"Label": @"RON - Romanian New Leu", @"Value" : @"RON"};
    [currencyList addObject:ron];

    NSDictionary *rub = @{@"Label": @"RUB - Russian Ruble", @"Value" : @"RUB"};
    [currencyList addObject:rub];

    NSDictionary *sek = @{@"Label": @"SEK - Swedish Krona", @"Value" : @"SEK"};
    [currencyList addObject:sek];

    NSDictionary *sgd = @{@"Label": @"SGD - Singapore Dollar", @"Value" : @"SGD"};
    [currencyList addObject:sgd];

    NSDictionary *thb = @{@"Label": @"THB - Thai Baht", @"Value" : @"THB"};
    [currencyList addObject:thb];

    NSDictionary *try = @{@"Label": @"TRY - Turkish Lira", @"Value" : @"TRY"};
    [currencyList addObject:try];

    NSDictionary *zar = @{@"Label": @"ZAR - South African Rand", @"Value" : @"ZAR"};
    [currencyList addObject:zar];
}

-(void) initializePickerView
{
    fromCurrencyPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    fromCurrencyPickerView.dataSource = self;
    fromCurrencyPickerView.delegate = self;
    
    toCurrencyPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    toCurrencyPickerView.dataSource = self;
    toCurrencyPickerView.delegate = self;
}

-(void) initializeAmountTextField
{
    amountTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 320, 35)];
    
    amountTextField.adjustsFontSizeToFitWidth = YES;
    amountTextField.placeholder = @"15";
    amountTextField.backgroundColor = [UIColor whiteColor];
    amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    amountTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:20.0f];
    amountTextField.delegate = self;
    
}

-(void) intializeConversionView
{
    conversion = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 320, 30)];
    [conversion setFont:[UIFont fontWithName:@"Apple SD Gothic Neo" size:18.0f]];

    amountConvertion = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, 320, 30)];
    [amountConvertion setFont:[UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f]];

    lastUpdatedLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 75, 320, 20)];
    [lastUpdatedLabel setFont:[UIFont fontWithName:@"Apple SD Gothic Neo" size:12.0f]];

    conversionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
}

#pragma mark -
#pragma mark notifications

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
}

@end
