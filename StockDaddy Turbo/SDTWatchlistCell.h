//
//  SDTWatchlistCell.h
//  StockDaddy Turbo
//
//  Created by Jain on 3/23/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTWatchlistCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *symbol;
@property (weak, nonatomic) IBOutlet UILabel *stockName;
@property (weak, nonatomic) IBOutlet UIImageView *containerColor;
@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *chgPct;
@property (weak, nonatomic) IBOutlet UIImageView *mainCont;
@end
