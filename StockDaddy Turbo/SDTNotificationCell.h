//
//  SDTNotificationCell.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTNotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *stockName;
@property (weak, nonatomic) IBOutlet UILabel *dateTime;
@property (weak, nonatomic) IBOutlet UILabel *msg;

@end
