//
//  SDTPortfolio.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/22/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SDTStock.h"
#import "SDTStockBuy.h"
#import "SDTStockSell.h"

@interface SDTPortfolio : NSObject {
    NSString *databasePath;
    sqlite3 *stockDaddyDB;
    SDTStock *stockInfo;
    SDTStockBuy *stockBuyInfo;
    SDTStockSell *stockSellInfo;
}

- (id) init;

- (BOOL) insertPortName:(NSString*)portName insertExchType:(NSString*)exchType insertRegion:(NSString*)region insertExchTypeSymb:(NSString*)exchTypeSymb insertGoogleExchSymb:(NSString*)googleExchSymb insertCurrency:(NSString*)currency insertNotes:(NSString*)notes;

- (BOOL) deleteFromPortfolioTable:(NSString*)portfolioID;

-(BOOL) updatePortfolioForID:(NSString*)portfolioID portfolioName:(NSString*)portName portfolioNotes:(NSString*)portNotes;

- (NSString*) getPortfolioNameFromPortfolioID:(NSString*)portID;

- (NSString*) googleRequestURLSymList:(NSString*)portfolioID;
- (NSMutableArray *) getAllPortfolioForTableViewDS;
- (NSMutableArray*) getPortfolioList:(NSString*)exchName;

- (NSString*) portfolioTotalInvestment:(NSString*)portID;
- (NSString*) portfolioNetWorth:(NSString*)portID;
- (NSString*) portfolioTodaysGain:(NSString*)portID;
- (NSString*) portfolioCurrentGain:(NSString*)portID;
- (NSString*) portfolioCurrentGainPct:(NSString*)portID;
- (NSString*) portfolioGainLoss:(NSString*)portID;
- (NSString*) portfolioGainLossPct:(NSString*)portID;
- (NSDictionary*) portfolioMaxGainer:(NSString*)portID;
- (NSDictionary*) portfolioMaxLoser:(NSString*)portID;

- (NSString*) sumBuyQuan:(NSString*)portID stockID:(NSString*)stockID;
- (NSString*) avgBuyPrice:(NSString*)portID stockID:(NSString*)stockID;

- (NSMutableArray*) portfolioNetworthDS:(NSString*)portfolioID;
- (NSArray*) portfolioMaxMinDS:(NSString*)portfolioID sortAscending:(BOOL)asc;
- (NSMutableArray*) portfolioTodaysGainDS:(NSString*)portfolioID;
- (NSMutableArray*) portfolioCurrentGainDS:(NSString*)portfolioID;
- (NSMutableArray*) portfolioTotalInvestmentDS:(NSString*)portfolioID;
- (NSMutableArray*) portfolioGainLossDS:(NSString*)portfolioID;


@property (nonatomic, strong) NSMutableArray *googleData;

@end
