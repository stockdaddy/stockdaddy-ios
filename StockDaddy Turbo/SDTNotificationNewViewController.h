//
//  SDTNotificationNewViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTNotification.h"
#import "SDTNotificationCell.h"

@interface SDTNotificationNewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    SDTNotification *notificationInfo;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *noDataCont;

@end
