//
//  SDTTickerQuote.h
//  StockDaddy Turbo
//
//  Created by Jain on 2/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDTTickerQuoteViewController.h"

@interface SDTTickerQuote : NSObject <NSURLConnectionDelegate> {
    NSMutableData *_responseData;
}

@property (nonatomic, strong) NSString *postNotifName;

- (id) initWithPostNotificationName:(NSString*)postNotName;

- (void) yahooTickerQuote:(NSString*)searchText;
- (void) yahooOptionChainQuote:(NSString*)symbol;
- (void) yahooStockNews:(NSString*)symbol;
- (void) googleTickerQuote:(NSString*)symbol;
- (void) indianTickerQuote:(NSString*)searchText;

@end
