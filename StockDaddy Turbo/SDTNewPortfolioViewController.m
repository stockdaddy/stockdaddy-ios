//
//  SDTNewPortfolioViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/26/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTNewPortfolioViewController.h"

@interface SDTNewPortfolioViewController ()

@end

@implementation SDTNewPortfolioViewController

NSMutableDictionary *regionData;
NSArray *regions;
NSMutableArray *stockExchangeData;
BOOL isShowingRegion;
int selectedRegionDataIndex;
BOOL isShowingStockExchange;
int selectedStockExchangeIndex;
UITextField *portNameTextFieldNew;
UITextView *portNotes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if(self.isEditing){
        [self fillFormForEditing];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    exchangeInfo = [[SDTExchangeInfo alloc] init];
    portfolio = [[SDTPortfolio alloc] init];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    
    regionData = [[NSMutableDictionary alloc] init];
    [self initializeRegionData];
    regions = [[NSArray alloc] initWithArray:[regionData allKeys]];
    stockExchangeData = [[NSMutableArray alloc] init];
    [self updateStockExchangeData];
    
    isShowingRegion = NO;
    selectedRegionDataIndex = 0;
    isShowingStockExchange = NO;
    selectedStockExchangeIndex = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    [self initializeTextField];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma table view properties

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    } else if(section == 1) {
        if(isShowingRegion){
            return [regions count];
        } else {
            return 1;
        }
    } else if(section == 2){
        if(isShowingStockExchange){
            return [stockExchangeData count];
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

// Add header titles in sections.
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    switch (section) {
        case 0:
            title = @"Portfolio Name";
            break;
        case 1:
            title = @"Region";
            break;
        case 2:
            title = @"Stock Exchange Type";
            break;
        case 3:
            title = @"Portfolio Notes";
            break;
    }
    
    return title;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 40.0f;
    } else {
        return 30.0f;
    }
}

// Set the row height.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section != 3){
        return 35.0;
    } else {
        return 75.0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewPortfolioCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
    if(indexPath.section == 0){
        [cell.contentView addSubview:portNameTextFieldNew];
    }
    else if(indexPath.section == 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if(!isShowingRegion){
            [cell.textLabel setText:[regions objectAtIndex:selectedRegionDataIndex]];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            [cell.textLabel setText:[regions objectAtIndex:indexPath.row]];
            if(indexPath.row == selectedRegionDataIndex){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
    }
    else if(indexPath.section == 2){
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if(!isShowingStockExchange){
            [cell.textLabel setText:[stockExchangeData objectAtIndex:selectedStockExchangeIndex]];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            [cell.textLabel setText:[stockExchangeData objectAtIndex:indexPath.row]];
            
            if(indexPath.row == selectedStockExchangeIndex){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
    }
    else {
        [cell.contentView addSubview:portNotes];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if editing is true, then we don't want the user to change the stock exchange & region.
    if(!self.isEditing){
        if(indexPath.section == 1){
            if(isShowingRegion){
                selectedRegionDataIndex = (int)indexPath.row;
            }
            isShowingRegion = !isShowingRegion;
            [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
            selectedStockExchangeIndex = 0;
            [self updateStockExchangeData];
            [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
            
        } else  if(indexPath.section == 2){
            if(isShowingStockExchange){
                selectedStockExchangeIndex = (int)indexPath.row;
            }
            isShowingStockExchange = !isShowingStockExchange;
            
            [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark actions
- (IBAction)cancelButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButton:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Add To Portfolio" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

    NSString *region = [regions objectAtIndex:selectedRegionDataIndex];
    NSString *exchName = [[regionData objectForKey:region] objectAtIndex:selectedStockExchangeIndex];
    NSString *yahooSym = [exchangeInfo getYahooSymbol:exchName region:region];
    NSString *googSym = [exchangeInfo getGoogleSymbol:exchName region:region];

    NSLog(@"Portfolio Name Text is %@", [portNameTextFieldNew text]);
    NSLog(@"Region is %@", region);
    NSLog(@"Exchange Name is %@", exchName);
    NSLog(@"YahooSymbol Name is %@", yahooSym);
    NSLog(@"GoogleSymbol Name is %@", googSym);
    NSLog(@"Portfolio Notes are %@", [portNotes text]);
    
    // empty
    if([[portNameTextFieldNew text] length] == 0){
        alert.message = @"Portfolio Name cannot be Empty.";
        [alert show];
    } else if([googSym length] == 0){
        alert.message = @"Cannot create the Portfolio with the selected Exchange Type due to Insufficient Information available at the moment.";
        [alert show];
    } else {
        
        if(self.isEditing){
            NSLog(@"Edit Save Portfolio");
            if([portfolio updatePortfolioForID:self.portfolioID portfolioName:[portNameTextFieldNew text] portfolioNotes:[portNotes text]]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NewPortfolioAdded" object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        } else {
            if( [portfolio insertPortName:[portNameTextFieldNew text] insertExchType:exchName insertRegion:region insertExchTypeSymb:yahooSym insertGoogleExchSymb:googSym insertCurrency:@"" insertNotes:[portNotes text]] ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NewPortfolioAdded" object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        } // end of isEditing else
    } // end of validation
}

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma helper function

-(void) initializeRegionData
{
    NSArray *multiExchange = @[@"Multi-Exchange"];
    NSArray *northAmerica = @[@"American Stock Exchange", @"BATS Exchange", @"Chicago Board of Trade", @"Chicago Mercantile Exchange", @"Dow Jones Indexes", @"NASDAQ Stock Exchange", @"New York Board of Trade", @"New York Commodities Exchange", @"New York Mercantile Exchange", @"New York Stock Exchange", @"OTC Bulletin Board Market", @"Pink Sheets", @"S & P Indices", @"Toronto Stock Exchange", @"TSX Venture Exchange"];
    
    NSArray *asiaPacific = @[@"Shanghai Stock Exchange", @"Shenzhen Stock Exchange", @"Hong Kong Stock Exchange", @"National Stock Exchange of India", @"Jakarta Stock Exchange", @"Nikkei Indices", @"Singapore Stock Exchange", @"Korea Stock Exchange", @"KOSDAQ", @"Taiwan OTC Exchange", @"Taiwan Stock Exchange"];
    
    NSArray *europe = @[@"Vienna Stock Exchange", @"Copenhagen Stock Exchange", @"Euronext", @"Paris Stock Exchange", @"Berlin Stock Exchange", @"Bremen Stock Exchange", @"Dusseldorf Stock Exchange", @"Frankfurt Stock Exchange", @"Hamburg Stock Exchange", @"Hanover Stock Exchange", @"Munich Stock Exchange", @"Stuttgart Stock Exchange", @"XETRA Stock Exchange", @"Milan Stock Exchange", @"Amsterdam Stock Exchange", @"Oslo Stock Exchange", @"Barcelona Stock Exchange", @"Bilbao Stock Exchange", @"Madrid Fixed Income Market", @"Madrid SE C.A.T.S.", @"Madrid Stock Exchange", @"Stockholm Stock Exchange", @"Swiss Exchange", @"FTSE Indices", @"London Stock Exchange"];
    
    NSArray *southAmerica = @[@"BOVESPA - Sao Paolo Stock Exchange", @"Santiago Stock Exchange", @"Mexico Stock Exchange"];
    
    NSArray *southPacific = @[@"Australian Stock Exchange", @"New Zealand Stock Exchange"];
    
    [regionData setObject:multiExchange forKey:@"Multi-Exchange"];
    [regionData setObject:northAmerica forKey:@"North America"];
    [regionData setObject:europe forKey:@"Europe"];
    [regionData setObject:southAmerica forKey:@"South America"];
    [regionData setObject:southPacific forKey:@"South Pacific"];
    [regionData setObject:asiaPacific forKey:@"Asia-Pacific"];
}

-(void) updateStockExchangeData
{
    stockExchangeData = [regionData objectForKey:[regions objectAtIndex:selectedRegionDataIndex]];
}

-(void) initializeTextField
{
    portNameTextFieldNew = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    portNameTextFieldNew.adjustsFontSizeToFitWidth = YES;
    portNameTextFieldNew.placeholder = @"Personal";
    portNameTextFieldNew.backgroundColor = [UIColor whiteColor];
    portNameTextFieldNew.keyboardType = UIKeyboardTypeAlphabet;
    
    portNotes = [[UITextView alloc] initWithFrame:CGRectMake(15, 0, 320, 75)];
    portNotes.backgroundColor = [UIColor whiteColor];
    portNotes.keyboardType = UIKeyboardTypeAlphabet;
}

-(void) fillFormForEditing
{
    self.navigationItem.title = @"Edit Portfololio";
    portNameTextFieldNew.text = self.portfolioName;
    portNotes.text = self.portNotes;
    selectedRegionDataIndex = (int)[regions indexOfObject:self.region];
    selectedStockExchangeIndex = (int)[[regionData objectForKey:self.region] indexOfObject:self.exchType];
  
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    [self updateStockExchangeData];
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
}

@end
