//
//  SDTWatchlistNewViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/13/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTWatchlist.h"
#import "SDTWatchlistCell.h"
#import "SDTTickerQuote.h"
#import "SDTTickerQuoteViewController.h"
#import <iAd/iAd.h>

@interface SDTWatchlistNewViewController : UIViewController  <UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate> {
    SDTWatchlist *watchlistInfo;
    SDTTickerQuote *tickerQuote;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIView *noDataCont;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActInd;
@property (weak, nonatomic) IBOutlet UILabel *loadingLab;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@property (weak, nonatomic) IBOutlet ADBannerView *adBanner;
@end
