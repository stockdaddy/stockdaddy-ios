//
//  SDTTickerQuoteViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 2/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTTickerQuoteViewController.h"

@interface SDTTickerQuoteViewController ()

@end

@implementation SDTTickerQuoteViewController

SDTTickerQuote *tickerQuote;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self doNetworkRequest];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // set Text as "Facebook (FB)"
    [self.symbolLabel setText: [NSString stringWithFormat: @"%@", self.symbol]];
    [self.companyNameLabel setText: [NSString stringWithFormat: @"%@", self.companyName]];
    
    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooQuoteData"];
    
    self.loadingView.hidden = false;
    self.activityIndicator.hidden = false;
    self.loadingLabel.hidden = false;
    self.DataNotFoundLabel.hidden = true;
    self.RefreshButtonOutlet.hidden = true;
    
    UIBarButtonItem *optionBarItem = [[UIBarButtonItem alloc] initWithTitle:@"\uf03a"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(stockOptionsVC)];
    
    [optionBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"FontAwesome" size:20.0]} forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = optionBarItem;
    
    self.adBanner.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadUIView:(NSNotification *)notification
{
    NSLog(@"recieved YahooQuoteData Data");
    self.loadingView.hidden = true;
    self.activityIndicator.hidden = true;
    self.loadingLabel.hidden = true;
    self.DataNotFoundLabel.hidden = true;
    self.RefreshButtonOutlet.hidden = true;
    
    NSDictionary *returnedData = [notification object];
    
    if(([self.symbol rangeOfString:@".NS"].location == NSNotFound) && ([self.symbol rangeOfString:@".BO"].location == NSNotFound)) {
        NSLog(@"Its Not an Indian Stock, Updating with Yahoo Data");
        
        if([returnedData objectForKey:@"query"] != [NSNull null]){
            NSDictionary *queryData = [returnedData objectForKey:@"query"];
            if([queryData objectForKey:@"results"] != [NSNull null]){
                NSDictionary *resultsData = [queryData objectForKey:@"results"];
                if([resultsData objectForKey:@"quote"] != [NSNull null]){
                    NSDictionary *quoteData = [resultsData objectForKey:@"quote"];
                    
                    // Update the Labels
                    [self reloadTickerLabels:quoteData];
                    
                } else { NSLog(@"QuoteData is NULL"); }
                
            } else {
                NSLog(@"Results is NULL. Posting DataNotFound Notification");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DataNotFound" object: nil];
            }
            
        } else { NSLog(@"QueryData is NULL"); }
        
    } else {
        NSLog(@"It is an Indian Stock, Updating with Indian Source Data");
        
        // Update the Labels (Indian)
        [self reloadIndianTickerLabels:returnedData];
    }
    
    // Remove it as an observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"DataNotFound"
                                                  object: nil];

    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"YahooQuoteData"
                                                  object: nil];
}

- (void)DataNotFound:(NSNotification *)notification
{
    NSLog(@"recieved DataNotFound Data");
    self.loadingView.hidden = false;
    self.DataNotFoundLabel.hidden = false;
    self.RefreshButtonOutlet.hidden = false;
    self.loadingLabel.hidden = true;
    self.activityIndicator.hidden = true;

    // Remove it as an observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"DataNotFound"
                                                  object: nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"YahooQuoteData"
                                                  object: nil];
}

- (IBAction)RefreshButton:(id)sender
{
    NSLog(@"Refresh Button Clicked for Ticker Quote!");
    
    // initiate the Yahoo Search Request (check if indian stock or not)
    [self doNetworkRequest];
}

- (void)reloadTickerLabels:(NSDictionary *)returnedData
{

    NSLog(@"Returned Yahoo Data is %@", returnedData);
    
    // Update Main Ticker Quote Label
    if([returnedData objectForKey:@"LastTradePriceOnly"] != [NSNull null]) {
        [self.priceLabel setText: ([returnedData objectForKey:@"LastTradePriceOnly"] != [NSNull null]) ? [returnedData objectForKey:@"LastTradePriceOnly"] : @"-"];
    } else { [self.priceLabel setText: @"-"]; }
    
    // Update Main Ticker Quote Pct Change & Value Label
    if([returnedData objectForKey:@"Change"] != [NSNull null] && ([returnedData objectForKey:@"PercentChange"] != [NSNull null])) {
        [self.priceChangeLabel setText: [NSString stringWithFormat: @"%@ (%@)", [returnedData objectForKey:@"Change"], [returnedData objectForKey:@"PercentChange"]]];
    } else {[self.priceChangeLabel setText: @""];}

    self.chgVal = [returnedData objectForKey:@"Change"];
    self.chgPct = [returnedData objectForKey:@"PercentChange"];
    
    // Update Ticker Last Update Time&Date Label
    if([returnedData objectForKey:@"LastTradeTime"] != [NSNull null] && ([returnedData objectForKey:@"LastTradeDate"] != [NSNull null])) {
        [self.lastUpatedLabel setText: [NSString stringWithFormat: @"Last Updated: %@ at %@", [returnedData objectForKey:@"LastTradeDate"], [returnedData objectForKey:@"LastTradeTime"]]];
    } else {[self.lastUpatedLabel setText: @""];}
    
    // Update Ticker Previous close Label
    [self.previousCloseLabel setText: ([returnedData objectForKey:@"PreviousClose"] != [NSNull null]) ? [returnedData objectForKey:@"PreviousClose"] : @"-"];
    
    // Update Ticker Open Qutoe Label
    [self.openLabel setText: ([returnedData objectForKey:@"Open"] != [NSNull null]) ? [returnedData objectForKey:@"Open"] : @"-"];
    
    // Update Ticker Bid Quote Label
    if([returnedData objectForKey:@"BidRealtime"] == [NSNull null]){
        if([returnedData objectForKey:@"Bid"]  == [NSNull null]){
            [self.bidLabel setText: @"-"];
        } else { [self.bidLabel setText: [returnedData objectForKey:@"Bid"]]; }
    } else { [self.bidLabel setText: [returnedData objectForKey:@"BidRealtime"]]; }
    
    // Update Ticker Ask Quote Label
    if([returnedData objectForKey:@"AskRealtime"] == [NSNull null]){
        if([returnedData objectForKey:@"Ask"]  == [NSNull null]){
            [self.askLabel setText: @"-"];
        } else { [self.askLabel setText: [returnedData objectForKey:@"Ask"]]; }
    } else { [self.askLabel setText: [returnedData objectForKey:@"AskRealtime"]]; }
    
    // Update Ticker 1y Est Label
    [self.oneYearEstLabel setText: ([returnedData objectForKey:@"OneyrTargetPrice"] != [NSNull null]) ? [returnedData objectForKey:@"OneyrTargetPrice"] : @"-"];
    
    // Update Ticker Dividend Yield Label
    [self.divYieldLabel setText: ([returnedData objectForKey:@"DividendYield"] != [NSNull null]) ? [returnedData objectForKey:@"DividendYield"] : @"-"];
    
    // Update Ticker Days Range Label
    [self.daysRangeLabel setText: ([returnedData objectForKey:@"DaysRange"] != [NSNull null]) ? [returnedData objectForKey:@"DaysRange"] : @"-"];
    
    // Update Ticker 1y Range Label
    [self.oneYearRangeLabel setText: ([returnedData objectForKey:@"YearRange"] != [NSNull null]) ? [returnedData objectForKey:@"YearRange"] : @"-"];
    
    // Update Ticker Avg Daily Volume Label
    float avgVol = [[returnedData objectForKey:@"AverageDailyVolume"] floatValue] / 1000000;
    [self.avgVolLabel setText: ([returnedData objectForKey:@"AverageDailyVolume"] != [NSNull null]) ? [NSString stringWithFormat:@"%0.02fM", avgVol] : @"-"];
    
    // Update Ticker Market Captilization Label
    [self.mktCapLabel setText: ([returnedData objectForKey:@"MarketCapitalization"] != [NSNull null]) ? [returnedData objectForKey:@"MarketCapitalization"] : @"-"];
    
    // Update Ticker PE Ratio Label
    [self.PELabel setText:         ([returnedData objectForKey:@"PERatio"] != [NSNull null]) ? [returnedData objectForKey:@"PERatio"] : @"-"];
    
    // Update Ticker EPS Label
    [self.EPSLabel setText:     ([returnedData objectForKey:@"EarningsShare"] != [NSNull null]) ? [returnedData objectForKey:@"EarningsShare"] : @"-"];
    
    // change the color of the view if stock less than 0
    if([[returnedData objectForKey:@"PercentChange"] doubleValue] >= 0){
        self.quoteView.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
    }else { self.quoteView.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0]; }
}

- (void)reloadIndianTickerLabels:(NSDictionary *)returnedData
{

    NSArray *dataArr = [returnedData objectForKey:@"data"];
    NSDictionary *resultData = [dataArr firstObject];
    NSLog(@"Returned Indian Data is %@", resultData);
    
    // Update Main Ticker Quote Label
    [self.priceLabel setText: ([resultData objectForKey:@"lastPrice"] != [NSNull null]) ? [resultData objectForKey:@"lastPrice"] : @"-"];
    
    // Update Main Ticker Quote Pct Change & Value Label
    [self.priceChangeLabel setText: [NSString stringWithFormat: @"%@ (%@)", [resultData objectForKey:@"change"], [resultData objectForKey:@"pChange"]]];
    
    // Update Ticker Last Update Time&Date Label
    [self.lastUpatedLabel setText: [returnedData objectForKey:@"lastUpdateTime"]];
    
    // Update Ticker Previous close Label
    [self.previousCloseLabel setText: [resultData objectForKey:@"previousClose"]];
    
    // Update Ticker Open Qutoe Label
    [self.openLabel setText: [resultData objectForKey:@"open"]];
    
    // Update Ticker Bid Quote Label
    [self.bidLabel setText: [resultData objectForKey:@"sellPrice1"]];
     
    // Update Ticker Ask Quote Label
    [self.askLabel setText: [resultData objectForKey:@"buyPrice1"]];
    
    // Update Ticker 1y Est Label
    [self.oneYearEstLabel setText: @"-"];
    
    // Update Ticker Dividend Yield Label
    [self.divYieldLabel setText: @"-"];
    
    // Update Ticker Days Range Label
    [self.daysRangeLabel setText: [NSString stringWithFormat:@"%@-%@", [resultData objectForKey:@"dayLow"], [resultData objectForKey:@"dayHigh"]]];
    
    // Update Ticker 1y Range Label
    [self.oneYearRangeLabel setText: [NSString stringWithFormat:@"%@-%@", [resultData objectForKey:@"low52"], [resultData objectForKey:@"high52"]]];

    // Update Ticker Avg Daily Volume Label
    [self.avgVolLabel setText: @"-"];
    
    // Update Ticker Market Captilization Label
    [self.mktCapLabel setText: @"-"];
    
    // Update Ticker PE Ratio Label
    [self.PELabel setText: @"-"];
    
    // Update Ticker EPS Label
    [self.EPSLabel setText: @"-"];
    
    // change the color of the view if stock less than 0
    if([[resultData objectForKey:@"pChange"] doubleValue] >= 0){
        self.quoteView.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
    }else { self.quoteView.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0]; }
}

- (void) stockOptionsVC
{
    SDTTickerOptionsViewController *tickerOptions = [self.storyboard instantiateViewControllerWithIdentifier:@"AddTickerInfo"];
    (void) [tickerOptions initWithSymbol:self.symbol initWithCompanyName:self.companyName initWithExchDisp:self.exchDisp initWithTypeDisp:self.typDisp initWithPrice:self.priceLabel.text initWithChgVal:self.chgVal initWithChgPct:self.chgPct];
    
    [self.navigationController pushViewController:tickerOptions animated:YES];
}

- (void) doNetworkRequest
{
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reloadUIView:)
                                                 name: @"YahooQuoteData"
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(DataNotFound:)
                                                 name: @"DataNotFound"
                                               object: nil];
    // initiate the Yahoo Search Request
    if(([self.symbol rangeOfString:@".BO"].location == NSNotFound) && ([self.symbol rangeOfString:@".NS"].location == NSNotFound)) {
        [tickerQuote yahooTickerQuote:self.symbol];
    }
    else {
        NSLog(@" The first string of the symbol after trimming is %@ ", [[self.symbol componentsSeparatedByString:@"."] firstObject]);
        [tickerQuote indianTickerQuote:[[self.symbol componentsSeparatedByString:@"."] firstObject]];
    }

}

#pragma mark -
#pragma iAd Banner Delegate

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"TickerQuote Banner failed to load because of %@", error);
    self.adBanner.hidden = YES;
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    
    if( ([defaults objectForKey:@"showAds"]) && ([[defaults objectForKey:@"showAds"] isEqualToString:@"RemoveThem"])){
        self.adBanner.hidden = YES;
    } else {
        self.adBanner.hidden = NO;
    }

}

//-(void)bannerViewWillLoadAd:(ADBannerView *)banner
//{
//}

//-(void)bannerViewActionDidFinish:(ADBannerView *)banner
//{
//}

@end

