//
//  SDTOptionChainsViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/24/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTOptionChainsViewController.h"

@interface SDTOptionChainsViewController ()

@end

@implementation SDTOptionChainsViewController

NSMutableArray *expirationData;
NSMutableArray *callPutData;
NSMutableArray *optionsData;
NSArray *quoteChainData;
BOOL isShowingExpirationList;
int selectedExpirationDataindex;
BOOL isShowingCallPutList;
int selectedCallPutindex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    SDTTickerQuote *tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"YahooOptionChainQuote"];
    [tickerQuote yahooOptionChainQuote:self.symbol];
    
    self.header.hidden = YES;
    self.noDataCont.hidden = NO;
    self.noDataLabel.text = @"Loading ...";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reloadUIView:)
                                                 name: @"YahooOptionChainQuote"
                                               object: nil];
    
    expirationData = [[NSMutableArray alloc] init];
    callPutData = [[NSMutableArray alloc] init];
    optionsData = [[NSMutableArray alloc] init];

    NSMutableDictionary *tempCall = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *tempPut = [[NSMutableDictionary alloc] init];
    
    [tempCall setObject:@"Call" forKey:@"Name"];
    [tempCall setObject:@"C" forKey:@"Value"];
    [callPutData addObject:tempCall];

    [tempPut setObject:@"Put" forKey:@"Name"];
    [tempPut setObject:@"P" forKey:@"Value"];
    [callPutData addObject:tempPut];
    
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    self.optionDataTable.dataSource = self;
    self.optionDataTable.delegate = self;
    
    isShowingExpirationList = NO;
    selectedExpirationDataindex = 0;
    isShowingCallPutList = NO;
    selectedExpirationDataindex = 0;

    [self.mainTable.superview addSubview:self.header];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma table view properties

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == self.mainTable){
        return 2;
    } else {
        return 1;
    }
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.mainTable){
        if(section == 0){
            if(isShowingExpirationList){
                return expirationData.count;
            } else {
                return 1;
            }
        } else {
            if(isShowingCallPutList){
                return callPutData.count;
            } else {
                return 1;
            }
        }
    }
    else {
        return optionsData.count;
    }
}

// Add header titles in sections.
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView == self.mainTable){
        if(section == 0){
            return @"Expiration";
        }
        else {
            return @"Type";
        }
    } else {
        return @"";
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.mainTable){
        if(section == 0){
            return 20.0f;
        } else {
            return 30.0f;
        }
    } else {
        return 0.0f;
    }
}


// Set the row height.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.mainTable){
        return 30.0;
    } else {
        return 20.0;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.mainTable){
        static NSString *CellIdentifier = @"OptionsCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        // Configure the cell.
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if(expirationData.count != 0) {
            // Section for Expiration
            if(indexPath.section == 0){
                if(!isShowingExpirationList){
                    [cell.textLabel setText:[expirationData objectAtIndex:selectedExpirationDataindex]];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                } else {
                    [cell.textLabel setText:[expirationData objectAtIndex:indexPath.row]];
                    if(indexPath.row == selectedExpirationDataindex){
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    } else {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
            }
            // section for CallPut
            else {
                if(!isShowingCallPutList){
                    [cell.textLabel setText:[[callPutData objectAtIndex:selectedCallPutindex] objectForKey:@"Name"] ];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                } else {
                    [cell.textLabel setText:[[callPutData objectAtIndex:indexPath.row] objectForKey:@"Name"] ];
                    if(indexPath.row == selectedCallPutindex){
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    } else {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
            }
        }

        cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"optionDataCell";
        SDTOptionChainCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"optionDataCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        if(expirationData.count != 0) {
            [cell.strike setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"strikePrice"]];
            [cell.price setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"lastPrice"]];
            [cell.chg setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"bid"]];
            [cell.bid setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"ask"]];
            [cell.ask setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"strikePrice"]];
            [cell.volume setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"vol"]];
            [cell.openInt setText:[[optionsData objectAtIndex:indexPath.row] objectForKey:@"openInt"]];
        }

        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([expirationData count] != 0){
        if(tableView == self.mainTable){
            if(indexPath.section == 0){
                if(isShowingExpirationList){
                    selectedExpirationDataindex = (int)indexPath.row;
                }
                isShowingExpirationList = !isShowingExpirationList;
                [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            } else {
                if(isShowingCallPutList){
                    selectedCallPutindex = (int)indexPath.row;
                }
                isShowingCallPutList = !isShowingCallPutList;
                [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
            }
            [self updateOptionsData:selectedExpirationDataindex callPutIndex:selectedCallPutindex];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma helper functions

- (void)reloadUIView:(NSNotification *)notification
{
    NSLog(@"recieved YahooOptionChainQuote Data");
    
    NSDictionary *returnedData = [notification object];
    
    if([returnedData objectForKey:@"query"] != [NSNull null]){
        NSDictionary *queryData = [returnedData objectForKey:@"query"];
        if([queryData objectForKey:@"results"] != [NSNull null]){
            NSDictionary *resultsData = [queryData objectForKey:@"results"];
            if([resultsData objectForKey:@"optionsChain"] != [NSNull null]){
                NSArray *optionsChainData = [resultsData objectForKey:@"optionsChain"];

                [self updateExpirationData:optionsChainData];
            }
        }
    }
    
    if([expirationData count] == 0){
        self.header.hidden = YES;
        self.noDataCont.hidden = NO;
        self.noDataLabel.text = @"No Data Found for this Ticker.";
    } else {
        self.header.hidden = NO;
        self.noDataCont.hidden = YES;
        self.noDataLabel.text = @"Loading ...";
    }
    
    // Remove it as an observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"YahooOptionChainQuote"
                                                  object: nil];
}

- (void) updateExpirationData:(NSArray*)optionsChainData
{
    for(NSDictionary *tempDic in optionsChainData) {
        [expirationData addObject:[tempDic objectForKey:@"expiration"]];
    }
    quoteChainData = [[NSArray alloc] initWithArray:optionsChainData];

    [self updateOptionsData:selectedExpirationDataindex callPutIndex:selectedCallPutindex];
}

- (void) updateOptionsData:(NSInteger)expirationIndex callPutIndex:(NSInteger)callPutindex
{
    [optionsData removeAllObjects];
    if(!optionsData) {
        optionsData = [[NSMutableArray alloc] init];
    }
    NSString *expirationDate = [expirationData objectAtIndex:selectedExpirationDataindex];
    NSString *callPutValue = [[callPutData objectAtIndex:selectedCallPutindex] objectForKey:@"Value"];
    
    for(NSDictionary *optionsChain in quoteChainData){
        if([[optionsChain objectForKey:@"expiration"] isEqualToString:expirationDate]){
            for(NSDictionary *option in [optionsChain objectForKey:@"option"]){
                if([callPutValue isEqualToString:[option objectForKey:@"type"]]){
                    [optionsData addObject:option];
                }
            }
        }
    }
    
    NSLog(@"optionsData Count for Expiry %@ for Type %@ %lu", expirationDate, callPutValue, (unsigned long)optionsData.count);

    [self.mainTable reloadData];
    [self.optionDataTable reloadData];    
}

@end
