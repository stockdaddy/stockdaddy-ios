//
//  SDTWorldMarketNewViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/14/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTWorldMarketNewViewController.h"

@interface SDTWorldMarketNewViewController ()

@end

@implementation SDTWorldMarketNewViewController

NSArray *stockIndexName;
NSMutableArray *googleData;
NSArray *regionSection;
UIRefreshControl *refreshControl;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshTable];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    
    googleData = [[NSMutableArray alloc] init];
    
    [self initialiseStockIndexName];
    [self initializeRegionSection];

    refreshControl = [[UIRefreshControl alloc] init];
    [self.mainTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [regionSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // since we know we have 16 indexe
    if([googleData count] ==16){
        if(section == 0){
            return [[googleData subarrayWithRange:NSMakeRange(0, 5)] count];
        }
        else if(section == 1) {
            return [[googleData subarrayWithRange:NSMakeRange(5, 5)] count];
        }
        else if(section == 2) {
            return [[googleData subarrayWithRange:NSMakeRange(10, 4)] count];
        }
        else {
            return [[googleData subarrayWithRange:NSMakeRange(14, 2)] count];
        }
    } else {
        return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [regionSection objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StockIndexCell";
    SDTWatchlistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"StockIndexCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    // Configure the cell...
    NSDictionary *tempDict;
    NSArray *tempIndexName;
    switch (indexPath.section) {
        case 0:
            tempDict = [[googleData subarrayWithRange:NSMakeRange(0, 5)] objectAtIndex:indexPath.row];
            tempIndexName = [stockIndexName subarrayWithRange:NSMakeRange(0, 5)];
            break;
        case 1:
            tempDict = [[googleData subarrayWithRange:NSMakeRange(5, 5)] objectAtIndex:indexPath.row];
            tempIndexName = [stockIndexName subarrayWithRange:NSMakeRange(5, 5)];
            break;
        case 2:
            tempDict = [[googleData subarrayWithRange:NSMakeRange(10, 4)] objectAtIndex:indexPath.row];
            tempIndexName = [stockIndexName subarrayWithRange:NSMakeRange(10, 4)];
            break;
        case 3:
            tempDict = [[googleData subarrayWithRange:NSMakeRange(14, 2)] objectAtIndex:indexPath.row];
            tempIndexName = [stockIndexName subarrayWithRange:NSMakeRange(14, 2)];
            break;
    }
    
    // Configure the cell...
    cell.symbol.text = [tempDict objectForKey:@"t"];
    cell.stockName.text = [tempIndexName objectAtIndex:indexPath.row];
    cell.value.text = [tempDict objectForKey:@"l"];
    cell.chgPct.text = [NSString stringWithFormat:@"%@ (%@%%)", [tempDict objectForKey:@"c"], [tempDict objectForKey:@"cp"]];
    if([[tempDict objectForKey:@"cp"] floatValue] >= 0.0){
        cell.containerColor.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
        cell.mainCont.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.4 alpha:1.0];
    } else {
        cell.containerColor.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
        cell.mainCont.backgroundColor = [UIColor colorWithRed:0.98 green:0.4 blue:0.4 alpha:1.0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


#pragma mark -
#pragma helperMethods

- (void) googleReturnedData:(NSNotification *)notification
{
    NSLog(@"recieved GoogStockMarketData Data");
    
    NSArray *returnedData = [notification object];
    googleData = [NSMutableArray arrayWithArray:returnedData];
    [self.mainTable reloadData];
    
    // Remove as observer
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"GoogStockMarketData"
                                                  object: nil];
    
    self.loadingCont.hidden = YES;
    
    [refreshControl endRefreshing];
}

- (NSString*) googleWorldMarketSymbols
{
    return @"INDEXBVMF:IBOV,INDEXDJX:.DJI,INDEXNASDAQ:.IXIC,INDEXSP:.INX,TSE:OSPTX,INDEXDB:DAX,INDEXEURO:PX1,INDEXFTSE:UKX,INDEXSWX:SMI,INDEXBKK:SET,INDEXBOM:SENSEX,SHA:000001,INDEXHANGSENG:HSI,INDEXNIKKEI:NI225,INDEXASX:XJO,NZE:ALLG";
}

- (void) initialiseStockIndexName
{
    stockIndexName = @[@"BM&F Bovespa", @"Dow Jones Industrial Average", @"NASDAQ Composite", @"S&P 500", @"S&P/TSX Composite index", @"Deutsche Borse Indexes", @"CAC 40", @"FTSE 100", @"SMI PR", @"SET Index", @"BSE SENSEX", @"SSE Composite Index", @"HANG SENG INDEX", @"NIKKEI 225", @"S&P/ASX 200", @"NZX All Index"];
}

- (void) initializeRegionSection
{
    regionSection = @[ @"Americas", @"Europe", @"Asia", @"South Pacific"];
}

- (void) refreshTable
{
    tickerQuote = [[SDTTickerQuote alloc] initWithPostNotificationName:@"GoogStockMarketData"];
    [tickerQuote googleTickerQuote:[self googleWorldMarketSymbols]];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(googleReturnedData:)
                                                 name: @"GoogStockMarketData"
                                               object: nil];
    self.loadingCont.hidden = NO;
}
@end
