//
//  SDTPageViewContentViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/29/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTPageViewContentViewController.h"

@interface SDTPageViewContentViewController ()

@end

@implementation SDTPageViewContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"yes" forKey:@"showTutorial"];
    [defaults synchronize];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
