//
//  SDTStockBuyViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTPortfolio.h"
#import "SDTStockBuy.h"

@interface SDTStockBuyViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource> {
    SDTPortfolio *portfolio;
    SDTStockBuy *stockBuy;
    UIAlertView *alertView;
}

@property (nonatomic, strong) NSString *stockSymbol;
@property (nonatomic, strong) NSString *stockName;
@property (nonatomic, strong) NSString *exchName;
@property (nonatomic, strong) NSString *exchDisp;
@property (nonatomic, strong) NSString *typeDisp;
@property (nonatomic, strong) NSString *googExchSymbol;
@property (nonatomic, strong) NSString *currPrice;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, strong) UIDatePicker *datePickerViewNew;
@property (nonatomic, strong) UIView *dateViewNew;

//For Editing Purposes
@property BOOL isEditing;
@property (nonatomic, strong) NSString *stockBuyID;
@property (nonatomic, strong) NSString *investDate;
@property (nonatomic, strong) NSString *investQuan;

@end
