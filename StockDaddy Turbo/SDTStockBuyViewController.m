//
//  SDTStockBuyViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStockBuyViewController.h"

@interface SDTStockBuyViewController ()

@end

@implementation SDTStockBuyViewController

NSMutableArray *portfolioData;
BOOL isShowingPortfolio;
int selectedPortfolioIndex;
BOOL isShowingDatePicker;
UITextField *buyQuanTextField;
UITextField *buyPriceTextField;
CGPoint offset;
CGFloat mainTableHeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:YES];
    
    if(self.isEditing){
        [self fillFormForEditing];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Add Stock";
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(saveMethod)];

    self.navigationItem.rightBarButtonItem = saveButton;
    
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    
    stockBuy = [[SDTStockBuy alloc] init];
    portfolio = [[SDTPortfolio alloc] init];
    portfolioData = [[NSMutableArray alloc] initWithArray:[portfolio getPortfolioList:self.exchName]];

    isShowingDatePicker = NO;
    isShowingPortfolio = NO;
    selectedPortfolioIndex = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    [self initializeFormTextFields];
    
    alertView = [[UIAlertView alloc]initWithTitle:@"Add To Portfolio" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

    // initalize for cell (buy Date)
    self.dateViewNew = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 162)];
    self.datePickerViewNew = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 162)];
    self.datePickerViewNew.datePickerMode = UIDatePickerModeDate;
    self.datePickerViewNew.hidden = YES;
    self.dateViewNew.hidden = YES;
    [self.dateViewNew addSubview:self.datePickerViewNew];
    
    // For Adjusting TableView when Keyboard Shows
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma TableView Properties

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section == 0){
        if(isShowingPortfolio){
            return [portfolioData count];
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return @"Portfolio";
    } else if(section == 1){
        return @"Stock Name";
    } else if(section == 2){
        return @"Investment Date";
    } else if(section == 3){
        return @"Investment Quantity";
    } else {
        return @"Investment Price";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2){
        return (isShowingDatePicker) ? 169.0f : 35.0f;
    }
    else {
        return 35.0f;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 40.0f;
    } else {
        return 30.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"portfolioList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray *topLevelObjects =
        [[NSBundle mainBundle] loadNibNamed:@"portfolioList" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }

    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    // Section for Portfolio List
    if(indexPath.section == 0){
        if([portfolioData count] == 0){
            [cell.textLabel setText:@"No Portfolio Found."];
        } else {
            if(!isShowingPortfolio){
                [cell.textLabel setText:[[portfolioData objectAtIndex:selectedPortfolioIndex] objectForKey:@"PortfolioName"]];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else {
                [cell.textLabel setText:[[portfolioData objectAtIndex:indexPath.row] objectForKey:@"PortfolioName"]];
                if(indexPath.row == selectedPortfolioIndex){
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    // section for StockName
    else if(indexPath.section == 1){
        [cell.textLabel setText:self.stockName];
    }
    // Section for Buy Date
    else if(indexPath.section == 2){
        cell.textLabel.hidden = (isShowingDatePicker) ? YES : NO;

        if(self.isEditing){
            [cell.textLabel setText:self.investDate];
        } else {
            [cell.textLabel setText:[self convertDateToString:[self.datePickerViewNew date]]];
        }
        [cell.contentView addSubview:self.dateViewNew];
    }
    // Section for Buy Quantity
    else if(indexPath.section == 3){
        [cell.contentView addSubview:buyQuanTextField];
    } else {
        [cell.contentView addSubview:buyPriceTextField];
    }
    // Section for Buy Price
    
    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if([portfolioData count] == 0){
            return;
        } else {
            if(isShowingPortfolio){
                selectedPortfolioIndex = (int)indexPath.row;
            }
            isShowingPortfolio = !isShowingPortfolio;
            [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    // section for datePicker
    else if(indexPath.section == 2) {

        if(isShowingDatePicker) {
            self.datePickerViewNew.hidden = YES;
            self.dateViewNew.hidden = YES;
            self.investDate = [self convertDateToString:[self.datePickerViewNew date]];
        } else {
            self.datePickerViewNew.hidden = NO;
            self.dateViewNew.hidden = NO;
        }

        isShowingDatePicker = !isShowingDatePicker;
        [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma navigation

-(void) saveMethod
{
    NSLog(@"Save Button Clicked");

    // Check for Validation
    if([self portfolioValidation]){
        
        NSString *portID = [[portfolioData objectAtIndex:selectedPortfolioIndex] objectForKey:@"PortfolioID"];
        NSLog(@"Portfolio Name is %@", [[portfolioData objectAtIndex:selectedPortfolioIndex] objectForKey:@"PortfolioName"]);
        NSLog(@"Portfolio ID is %@", portID);
        NSLog(@"StockName is %@", self.stockName);
        NSLog(@"StockSymbol is %@", self.stockSymbol);
        NSLog(@"ExchDisp is %@", self.exchDisp);
        NSLog(@"TypeDisp is %@", self.typeDisp);
        NSLog(@"Google Exch Symbol is %@", self.googExchSymbol);
        NSLog(@"ExchName is %@", self.exchName);
        NSLog(@"BuyDate is %@", [self convertDateToString:[self.datePickerViewNew date]]);
        NSLog(@"BuyPrice is %@",[buyPriceTextField text]);
        NSLog(@"BuyQuan is %@",[buyQuanTextField text]);
     
        if(self.isEditing){
            if([stockBuy updateStockBuyForID:self.stockBuyID buyDate:[self convertDateToString:[self.datePickerViewNew date]] buyPrice:[buyPriceTextField text] buyQuanity:[buyQuanTextField text]]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePortfolioLabel" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                alertView.message = @"Cannot Add To Portfolio for uncertain reason.";
                [alertView show];
            }
        } else {
            if([stockBuy addToStockBuyTable:portID stockName:self.stockName stockSymbol:self.stockSymbol buyDate:[self convertDateToString:[self.datePickerViewNew date]] buyQuantity:[buyQuanTextField text] buyPrice:[buyPriceTextField text] notes:@"" exchDisp:self.exchDisp typeDisp:self.typeDisp googleExchSym:self.googExchSymbol]){
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                alertView.message = @"Cannot Add To Portfolio for uncertain reason.";
                [alertView show];
            }
        } // end of else editing
    }
}

#pragma mark -
#pragma helper functions

-(void) initializeFormTextFields
{
    buyQuanTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    buyPriceTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 320, 35)];
    
    buyQuanTextField.adjustsFontSizeToFitWidth = YES;
    buyQuanTextField.placeholder = @"15";
    buyQuanTextField.backgroundColor = [UIColor whiteColor];
    buyQuanTextField.keyboardType = UIKeyboardTypeDecimalPad;
    buyQuanTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];

    buyPriceTextField.adjustsFontSizeToFitWidth = YES;
    buyPriceTextField.placeholder = @"466.23";
    buyPriceTextField.backgroundColor = [UIColor whiteColor];
    buyPriceTextField.keyboardType = UIKeyboardTypeDecimalPad;
    buyPriceTextField.text = self.currPrice;
    buyPriceTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15.0f];
}

-(NSString*) convertDateToString:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    return [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date] ];
    
}

-(NSDate*) convertStringToDate:(NSString*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    return [dateFormatter dateFromString:date];
}

// Yes it is an integer: No: not an integer.
-(BOOL) isInteger:(NSString*)value
{
    float valueFlot = [value floatValue];
    return (valueFlot <= LONG_MIN || valueFlot >= LONG_MAX || valueFlot == (long)valueFlot) ? YES : NO;
}

// Yes: Validation ok; No: Not Valid
-(BOOL) portfolioValidation
{
    BOOL valid = true;

    if([[buyQuanTextField text] length] == 0 || [[buyPriceTextField text] length] == 0){
        alertView.message = @"Buy Quantity Or Buy Price Cannot Be Empty";
        [alertView show];
        valid = false;
    }
    
    if(![self isInteger:[buyQuanTextField text]]) {
        alertView.message = @"Buy Quantity is not a Valid Integer";
        [alertView show];
        valid = false;
    }
    
    if(([[buyQuanTextField text] doubleValue] < 0) || ([[buyPriceTextField text] doubleValue] < 0)){
        alertView.message = @"Buy Quantity Or Buy Price Cannot Be less than 0";
        [alertView show];
        valid = false;
    }
    
    if([portfolioData count] == 0){
        alertView.message = @"No Portfolio(s) Found!";
        [alertView show];
        valid = false;
    }
    
    return valid;
}

#pragma mark notifications

-(void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    offset = self.mainTable.contentOffset;
    mainTableHeight = self.mainTable.frame.size.height;
    CGRect viewFrame = self.mainTable.frame;
    viewFrame.size.height -= keyboardSize.height;
    self.mainTable.frame = viewFrame;
    
    CGRect textFieldRect = [buyPriceTextField frame];
    textFieldRect.origin.y += 10;
    [self.mainTable scrollRectToVisible:textFieldRect animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainTable.frame = CGRectMake(0, 0, 320, mainTableHeight);
    self.mainTable.contentOffset = offset;
    self.mainTable.scrollIndicatorInsets = contentInsets;
    
    [self.mainTable scrollRectToVisible:buyPriceTextField.frame animated:YES];
    
}

#pragma mark -
#pragma For Editing

-(void) fillFormForEditing
{
    self.navigationItem.title = @"Edit Stock";
    buyQuanTextField.text = self.investQuan;
    self.datePickerViewNew.date = [self convertStringToDate:self.investDate];
    
    [self.mainTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
}

@end
