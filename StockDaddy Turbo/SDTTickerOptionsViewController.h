//
//  SDTTickerOptionsViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 3/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTExchangeInfo.h"
#import "SDTWatchlist.h"
#import "SDTAlertDataViewController.h"
#import "SDTOptionChainsViewController.h"
#import "SDTStockNewsViewController.h"
#import "SDTStockBuyViewController.h"
#import "SDTGraphsViewController.h"

@interface SDTTickerOptionsViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSString *exchDisp;
@property (nonatomic, strong) NSString *typeDisp;
@property (nonatomic, strong) NSString *yahooSymbol;
@property (nonatomic, strong) NSString *googleSymbol;
@property (nonatomic, strong) NSString *exchangeName;
@property (nonatomic, strong) NSString *currentPrice;
@property (nonatomic, strong) NSString *currentChg;
@property (nonatomic, strong) NSString *currentChgPct;

@property (strong, nonatomic) NSArray *tickerOptionList;
@property (strong, nonatomic) UIAlertView *alert;

- (id)initWithSymbol:(NSString*)symbol initWithCompanyName:(NSString*)compName initWithExchDisp:(NSString*)exchDisp initWithTypeDisp:(NSString*)typeDisp initWithPrice:(NSString*)price initWithChgVal:(NSString*)chgVal initWithChgPct:(NSString*)chgPct;

- (void)addToPortfolio;
- (void)addToWatchlist;
- (void)addToAlertList;
- (void)showOptionChains;
- (void)showNews;

@end
