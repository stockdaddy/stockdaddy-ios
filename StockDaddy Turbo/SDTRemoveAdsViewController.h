//
//  SDTRemoveAdsViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/25/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface SDTRemoveAdsViewController : UIViewController <SKProductsRequestDelegate,SKPaymentTransactionObserver>


- (IBAction)removeAdsButton:(id)sender;


- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;

@end
