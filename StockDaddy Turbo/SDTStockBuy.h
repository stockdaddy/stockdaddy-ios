//
//  SDTStockBuy.h
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SDTStock.h"
#import "SDTStockSell.h"

@interface SDTStockBuy : NSObject {
    SDTStock *stockInfo;
    SDTStockSell *stockSellInfo;
}

- (id) init;
- (void) initDatabase;
- (BOOL) addToStockBuyTable:(NSString*)portfolioID stockName:(NSString*)stockName stockSymbol:(NSString*)stocksym buyDate:(NSString*)buyDate buyQuantity:(NSString*)buyQuan buyPrice:(NSString*)buyPrc notes:(NSString*)notes exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typeDisp googleExchSym:(NSString*)googExchSym;
- (BOOL) deleteFromStockBuyTable:(NSString*)stockBuyID;
- (BOOL) updateStockBuyForID:(NSString*)stockBuyID buyDate:(NSString*)buyDate buyPrice:(NSString*)buyPrice buyQuanity:(NSString*)buyQuant;
- (BOOL) updateBuyQuantityForStockBuyID:(NSString*)stockBuyID buyQuanity:(NSString*)buyQuant;

- (NSMutableArray*) getBuyQuanPriceList:(NSString*)portfolioID stockID:(NSString*)stockID;

- (NSString*) cumalativeBuyQuantity:(NSString*)portID stockID:(NSString*)stockID;
- (NSString*) averageBuyPrice:(NSString*)portID stockID:(NSString*)stockID;
- (NSString*) currentInvestmentForStockID:(NSString*)portfolioID stockID:(NSString*)stockID;
- (NSString*) gainLossForStockID:(NSString*)portfolioID stockID:(NSString*)stockID;
- (NSString*) remainingQtyForStockID:(NSString*)portfolioID stockID:(NSString*)stockID;

- (NSString*) getBuyQuantityForBuyID:(NSString*)buyID;
- (NSString*) getBuyPriceForBuyID:(NSString*)buyID;
- (NSString*) getBuyPriceForStockID:(NSString*)portfolioID stockID:(NSString*)stockID;

@end
