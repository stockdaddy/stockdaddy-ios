//
//  SDTTabBarHomeViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 5/31/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTTabBarHomeViewController.h"

@interface SDTTabBarHomeViewController ()

@end

@implementation SDTTabBarHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Check if Ads have been removed, if yes, update the VCs to remove VC for Ads
    NSUserDefaults *defaultsAds = [NSUserDefaults standardUserDefaults];
    if([[defaultsAds objectForKey:@"showAds"] isEqualToString:@"RemoveThem"]){
        NSMutableArray *tabBarVC = [NSMutableArray arrayWithArray:[self viewControllers]];
        // Remove Ads (at index 6)
        [tabBarVC removeObjectAtIndex:6];
        [self setViewControllers:tabBarVC];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
