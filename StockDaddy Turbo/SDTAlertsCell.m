//
//  SDTAlertsCell.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/16/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTAlertsCell.h"

@implementation SDTAlertsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
