//
//  SDTTickerOptionsViewController.m
//  StockDaddy Turbo
//
//  Created by Jain on 3/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTTickerOptionsViewController.h"

@interface SDTTickerOptionsViewController ()

@end

@implementation SDTTickerOptionsViewController

- (id)initWithSymbol:(NSString*)symbol initWithCompanyName:(NSString*)compName initWithExchDisp:(NSString*)exchDisp initWithTypeDisp:(NSString*)typeDisp initWithPrice:(NSString*)price initWithChgVal:(NSString*)chgVal initWithChgPct:(NSString*)chgPct
{
    self = [super init];
    if(self) {
        self.symbol = symbol;
        self.companyName = compName;
        self.exchDisp = exchDisp;
        self.typeDisp = typeDisp;
        self.currentPrice = price;
        self.currentChg = chgVal;
        self.currentChgPct = chgPct;
    }
    
    [self initializeOtherData];
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.tickerOptionList = @[@"Add to Portfolio", @"Add To Watchlist", @"Notify Me", @"Option Chains", @"News", @"Charts"];
    
    self.clearsSelectionOnViewWillAppear = YES;

    self.alert = [[UIAlertView alloc]initWithTitle:@"Add to Watchlist" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.tickerOptionList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TickerOptionCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    // Configure the cell...
    NSString *option = [self.tickerOptionList objectAtIndex:indexPath.row];
    [cell.textLabel setText:option];
    cell.textLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:17.0f];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Index Row 0: "Add To Portfolio"
    //Index Row 1: "Add To Watchlist"
    //Index Row 2: "Notify Me"
    //Index Row 3: "Option Chains"
    //Index Row 4: "News"
    //Index Row 5: "Graphs"

    switch (indexPath.row) {
        case 0:
            [self addToPortfolio];
            break;
        case 1:
            [self addToWatchlist];
            break;
        case 2:
            [self addToAlertList];
            break;
        case 3:
            [self showOptionChains];
            break;
        case 4:
            [self showNews];
            break;
        case 5:
            [self showGraphs];
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark option Methods

- (void)addToPortfolio
{
    NSLog(@"Add To Portfolio Option Clicked");
    NSLog(@"StockName is %@", self.companyName);
    NSLog(@"StockSymbol is %@", self.symbol);
    NSLog(@"ExchangeName is %@", self.exchangeName);
    NSLog(@"googleSymbol is %@", self.googleSymbol);

    self.alert.title = @"Add to Portfolio";
    
    if(![self tickerValidation]) {
        self.alert.message = @"Cannot Add to Portfolio due to Insufficient Information";
        [self.alert show];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    }
    else {
        SDTStockBuyViewController *stockBuy = [self.storyboard instantiateViewControllerWithIdentifier:@"StockBuyVC"];
        stockBuy.stockName = self.companyName;
        stockBuy.stockSymbol = self.symbol;
        stockBuy.exchDisp = self.exchDisp;
        stockBuy.typeDisp = self.typeDisp;
        stockBuy.exchName = self.exchangeName;
        stockBuy.googExchSymbol = self.googleSymbol;
        stockBuy.currPrice = self.currentPrice;
        
        [self.navigationController pushViewController:stockBuy animated:YES];
    }
}

- (void) addToWatchlist
{
    NSLog(@"Add To Watchlist Option Clicked");

    self.alert.title = @"Add to Watchlist";

    if(![self tickerValidation]) {
        self.alert.message = @"Cannot Add to My Watchlist due to Insufficient Information";
        [self.alert show];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    }
    else {
        NSLog(@"Symbol is %@", self.symbol);
        NSLog(@"CompanyName is %@", self.companyName);
        NSLog(@"exchDisp is %@", self.exchDisp);
        NSLog(@"typDisp is %@", self.typeDisp);
        NSLog(@"yahooSymbol is %@", self.yahooSymbol);
        NSLog(@"googleSymbol is %@", self.googleSymbol);
        NSLog(@"ExchangeName is %@", self.exchangeName);
        
        SDTWatchlist *watchlistData = [[SDTWatchlist alloc]init];
        
        if([watchlistData addToWatchlist:[self.symbol componentsSeparatedByString:@"."][0] stockName:self.companyName exchName:self.exchangeName yahooSymNExch:self.symbol googleSymbol:self.googleSymbol exchDisp:self.exchDisp typeDisp:self.typeDisp preLoaded:0]) {
            [self.navigationController popViewControllerAnimated:YES];
            self.alert.message = @"Successfully Added to My Watchlist";
        } else {
            self.alert.message = @"Cannot Add to My Watchlist as Ticker already Exists";
            [self.alert show];
        }
    }
}

- (void) addToAlertList
{
    NSLog(@"Notify Me Option Clicked");
    
    self.alert.title = @"Add to Alert List";

    if(![self tickerValidation]) {
        self.alert.message = @"Cannot Add to Alerts List due to Insufficient Information";
        [self.alert show];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    } else {
        SDTAlertDataViewController *alertData = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertDataOption"];
        alertData.symbol = self.symbol;
        alertData.stockName = self.companyName;
        alertData.googleSymbol = self.googleSymbol;
        alertData.exchName = self.exchangeName;
        alertData.currPrice = self.currentPrice;
        alertData.currChg = self.currentChg;
        alertData.currChgPct = self.currentChgPct;
        
        [self.navigationController pushViewController:alertData animated:YES];
    }
}

- (void)showOptionChains
{
    NSLog(@"Option Chains Option Clicked");
    
    self.alert.title = @"Option Chains";

    if(![self tickerValidation]) {
        self.alert.message = @"Cannot List Option Chains due to Insufficient Information";
        [self.alert show];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    } else {
        SDTOptionChainsViewController *optionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OptionChainVC"];
        optionsVC.symbol = self.symbol;
        
        [self.navigationController pushViewController:optionsVC animated:YES];
    }
}

- (void)showNews
{
    NSLog(@"News Option Clicked");
    
    self.alert.title = @"Stock News";

    if(![self tickerValidation]) {
        self.alert.message = @"Cannot Check Stock News due to Insufficient Information";
        [self.alert show];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    } else {
        SDTStockNewsViewController *stockNews = [self.storyboard instantiateViewControllerWithIdentifier:@"StockNewsVC"];
        stockNews.symbol = self.symbol;

        [self.navigationController pushViewController:stockNews animated:YES];

    }
}

- (void) showGraphs
{
    NSLog(@"Graph Option Clicked!");
    
    SDTGraphsViewController *graphVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GraphVC"];
    graphVC.stockSymbol = self.symbol;
    
    [self.navigationController pushViewController:graphVC animated:YES];
}

#pragma mark -
#pragma re-usable functiosn

// For initializing yahooSymbol, googleSymbol and ExchangeName;
- (void) initializeOtherData
{
    if([[self.symbol componentsSeparatedByString:@"."] count] > 1) {
        self.yahooSymbol = [[self.symbol componentsSeparatedByString:@"."] lastObject];
    } else {
        self.yahooSymbol = @"";
    }

    SDTExchangeInfo *exchinfo = [[SDTExchangeInfo alloc]init];
    if([exchinfo getExchangeName:self.yahooSymbol withExchDisp:self.exchDisp withTypeDisp:self.typeDisp]) {
        self.exchangeName = [exchinfo exchangeName];
    } else {
        self.exchangeName = @"";
        NSLog(@"Returning No for getExchangeName");
    }
    
    if([exchinfo getGoogleSymbol:self.yahooSymbol withExchDisp:self.exchDisp withTypeDisp:self.typeDisp]) {
        self.googleSymbol = [exchinfo googleSymbol];
    } else {
        self.googleSymbol = @"";
        NSLog(@"Returning No for GetGoogleSymbol");
    }
}

//True: Valid; False: Not-Valid
- (BOOL) tickerValidation
{
    BOOL valid = true;
    
    if(([self.exchangeName length] == 0) || ([self.googleSymbol length] == 0)){
        valid = false;
    }
    
    if([self.typeDisp isEqualToString:@"Index"]){
        valid = false;
    }
    
    if([self.typeDisp isEqualToString:@"Currency"]){
        valid = false;
    }

    if([self.typeDisp isEqualToString:@"Fund"]){
        valid = false;
    }

    return valid;
}

@end
