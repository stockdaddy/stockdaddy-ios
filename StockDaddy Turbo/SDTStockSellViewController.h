//
//  SDTStockSellViewController.h
//  StockDaddy Turbo
//
//  Created by Jain on 5/4/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDTPortfolio.h"
#import "SDTStockSell.h"
#import "SDTStockBuy.h"

@interface SDTStockSellViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    SDTPortfolio *portfolioInfo;
    SDTStockSell *sellInfo;
    SDTStockBuy *buyInfo;
}

@property (nonatomic, strong) NSString *portfolioID;
@property (nonatomic, strong) NSString *stockID;
@property (nonatomic, strong) NSString *stockName;
@property (nonatomic, strong) NSString *buyID;
@property (nonatomic, strong) NSString *buyDate;
@property (nonatomic, strong) NSString *buyQuant;
@property (nonatomic, strong) NSString *buyPrice;

@property (nonatomic, strong) UIDatePicker *datePickerViewNew;
@property (nonatomic, strong) UIView *dateViewNew;

@property (weak, nonatomic) IBOutlet UITableView *mainTable;

@end
