//
//  SDTStock.m
//  StockDaddy Turbo
//
//  Created by Jain on 4/27/14.
//  Copyright (c) 2014 JainLab Inc. All rights reserved.
//

#import "SDTStock.h"

@implementation SDTStock
static NSString *databasePath;
static sqlite3 *stockDaddyDB;

- (id)init
{
    self = [super init];
    if(self){
    }
    
    return self;
}

#pragma mark -
#pragma db operations

- (void)initDatabase
{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"stockDaddyTurbo.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Stock ("
        "                StockID INTEGER PRIMARY KEY AUTOINCREMENT,"
        "				 PortfolioID VARCHAR, "
        "				 StockName VARCHAR, "
        "				 StockSymbol VARCHAR, "
        "				 ExchDisp VARCHAR, "
        "				 TypeDisp VARCHAR, "
        "				 GoogleSymExch VARCHAR)";
        
        if (sqlite3_exec(stockDaddyDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to create Stock table because of %s", errMsg);
        }
        [self closeDataBase];
        
    } else {
        NSLog(@"Failed to open/create database");
    }
}

// returns the last inserted stockID
- (NSString*) addTOStocksTable:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol exchDisp:(NSString*)exchDisp typeDisp:(NSString*)typDisp  googExchSym:(NSString*) googExchSym
{
    NSString *stockID = @"";
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *insert_query = [NSString stringWithFormat:@"INSERT OR IGNORE INTO Stock (PortfolioID, StockName, StockSymbol, ExchDisp, TypeDisp, GoogleSymExch) VALUES(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", portfolioID, name, symbol, exchDisp, typDisp, googExchSym];
        const char *insert_stmt = [insert_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, insert_stmt, NULL, NULL, &errMsg) == SQLITE_OK) {
            [self closeDataBase];
            NSInteger stockIDInt = [[[NSNumber alloc] initWithUnsignedLongLong:sqlite3_last_insert_rowid(stockDaddyDB)] integerValue];
            stockID = [@(stockIDInt) stringValue];
        } else {
            NSLog(@"Failed to Insert to Stock table because of %s", errMsg);            
        }
        [self closeDataBase];
        return stockID;
    } else { NSLog(@"Failed to open/create database"); return stockID; }
}

// return YES: Succesfully Deleted; NO: Error
- (BOOL) deleteFromStockTable:(NSString*)stockID
{
    BOOL deleted = YES;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        char *errMsg;
        NSString *delete_query = [NSString stringWithFormat:@"DELETE FROM Stock WHERE StockID=\"%@\"", stockID];
        const char *delete_stmt = [delete_query UTF8String];
        
        if (sqlite3_exec(stockDaddyDB, delete_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Failed to Delete From Stock table because of %s", errMsg);
            [self closeDataBase];
            deleted = NO;
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); deleted = NO; }
    
    return deleted;
}


// return Yes: StockID Exists; NO: StockID doesn't exists
- (BOOL) stockIDExists:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol
{
    NSString *stockID = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockID FROM Stock WHERE PortfolioID=\"%@\" AND StockName=\"%@\" AND StockSymbol=\"%@\"", portfolioID, name, symbol];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
               stockID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);            
        }
        
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return NO; }
    
    return ([stockID length] == 0) ? NO : YES;
}

// return Yes: returns StockID ; NO: returns StockID
- (NSString*) getStockID:(NSString*)portfolioID stockName:(NSString*)name stockSymbol:(NSString*)symbol
{
    NSString *stockID = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockID FROM Stock WHERE PortfolioID=\"%@\" AND StockName=\"%@\" AND StockSymbol=\"%@\"", portfolioID, name, symbol];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stockID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return @""; }
    
    return stockID;
}

// Returns the List of all the StockIDs associated with a Particular PortfolioID
// Called when Portfolio is clicked. will be pre-req for calculating stats
- (NSMutableArray*) getStockIDList:(NSString*)portfolioID
{
    NSMutableArray *stockIDList = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockID FROM Stock WHERE PortfolioID=\"%@\"", portfolioID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *sID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                [stockIDList addObject:sID];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockIDList;
}

// Returns the List of all the info(id, name, symbol, googExch) in a dictionary associated with a Particular PortfolioID
// Called for TransactionHistory
- (NSMutableArray*) getStockInfo:(NSString*)portfolioID
{
    NSMutableArray *stockInfoList = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockID, StockName, StockSymbol, GoogleSymExch FROM Stock WHERE PortfolioID=\"%@\"", portfolioID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *sID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *name = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *symbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                NSString *googExch = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:sID forKey:@"StockID"];
                [tempDictionary setObject:name forKey:@"StockName"];
                [tempDictionary setObject:symbol forKey:@"StockSymbol"];
                [tempDictionary setObject:googExch forKey:@"GoogleExchange"];
                
                [stockInfoList addObject:tempDictionary];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockInfoList;
}

// returns a string which will be used for google requests
// NASDAQ:BBRY,NASDAQ:AAPL
-(NSString*) generateGoogleURLStockList:(NSString*)portID
{
    NSString *stocksURL = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT GoogleSymExch, StockSymbol FROM Stock WHERE PortfolioID=\"%@\"", portID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *googSym = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *stockSym = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                stocksURL = [stocksURL stringByAppendingString:[NSString stringWithFormat:@"%@:%@,", googSym, stockSym]];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stocksURL;
}

// Returns the StockSybmol based on PortfolioID and StockID
// Called when calculating dashboard stats for further validation of data.
- (NSString*) stockSymbolByStockID:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *stockSymbol = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockSymbol FROM Stock WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stockSymbol = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockSymbol;
}

// Returns the StockName based on PortfolioID and StockID
// Called for showing it on the dashboard
- (NSString*) stockNameByStockID:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *stockName = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT StockName FROM Stock WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stockName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return stockName;
}

// Returns the ExchDisp based on PortfolioID and StockID
// Called for Dashboard tableView onClick to QuoteView.
- (NSString*) stockExchDispByStockID:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *exchDisp = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT ExchDisp FROM Stock WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                exchDisp = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return exchDisp;
}

// Returns the Type based on PortfolioID and StockID
// Called for Dashboard tableView onClick to QuoteView.
- (NSString*) stockTypeDispByStockID:(NSString*)portID stockID:(NSString*)stockID
{
    NSString *exchDisp = @"";
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &stockDaddyDB) == SQLITE_OK){
        NSString *sel_query = [NSString stringWithFormat:@"SELECT ExchDisp FROM Stock WHERE PortfolioID=\"%@\" AND StockID=\"%@\"", portID, stockID];
        const char *sel_stmt = [sel_query UTF8String];
        
        if (sqlite3_prepare_v2(stockDaddyDB, sel_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                exchDisp = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_finalize(statement);
        }
        [self closeDataBase];
    } else { NSLog(@"Failed to open/create database"); return nil; }
    
    return exchDisp;
}

- (void) closeDataBase
{
    sqlite3_close(stockDaddyDB);
}


@end
